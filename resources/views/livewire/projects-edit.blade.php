<div>
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <div class="subheader py-3 py-lg-8 subheader-transparent" id="kt_subheader">
                <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                    <!--begin::Info-->
                    <div class="d-flex align-items-center mr-1">
                        <!--begin::Page Heading-->
                        <div class="d-flex align-items-baseline flex-wrap mr-5">
                            <!--begin::Page Title-->
                            <h2 class="d-flex align-items-center text-dark font-weight-bold my-1 mr-3">projects</h2>
                            <!--end::Page Title-->
                            <!--begin::Breadcrumb-->
                            <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold my-2 p-0">
                                <li class="breadcrumb-item">
                                    <a href="/homeberry-portal/projects" class="text-muted">projects List</a>
                                </li>
                                <li class="breadcrumb-item">
                                    <span class="text-muted">Edit project</span>
                                </li>
                            </ul>
                            <!--end::Breadcrumb-->
                        </div>
                        <!--end::Page Heading-->
                    </div>
                </div>
            </div>
            <!--begin::Notice-->
            <div class="alert alert-custom alert-white alert-shadow gutter-b" role="alert">
                <div class="alert-icon">
                    <span class="svg-icon svg-icon-primary svg-icon-xl">
                        <!--begin::Svg Icon | path:assets/media/svg/icons/Tools/Compass.svg-->
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect x="0" y="0" width="24" height="24"></rect>
                                <path d="M7.07744993,12.3040451 C7.72444571,13.0716094 8.54044565,13.6920474 9.46808594,14.1079953 L5,23 L4.5,18 L7.07744993,12.3040451 Z M14.5865511,14.2597864 C15.5319561,13.9019016 16.375416,13.3366121 17.0614026,12.6194459 L19.5,18 L19,23 L14.5865511,14.2597864 Z M12,3.55271368e-14 C12.8284271,3.53749572e-14 13.5,0.671572875 13.5,1.5 L13.5,4 L10.5,4 L10.5,1.5 C10.5,0.671572875 11.1715729,3.56793164e-14 12,3.55271368e-14 Z" fill="#000000" opacity="0.3"></path>
                                <path d="M12,10 C13.1045695,10 14,9.1045695 14,8 C14,6.8954305 13.1045695,6 12,6 C10.8954305,6 10,6.8954305 10,8 C10,9.1045695 10.8954305,10 12,10 Z M12,13 C9.23857625,13 7,10.7614237 7,8 C7,5.23857625 9.23857625,3 12,3 C14.7614237,3 17,5.23857625 17,8 C17,10.7614237 14.7614237,13 12,13 Z" fill="#000000" fill-rule="nonzero"></path>
                            </g>
                        </svg>
                        <!--end::Svg Icon-->
                    </span>
                </div>
                <div class="alert-text">
                    <b class="mr-2">projects</b>Edit project.
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <!--begin::Card-->
                        <div class="card card-custom gutter-b example example-compact">
                            <!-- <form wire:submit.prevent="submitForm" class="grid grid-cols-1 row-gap-6"> -->
                            <form wire:submit.prevent="submit" class="grid grid-cols-1 row-gap-6">

                                @if (session()->has('message'))
                                <div class="alert alert-success">
                                    {{ session('message') }}
                                </div>
                                @endif
                                <div class="ml-3">
                                    <p class="text-sm leading-5 font-medium text-green-800">
                                        {{ $successMessage }}
                                    </p>
                                </div>
                               
                                <div class="card-body">
                                    <div class="form-group row">

                                        <label class="col-lg-2 col-form-label text-lg-right"> Name Of project in Arabic:</label>
                                        <div class="col-lg-3">
                                            <input type="text" wire:model="name_ar" name="name_ar" class="form-control @error('name_ar') is-invalid @enderror" placeholder="Enter Arabic Name">
                                            <span class="form-text text-muted">Enter Arabic Name</span>
                                            @error('name_ar')
                                            <p class="text-red-500 mt-1">{{ $message }}</p>
                                            @enderror
                                        </div>

                                        <label class="col-lg-2 col-form-label text-lg-right">Name Of project in English:</label>
                                        <div class="col-lg-3">
                                            <input type="text" wire:model="name_en" name="name_en" class="form-control @error('name_en') is-invalid @enderror" placeholder="Enter English Name">
                                            <span class="form-text text-muted">Enter English Name</span>
                                            @error('name_en')
                                            <p class="text-red-500 mt-1">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>


                                    <div class="form-group row">



                                        <label class="col-lg-2 col-form-label text-lg-right"> Description Of project in Arabic:</label>
                                        <div class="col-lg-3">
                                            <textarea rows="3" wire:model="description_ar" name="description_ar" class="form-control @error('description_ar') is-invalid @enderror" placeholder="Enter Arabic description"></textarea>
                                            <span class="form-text text-muted">Enter Arabic description</span>
                                            @error('description_ar')
                                            <p class="text-red-500 mt-1">{{ $message }}</p>
                                            @enderror
                                        </div>

                                        <label class="col-lg-2 col-form-label text-lg-right">description Of project in English:</label>
                                        <div class="col-lg-3">
                                            <textarea rows="3" wire:model="description_en" name="description_en" class="form-control @error('description_en') is-invalid @enderror" placeholder="Enter English description"></textarea>
                                            <span class="form-text text-muted">Enter English description</span>
                                            @error('description_en')
                                            <p class="text-red-500 mt-1">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label text-lg-right" for="exampleSelect1">Developer </label>
                                        <div class="col-lg-3">
                                            <select class="form-control @error('devolper_id') is-invalid @enderror" wire:model="devolper_id" id="exampleSelect1" data-size="5" required data-live-search="true" name="devolper_id">
                                                <option selected>Select Item</option>
                                                @foreach ($developers as $item)
                                                <option value="{{$item->id}}">{{$item->name}}</option>
                                                @endforeach
                                            </select>
                                            @error('devolper_id')
                                            <p class="text-red-500 mt-1">{{ $message }}</p>
                                            @enderror
                                        </div>

                                        <label class="col-lg-2 col-form-label text-lg-right" for="exampleSelect2">City </label>
                                        <div class="col-lg-3">
                                            <select class="form-control @error('city_id') is-invalid @enderror" wire:model="city_id" id="exampleSelect2" data-size="5" required data-live-search="true" name="city_id">
                                                <option selected>Select Item</option>
                                                @foreach ($cities as $item)
                                                <option value="{{$item->id}}">{{$item->name_en}}</option>
                                                @endforeach
                                            </select>
                                            @error('city_id')
                                            <p class="text-red-500 mt-1">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">

                                        <label class="col-lg-2 col-form-label text-lg-right" for="exampleSelect4">Delivery </label>
                                        <div class="col-lg-3">
                                            <select class="form-control @error('delivery') is-invalid @enderror" wire:model="delivery" id="exampleSelect4" data-size="5" required data-live-search="true" name="delivery">
                                                <option selected>Select Item</option>
                                                <option value="ready to move">ready to move</option>
                                                @for($i=1; $i<=10 ;$i++) <option value="{{$i}} years">{{$i}} years</option>
                                                    @endfor
                                            </select>
                                            @error('delivery')
                                            <p class="text-red-500 mt-1">{{ $message }}</p>
                                            @enderror
                                        </div>

                                        <label class="col-lg-2 col-form-label text-lg-right" for="exampleSelect4">Finished </label>
                                        <div class="col-lg-3">
                                            <select class="form-control @error('finished') is-invalid @enderror" wire:model="finished" id="exampleSelect4" data-size="5" required data-live-search="true" name="finished">
                                                <option selected>Select Item</option>
                                                <option value="semi">Semi</option>
                                                <option value="fully">Fully</option>
                                            </select>
                                            @error('finished')
                                            <p class="text-red-500 mt-1">{{ $message }}</p>
                                            @enderror
                                        </div>

                                    </div>

                                    <div class="form-group row">


                                        <label class="col-lg-2 col-form-label text-lg-right">Payment: </label>
                                        <div class="col-lg-3">
                                            <select wire:model="payment_id" name="payment_id" class="form-control " data-live-search="true">
                                                <option value="" selected>Select</option>
                                                @foreach ($payments as $item)
                                                <option value="{{$item->id}}">{{$item->name_en}}</option>
                                                @endforeach
                                            </select>
                                            @error('payment_id')
                                            <p class="text-red-500 mt-1">{{ $message }}</p>
                                            @enderror
                                        </div>

                                        {{-- <label for="example-number-input" class="col-2 col-form-label text-lg-right">Price</label>
                                        <div class="col-lg-3">
                                            <input class="form-control @error('price') is-invalid @enderror" type="number" wire:model="price" value="0" name="price" id="example-number-input">
                                            @error('price')
                                            <p class="text-red-500 mt-1">{{ $message }}</p>
                                            @enderror
                                        </div> --}}

                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label text-lg-right">Share Link</label>
                                        <div class="col-lg-3">
                                            <input type="url" wire:model="link" name="link" class="form-control @error('link') is-invalid @enderror" placeholder="Enter Link">
                                            @error('link')
                                            <p class="text-red-500 mt-1">{{ $message }}</p>
                                            @enderror
                                        </div>

                                        <label class="col-lg-2 col-form-label text-lg-right"> Youtube Link</label>
                                        <div class="col-lg-3">
                                            <input type="url" wire:model="youtube" name="youtube" class="form-control @error('youtube') is-invalid @enderror" placeholder="Enter youtube">
                                            @error('youtube')
                                            <p class="text-red-500 mt-1">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">

                                        <label class="col-lg-2 col-form-label text-lg-right"> Location Name </label>
                                        <div class="col-lg-3">
                                            <input type="text" wire:model="location" name="location" class="form-control @error('location') is-invalid @enderror" placeholder="Enter location">
                                            @error('location')
                                            <p class="text-red-500 mt-1">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label text-lg-right" for="activitytypes"> Activity Types </label>
                                        <div class="col-lg-3">
                                            <select multiple="multiple" wire:model="activitytypes" name="activitytypes" class="form-control @error('activitytypes') is-invalid @enderror" data-live-search="true" id="amenities">
                                                @foreach ($activitytypeArr as $item)

                                                @if(in_array($item->id,$lastactivitytypes))
                                                <option value="{{$item->id}}" selected>{{$item->name_en}}</option>
                                                @else
                                                <option value="{{$item->id}}">{{$item->name_en}}</option>
                                                @endif
                                                @endforeach
                                            </select>
                                            @error('activitytypes')
                                            <p class="text-red-500 mt-1">{{ $message }}</p>
                                            @enderror
                                        </div>

                                        <label class="col-lg-2 col-form-label text-lg-right" for="furnishedtypes"> Furnished Types </label>
                                        <div class="col-lg-3">

                                            <div class="checkbox-list">
                                              

                                                
                                                @foreach ($furnishedtypeArr as $item)
                                                @if(in_array($item->id,$lastfurnishedtypes))
                                                <label class="checkbox checkbox-primary">
                                                    <input type="checkbox" value="{{$item->id}}" name="furnishedtypes"wire:model="furnishedtypes" checked="checked">
                                                    <span></span>{{$item->name_en}}</label>
                                                @else 
                                                <label class="checkbox checkbox-primary">
                                                <input type="checkbox" value="{{$item->id}}" name="furnishedtypes"wire:model="furnishedtypes" >
                                                <span></span>{{$item->name_en}}</label>
                                                @endif
                                                @endforeach
                                                
                                            </div>
                                           
                                            @error('furnishedtypes')
                                            <p class="text-red-500 mt-1">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label text-lg-right">Images <span class="text-danger">*</span></label>
                                        <div class="custom-file col-lg-8">
                                            <input type="file" wire:model="images" accept=".png, .jpg, .jpeg"  name="images" multiple class="custom-file-input  @error('images') is-invalid @enderror" id="customFile">
                                            <div wire:loading wire:target="images" class="text-sm text-gray-500 italic">Uploading...</div>

                                            @error('images')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                            @enderror
                                            <label class="custom-file-label" for="customFile">Choose file</label>
                                        </div>

                                    </div>

                                    @if ($images)
                                    <div class="form-group row">
                                        Photo Preview:
                                        @foreach($images as $image)
                                        <div class="col-lg-2">
                                            <img src="{{ $image->temporaryUrl() }}">
                                        </div>
                                        @endforeach
                                    </div>
                                    @endif
                                    @if ($lastimages)

                                    <div class="form-group row">
                                        Images :
                                        @foreach($lastimages as $image)
                                        <div class="col-lg-2">
                                            <img src="{{ asset($image->file) }}">
                                        </div>
                                        @endforeach
                                    </div>
                                    @endif

                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label text-lg-right">Details</label>

                                    </div>

                                    @foreach($inputs as $ikey => $ivalue)
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label text-lg-right"></label>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Enter key" wire:model="key.{{ $ikey }}">
                                                @error('key.'.$ikey) <span class="text-danger error">{{ $message }}</span>@enderror
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <input type="text" class="form-control" wire:model="value.{{ $ikey }}" placeholder="Enter Value">
                                                @error('value.'.$ikey) <span class="text-danger error">{{ $message }}</span>@enderror
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <button class="btn btn-danger btn-sm" wire:click.prevent="remove({{$ikey}})">remove</button>
                                        </div>
                                    </div>
                                    @endforeach

                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label text-lg-right"></label>

                                        <div class="col-form-label text-lg-right">
                                            <button class="btn text-white btn-success btn-sm" wire:click.prevent="add({{$i}})">Add New Detail</button>
                                        </div>

                                    </div>


                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label text-lg-right">Status:</label>
                                        <div class="col-lg-3">
                                            <div class="radio-inline">
                                                <label class="radio radio-solid">
                                                    <input type="radio" name="status" wire:model="status" checked="checked" value="1">
                                                    <span></span>Active</label>
                                                <label class="radio radio-solid">
                                                    <input type="radio" name="status" wire:model="status" value="0">
                                                    <span></span>In Active</label>
                                            </div>
                                            <span class="form-text text-muted">Please select Status</span>
                                        </div>
                                        <label class="col-lg-2 col-form-label text-lg-right">Constructed:</label>
                                        <div class="col-lg-3">
                                            <div class="radio-inline">
                                                <label class="radio radio-solid">
                                                    <input type="radio" name="is_constructed" wire:model="is_constructed" checked="checked" value="1">
                                                    <span></span>True</label>
                                                <label class="radio radio-solid">
                                                    <input type="radio" name="is_constructed" wire:model="is_constructed" value="0">
                                                    <span></span>False</label>
                                            </div>
                                            <span class="form-text text-muted">Please select Constructed</span>
                                        </div>

                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label text-lg-right">Featured: <span class="text-danger">*</span></label>
                                        <div class="col-lg-3">
                                            <div class="radio-inline">
                                                <label class="radio radio-solid">
                                                    <input type="radio" name="priority" wire:model="priority" checked="checked" value="1">
                                                    <span></span>Yes</label>
                                                <label class="radio radio-solid">
                                                    <input type="radio" name="priority" wire:model="priority" value="0">
                                                    <span></span>No</label>
                                            </div>
                                            <span class="form-text text-muted">Please select Featured</span>
                                        </div>
                                    </div>

                                  
                                </div>
                                <div class="card-footer">
                                    <div class="row">
                                        <div class="col-lg-2"></div>
                                        <div class="col-lg-10">
                                            <button class="btn btn-primary btn-flat" type="submit"><i class="fa fa-save"></i>Submit</button>
                                            {{-- <button  type="button" id="submit" class="btn btn-primary btn-flat "><i class="fa fa-save"></i> Save</button> --}}
                                            <a href="/homeberry-portal/projects" class="btn btn-default btn-flat pull-left"><i class="fa fa-close"></i> Back</a>

                                        </div>
                                    </div>
                                </div>
                            </form>


                            <!-- end: Example Code-->
                        </div>
                        <!--end::Form-->
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

@section('scripts')

<script>
    window.addEventListener('alert', event => {
        Swal.fire({
            icon: event.detail.type,
            title: event.detail.title,
            text: event.detail.message,
            type: event.detail.type,
           // showCancelButton: true,
            
            confirmButtonText: "Go To Projects Table",
            confirmButtonColor: "#F64E60",
          
            allowOutsideClick: false
        }).then((result) => {
            if (result.isConfirmed) {
                window.location.href = "/homeberry-portal/projects";
            }
        });
    })
</script>



@stop

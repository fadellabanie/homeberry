<div>
   <div class="d-flex flex-column-fluid">
      <!--begin::Container-->
      <div class="container">
         <div class="subheader py-3 py-lg-8 subheader-transparent" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
               <!--begin::Info-->
               <div class="d-flex align-items-center mr-1">
                  <!--begin::Page Heading-->
                  <div class="d-flex align-items-baseline flex-wrap mr-5">
                     <!--begin::Page Title-->
                     <h2 class="d-flex align-items-center text-dark font-weight-bold my-1 mr-3">Permission</h2>
                     <!--end::Page Title-->
                     <!--begin::Breadcrumb-->
                     <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold my-2 p-0">
                        <li class="breadcrumb-item">
                           <a href="" class="text-muted">Permission List</a>
                        </li>
                     </ul>
                     <!--end::Breadcrumb-->
                  </div>
                  <!--end::Page Heading-->
               </div>
               <div class="d-flex align-items-center flex-wrap">
                  <!--begin::Button-->
                  <a href="{{route('add-permissions')}}" class="btn btn-primary btn-fixed-height font-weight-bold px-2 px-lg-5 mr-2">
                     <span class="svg-icon svg-icon-lg">
                        <!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                           <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                              <polygon points="0 0 24 0 24 24 0 24"></polygon>
                              <path d="M18,8 L16,8 C15.4477153,8 15,7.55228475 15,7 C15,6.44771525 15.4477153,6 16,6 L18,6 L18,4 C18,3.44771525 18.4477153,3 19,3 C19.5522847,3 20,3.44771525 20,4 L20,6 L22,6 C22.5522847,6 23,6.44771525 23,7 C23,7.55228475 22.5522847,8 22,8 L20,8 L20,10 C20,10.5522847 19.5522847,11 19,11 C18.4477153,11 18,10.5522847 18,10 L18,8 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                              <path d="M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero"></path>
                           </g>
                        </svg>
                        <!--end::Svg Icon-->
                     </span>
                     <span class="d-none d-md-inline">New Permission</span>
                  </a>
               </div>
            </div>
         </div>
         <!--begin::Notice-->
         <div class="alert alert-custom alert-white alert-shadow gutter-b" group="alert">
            <div class="alert-icon">
               <span class="svg-icon svg-icon-primary svg-icon-xl">
                  <!--begin::Svg Icon | path:assets/media/svg/icons/Tools/Compass.svg-->
                  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                     <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <rect x="0" y="0" width="24" height="24"></rect>
                        <path d="M7.07744993,12.3040451 C7.72444571,13.0716094 8.54044565,13.6920474 9.46808594,14.1079953 L5,23 L4.5,18 L7.07744993,12.3040451 Z M14.5865511,14.2597864 C15.5319561,13.9019016 16.375416,13.3366121 17.0614026,12.6194459 L19.5,18 L19,23 L14.5865511,14.2597864 Z M12,3.55271368e-14 C12.8284271,3.53749572e-14 13.5,0.671572875 13.5,1.5 L13.5,4 L10.5,4 L10.5,1.5 C10.5,0.671572875 11.1715729,3.56793164e-14 12,3.55271368e-14 Z" fill="#000000" opacity="0.3"></path>
                        <path d="M12,10 C13.1045695,10 14,9.1045695 14,8 C14,6.8954305 13.1045695,6 12,6 C10.8954305,6 10,6.8954305 10,8 C10,9.1045695 10.8954305,10 12,10 Z M12,13 C9.23857625,13 7,10.7614237 7,8 C7,5.23857625 9.23857625,3 12,3 C14.7614237,3 17,5.23857625 17,8 C17,10.7614237 14.7614237,13 12,13 Z" fill="#000000" fill-rule="nonzero"></path>
                     </g>
                  </svg>
                  <!--end::Svg Icon-->
               </span>
            </div>
            <div class="alert-text">
               <b class="mr-2">Permissions</b>List of Permission.
            </div>
         </div>
         <div class="container">
            <div class="row">
               <div class="col-lg-12">
                  <div class="flex flex-col mt-8">
                     <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                        <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                           <div class="flex items-center justify-between">
                              <div class="max-w-lg w-full lg:max-w-xs">
                                 <label for="search" class="sr-only">Search</label>
                                 <div class="relative">
                                    <div class="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                                       <svg class="h-5 w-5 text-gray-400" fill="currentColor" viewBox="0 0 20 20">
                                          <path fill-rule="evenodd" d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z" clip-rule="evenodd"></path>
                                       </svg>
                                    </div>
                                    <input wire:model="search" id="search" class="block w-full pl-10 pr-3 py-2 border border-gray-300 rounded-md leading-5 bg-white placeholder-gray-500 focus:outline-none focus:placeholder-gray-400 focus:border-blue-300 focus:shadow-outline-blue sm:text-sm transition duration-150 ease-in-out" placeholder="Search" type="search">
                                 </div>
                              </div>
                           </div>
                           <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg mt-4">
                              <table class="min-w-full divide-y divide-gray-200">
                                 <thead>
                                    <tr>
                                       <th class="px-6 py-3 text-left">
                                          <div class="flex items-center">
                                             <button wire:click="sortBy('id')" class="bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">ID</button>
                                             <x-sort-icon field="id" :sortField="$sortField" :sortAsc="$sortAsc" />
                                          </div>
                                       </th>
                                       <th class="px-6 py-3 text-left">
                                          <div class="flex items-center">
                                             <button wire:click="sortBy('name')" class="bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">Name</button>
                                             <x-sort-icon field="name" :sortField="$sortField" :sortAsc="$sortAsc" />
                                          </div>
                                       </th>
                                       <th class="px-6 py-3 text-left">
                                          <div class="flex items-center">
                                             <button wire:click="sortBy('created_at')" class="bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">Created </button>
                                             <x-sort-icon field="created_at" :sortField="$sortField" :sortAsc="$sortAsc" />
                                          </div>
                                       </th>
                                       <th class="px-6 py-3 bg-gray-50"></th>
                                    </tr>
                                 </thead>
                                 <tbody class="bg-white divide-y divide-gray-200">
                                    @foreach ($permissions as $permission)
                                    <tr>
                                       <td class="w-4/12 px-6 py-4 whitespace-no-wrap">
                                          <div class="flex items-center">
                                             <div class="ml-4">
                                                <div class="text-sm leading-5 font-medium text-gray-900">
                                                   {{ $permission->id }}
                                                </div>
                                             </div>
                                          </div>
                                       </td>
                                       <td class="w-4/12 px-6 py-4 whitespace-no-wrap">
                                          <div class="flex items-center">
                                             <div class="ml-4">
                                                <div class="text-sm leading-5 font-medium text-gray-900">
                                                   {{ $permission->name }}
                                                </div>
                                             </div>
                                          </div>
                                       </td>
                                       <td class="w-4/12 px-6 py-4 whitespace-no-wrap">
                                          <div class="flex items-center">
                                             <div class="ml-4">
                                                <div class="text-sm leading-5 font-medium text-gray-900">
                                                   {{ $permission->created_at->diffForHumans() }}
                                                </div>
                                             </div>
                                          </div>
                                       </td>
                                       <td class="px-6 py-4 whitespace-no-wrap text-right text-sm leading-5 font-medium">
                                          
                                          <a href="{{route('edit-permissions',$permission->id )}}" class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2" title="Edit details">
                                             <span class="svg-icon svg-icon-md">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                   <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                      <rect x="0" y="0" width="24" height="24"></rect>
                                                      <path d="M12.2674799,18.2323597 L12.0084872,5.45852451 C12.0004303,5.06114792 12.1504154,4.6768183 12.4255037,4.38993949 L15.0030167,1.70195304 L17.5910752,4.40093695 C17.8599071,4.6812911 18.0095067,5.05499603 18.0083938,5.44341307 L17.9718262,18.2062508 C17.9694575,19.0329966 17.2985816,19.701953 16.4718324,19.701953 L13.7671717,19.701953 C12.9505952,19.701953 12.2840328,19.0487684 12.2674799,18.2323597 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.701953, 10.701953) rotate(-135.000000) translate(-14.701953, -10.701953) "></path>
                                                      <path d="M12.9,2 C13.4522847,2 13.9,2.44771525 13.9,3 C13.9,3.55228475 13.4522847,4 12.9,4 L6,4 C4.8954305,4 4,4.8954305 4,6 L4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 L2,6 C2,3.790861 3.790861,2 6,2 L12.9,2 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                                                   </g>
                                                </svg>
                                             </span>
                                          </a>
                                          <button wire:click="destroy({{$permission->id}})" href="javascript:;" class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon" title="Delete">
                                             <span class="svg-icon svg-icon-md">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                   <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                      <rect x="0" y="0" width="24" height="24"></rect>
                                                      <path d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z" fill="#000000" fill-rule="nonzero"></path>
                                                      <path d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z" fill="#000000" opacity="0.3"></path>
                                                   </g>
                                                </svg>
                                             </span>
                                          </button>
                                       </td>
                                    </tr>
                                    @endforeach
                                 </tbody>
                              </table>
                           </div>
                           <div class="mt-8">
                              {{ $permissions->links() }}
                           </div>
                        </div>
                     </div>
                     <div class="h-96"></div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
  <div>

      <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
          <!--begin::Subheader-->
          <div class="subheader py-2 py-lg-4 subheader-transparent" id="kt_subheader">
              <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                  <!--begin::Details-->
                  <div class="d-flex align-items-center flex-wrap mr-2">
                      <!--begin::Title-->
                      <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Projects</h5>
                      <!--end::Title-->
                      <!--begin::Separator-->
                      <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-5 bg-gray-200"></div>
                      <!--end::Separator-->
                      <!--begin::Search Form-->
                      <div class="d-flex align-items-center" id="kt_subheader_search">
                          <span class="text-dark-50 font-weight-bold" id="kt_subheader_total">{{$projects->total()}} Total</span>
                          <form class="ml-5">
                              <div class="input-group input-group-sm input-group-solid" style="max-width: 175px">
                                  <input wire:model="search" type="text" class="form-control" id="kt_subheader_search_form" placeholder="Search...">
                                  <div class="input-group-append">
                                      <span class="input-group-text">
                                          <span class="svg-icon">
                                              <!--begin::Svg Icon | path:assets/media/svg/icons/General/Search.svg-->
                                              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                  <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                      <rect x="0" y="0" width="24" height="24"></rect>
                                                      <path d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                                                      <path d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z" fill="#000000" fill-rule="nonzero"></path>
                                                  </g>
                                              </svg>
                                              <!--end::Svg Icon-->
                                          </span>
                                          <!--<i class="flaticon2-search-1 icon-sm"></i>-->
                                      </span>
                                  </div>
                              </div>
                          </form>
                      </div>
                      <!--end::Search Form-->
                   
                  </div>
                  <!--end::Details-->
                  <!--begin::Toolbar-->
                  <div class="d-flex align-items-center">
                      <!--begin::Button-->
                      <div class="d-flex align-items-center">
                        <!--begin::Button-->
                        <!--end::Button-->
                        <div class="checkbox-inline">
                                        
                            <label class="checkbox checkbox-success">
                                <input wire:model="active" id="active" type="checkbox" name="active">
                                <span></span>Active</label>
                                <label class="checkbox checkbox-primary">
                                    <input wire:model="priority" id="priority" type="checkbox" name="priority">
                                    <span></span>Featured</label>
                        </div>
                    </div>
                      <!--end::Button-->
                      {{-- @can('add-location-projects')
                      <a href="/homeberry-portal/add-loaction-project" data-turbolinks="false" class="btn btn-light-primary font-weight-bold ml-2">Add Loaction Project</a>
                      @endcan --}}
                      <!--begin::Button-->
                      <a href="{{route('export-project')}}" class="btn btn-light-success font-weight-bold ml-2">Export</a>
                     
                      @can('add-projects')
                      <a href="/homeberry-portal/add-project"  class="btn btn-light-primary font-weight-bold ml-2">Add New project</a>
                     @endcan
                      <!--end::Button-->
                  </div>
                  <!--end::Toolbar-->
              </div>
          </div>
          <!--end::Subheader-->
          <!--begin::Entry-->
          <div class="d-flex flex-column-fluid">
              <!--begin::Container-->
              <div class="container">
                  <!--begin::Row-->
                  <div class="row">
                      @foreach ($projects as $item)
                      <div class="col-xl-6">
                          <!--begin::Card-->
                          <div class="card card-custom gutter-b card-stretch">
                              <!--begin::Body-->
                              <div class="card-body">
                                  <!--begin::Section-->
                                  <div class="d-flex align-items-center">
                                      <!--begin::Pic-->
                                      <div class="flex-shrink-0 mr-4 symbol symbol-65 symbol-circle">
                                          <img src="{{ asset($item->medias->first()  ? $item->medias->first()->file : 'https://lorempixel.com/640/480/?65383') }}" alt="image" />
                                      </div>
                                      <!--end::Pic-->
                                      <!--begin::Info-->
                                      <div class="d-flex flex-column mr-auto">
                                          <!--begin: Title-->
                                          <a href="/homeberry-portal/show-project/{{$item->id}}" class="card-title text-hover-primary font-weight-bolder font-size-h5 text-dark mb-1">{{ $item->name_en }}</a>
                                          <span class="text-muted font-weight-bold">{{ $item->name_ar }}</span>
                                          <!--end::Title-->
                                      </div>
                                      <!--end::Info-->
                                  </div>
                                  <!--end::Section-->
                                  <!--begin::Content-->
                                  <div class="d-flex flex-wrap mt-14">
                                      <div class="mr-12 d-flex flex-column mb-7">
                                          <span class="d-block font-weight-bold mb-4">Start Date</span>
                                          <span class="btn btn-light-primary btn-sm font-weight-bold btn-upper btn-text">{{$item->created_at}}</span>
                                      </div>
                                      <div class="mr-12 d-flex flex-column mb-7">
                                          <span class="d-block font-weight-bold mb-4">Delivery</span>
                                          <span class="btn btn-light-danger btn-sm font-weight-bold btn-upper btn-text">{{$item->delivery}}</span>
                                      </div>
                                      <!--begin::Progress-->
                                      <div class="flex-row-fluid mb-7">
                                          <span class="d-block font-weight-bold mb-4">Progress</span>
                                          <div class="d-flex align-items-center pt-2">
                                              <div class="progress progress-xs mt-2 mb-2 w-100">
                                                  <div class="progress-bar bg-warning" role="progressbar" style="width: {{delivery_percentage($item->delivery)}}%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                              </div>
                                              <span class="ml-3 font-weight-bolder">{{delivery_percentage($item->delivery)}}%</span>
                                          </div>
                                      </div>
                                      <!--end::Progress-->
                                  </div>
                                  <!--end::Content-->
                                  <!--begin::Text-->
                                  <p class="mb-7 mt-3">{{ $item->description_en }}</p>
                                  <!--end::Text-->
                                  <!--begin::Blog-->
                                  <div class="d-flex flex-wrap">
                                      <!--begin: Item-->
                                      <!--end::Item-->
                                      <!--begin::Item-->
                                      <div class="d-flex flex-column flex-lg-fill float-left mb-7">
                                          <span class="font-weight-bolder mb-4">Finished</span>
                                          @foreach ($item->furnishedTypes as $types)
                                          <span class="btn mb-4 btn-text btn-light-info btn-sm font-weight-bold" style="width: 80px;">{{$types->name_en ?? ""}}</span>
                                            @endforeach
                                      </div>
                                      <!--end::Item-->
                                      <!--begin::Item-->
                                      <div class="mr-12 d-flex flex-column mb-7">
                                          <span class="font-weight-bolder mb-4">Views</span>
                                          <span class="font-weight-bolder font-size-h5 pt-1">
                                              <span class="font-weight-bold text-dark-50">
                                                <span class="label pulse pulse-success mr-10">
                                                    <span class="position-relative"> {{ $item->views }}</span>
                                                    <span class="pulse-ring"></span>
                                                </span>
                                                </span></span>
                                      </div>
                                      <div class="mr-12 d-flex flex-column mb-7">
                                          <span class="font-weight-bolder mb-4">Clicked</span>
                                          <span class="font-weight-bolder font-size-h5 pt-1">
                                              <span class="font-weight-bold text-dark-50">
                                                <span class="label pulse pulse-warning mr-10">
                                                    <span class="position-relative"> {{ $item->clicked }}</span>
                                                    <span class="pulse-ring"></span>
                                                </span> </span></span>
                                      </div>
                                      <!--end::Item-->
                                      <a href="/homeberry-portal/show-project/{{$item->id}}" class="btn btn-block btn-sm btn-light-success font-weight-bolder text-uppercase py-4">Show</a>

                                      @can('delete-projects')
                                      <a onclick="confirm('Confirm delete?') || event.stopImmediatePropagation()" wire:click="destroy({{$item->id}})"  class="btn btn-block btn-sm btn-light-danger font-weight-bolder text-uppercase py-4">Delete</a>
                                      @endcan
                                  </div>
                                  <!--end::Blog-->
                              </div>
                              <!--end::Body-->
                          </div>

                          <!--end::Card-->
                      </div>
                      @endforeach
                  </div>
                  <!--end::Row-->
                  <!--begin::Pagination-->
                  <div class="card card-custom">
                      <div class="card-body py-7">
                          <!--begin::Pagination-->
                          <div class="d-flex justify-content-between align-items-center flex-wrap">
                              {{ $projects->links() }}
                          </div>
                          <!--end:: Pagination-->
                      </div>
                  </div>
                  <!--end::Pagination-->
              </div>
              <!--end::Container-->
          </div>
          <!--end::Entry-->
      </div>
  </div>

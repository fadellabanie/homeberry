<div>

    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <div class="subheader py-3 py-lg-8 subheader-transparent" id="kt_subheader">
                <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                    <!--begin::Info-->
                    <div class="d-flex align-items-center mr-1">
                        <!--begin::Page Heading-->
                        <div class="d-flex align-items-baseline flex-wrap mr-5">
                            <!--begin::Page Title-->
                            <h2 class="d-flex align-items-center text-dark font-weight-bold my-1 mr-3">Schedules</h2>
                            <!--end::Page Title-->
                            <!--begin::Breadcrumb-->
                            <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold my-2 p-0">
                                <li class="breadcrumb-item">
                                    <a href="" class="text-muted">Schedules List</a>
                                </li>
                               
                            </ul>
                            <!--end::Breadcrumb-->
                        </div>
                        <!--end::Page Heading-->
                    </div>

                </div>
            </div>
            <!--begin::Notice-->
            <div class="alert alert-custom alert-white alert-shadow gutter-b" role="alert">
                <div class="alert-icon">
                    <span class="svg-icon svg-icon-primary svg-icon-xl">
                        <!--begin::Svg Icon | path:assets/media/svg/icons/Tools/Compass.svg-->
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect x="0" y="0" width="24" height="24"></rect>
                                <path d="M7.07744993,12.3040451 C7.72444571,13.0716094 8.54044565,13.6920474 9.46808594,14.1079953 L5,23 L4.5,18 L7.07744993,12.3040451 Z M14.5865511,14.2597864 C15.5319561,13.9019016 16.375416,13.3366121 17.0614026,12.6194459 L19.5,18 L19,23 L14.5865511,14.2597864 Z M12,3.55271368e-14 C12.8284271,3.53749572e-14 13.5,0.671572875 13.5,1.5 L13.5,4 L10.5,4 L10.5,1.5 C10.5,0.671572875 11.1715729,3.56793164e-14 12,3.55271368e-14 Z" fill="#000000" opacity="0.3"></path>
                                <path d="M12,10 C13.1045695,10 14,9.1045695 14,8 C14,6.8954305 13.1045695,6 12,6 C10.8954305,6 10,6.8954305 10,8 C10,9.1045695 10.8954305,10 12,10 Z M12,13 C9.23857625,13 7,10.7614237 7,8 C7,5.23857625 9.23857625,3 12,3 C14.7614237,3 17,5.23857625 17,8 C17,10.7614237 14.7614237,13 12,13 Z" fill="#000000" fill-rule="nonzero"></path>
                            </g>
                        </svg>
                        <!--end::Svg Icon-->
                    </span>
                </div>
                <div class="alert-text">
                    <b class="mr-2">Schedules</b>List Schedules.
                </div>
            </div>
            <nav>
                <ul class="nav nav-pills" id="myTab" role="tablist">
                    <li class="nav-item" role="presentation">
                      <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Properties</a>
                    </li>
                    <li class="nav-item" role="presentation">
                      <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Projects</a>
                    </li>
                   
                  </ul>
                  <div class="tab-content mt-5" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <div class="container">
                            <div class="row">
                               @foreach ($schedulesProperty as $key => $item)
                                <div class="col-xl-4">
                                    <!--begin::Mixed Widget 8-->
                                    <div class="card card-custom gutter-b card-stretch">
                                        <!--begin::Card body-->
                                        <div class="card-body">
                                            <div class="d-flex flex-wrap align-items-center py-1">
                                                <!--begin::Pic-->
                                                <div class="symbol symbol-80 symbol-light-success mr-5">
                                                    <span class="symbol-label">
                                                    <img src="{{asset('admin/assets/media/svg/misc/014-kickstarter.svg')}}" class="h-50 align-self-center" alt="">
                                                    </span>
                                                </div>
                                                <!--end::Pic-->
                                                <!--begin::Title-->
                                                <div class="d-flex flex-column flex-grow-1 my-lg-0 my-2 pr-3">
                                                    <a href="{{route('show-property',$item->property->id)}}" class="text-dark font-weight-bolder text-hover-primary font-size-h5">   {{ $item->property->name_en === null ? $item->property->name_en : $item->property->name_ar}}
                                                    </a>
                                                    <span class="text-muted font-weight-bold font-size-lg">{{$item->date}}</span>
                                                </div>
                                                <!--end::Title-->
                                 
                                                <!--begin::Team-->
                                                <div class="d-flex flex-column mt-10">
                                                    <span class="text-dark mr-2 font-size-lg font-weight-bolder pb-4">User</span>
                                                    <div class="d-flex">
                                                        <!--begin::Pic-->
                                                        <a href="" class="symbol symbol-50 symbol-light-success mr-3">
                                                            <div class="symbol-label">
                                                                <img src="{{asset('admin/assets/media/svg/avatars/00'.$key.'-boy.svg')}}" class="h-75 align-self-end" alt="">
                                                            </div>
                                                        </a>
                                                        <!--end::Pic-->
                                                        <div class="mb-7">
                                                            <div class="d-flex justify-content-between align-items-center">
                                                                <span class="text-dark-75 font-weight-bolder mr-2">Name:</span>
                                                                <span class="text-muted font-weight-bold">{{$item->name}}</span>
                                                            </div>
                                                            <div class="d-flex justify-content-between align-items-center">
                                                                <span class="text-dark-75 font-weight-bolder mr-2">Email:</span>
                                                            <a href="#" class="text-muted text-hover-primary">{{$item->email}}</a>
                                                            </div>
                                                            <div class="d-flex justify-content-between align-items-cente my-2">
                                                                <span class="text-dark-75 font-weight-bolder mr-2">Phone:</span>
                                                                <a href="#" class="text-muted text-hover-primary">{{$item->mobile}}</a>
                                                            </div>
                                                           
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--end::Team-->
                                            </div>
                                        </div>
                                        <!--end::Body-->
                                    </div>
                                    <!--end::Mixed Widget 8-->
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="d-flex justify-content-between align-items-center flex-wrap">
                          {{$schedulesProperty->links()}}
                         
                        </div>
                    </div>
                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        <div class="container">
                            <div class="row">
                               @foreach ($schedulesProject as $item)
                                <div class="col-xl-4">
                                    <!--begin::Mixed Widget 8-->
                                    <div class="card card-custom gutter-b card-stretch">
                                        <!--begin::Card body-->
                                        <div class="card-body">
                                            <div class="d-flex flex-wrap align-items-center py-1">
                                                <!--begin::Pic-->
                                                <div class="symbol symbol-80 symbol-light-success mr-5">
                                                    <span class="symbol-label">
                                                        <img src="{{asset('admin/assets/media/svg/misc/014-kickstarter.svg')}}" class="h-50 align-self-center" alt="">
                                                    </span>
                                                </div>
                                                <!--end::Pic-->
                                                <!--begin::Title-->
                                                <div class="d-flex flex-column flex-grow-1 my-lg-0 my-2 pr-3">
                                                    <a href="{{route('show-project',$item->project->id)}}" class="text-dark font-weight-bolder text-hover-primary font-size-h5">   {{ $item->project->name_en === null ? $item->project->name_en : $item->project->name_ar}}
                                                    <br>Based SaaS</a>
                                                    <span class="text-muted font-weight-bold font-size-lg">{{$item->date}}</span>
                                                </div>
                                                <!--end::Title-->
                                 
                                                <!--begin::Team-->
                                                <div class="d-flex flex-column mt-10">
                                                    <span class="text-dark mr-2 font-size-lg font-weight-bolder pb-4">Team</span>
                                                    <div class="d-flex">
                                                        <!--begin::Pic-->
                                                        <a href="#" class="symbol symbol-50 symbol-light-success mr-3">
                                                            <div class="symbol-label">
                                                                <img src="assets/media/svg/avatars/001-boy.svg" class="h-75 align-self-end" alt="">
                                                            </div>
                                                        </a>
                                                        <!--end::Pic-->
                                                        <!--begin::Pic-->
                                                        <a href="#" class="symbol symbol-50 symbol-light-success mr-3">
                                                            <div class="symbol-label">
                                                                <img src="assets/media/svg/avatars/028-girl-16.svg" class="h-75 align-self-end" alt="">
                                                            </div>
                                                        </a>
                                                        <!--end::Pic-->
                                                        <!--begin::Pic-->
                                                        <a href="#" class="symbol symbol-50 symbol-light-success mr-3">
                                                            <div class="symbol-label">
                                                                <img src="assets/media/svg/avatars/024-boy-9.svg" class="h-75 align-self-end" alt="">
                                                            </div>
                                                        </a>
                                                        <!--end::Pic-->
                                                    </div>
                                                </div>
                                                <!--end::Team-->
                                            </div>
                                        </div>
                                        <!--end::Body-->
                                    </div>
                                    <!--end::Mixed Widget 8-->
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="d-flex justify-content-between align-items-center flex-wrap">
                            {{$schedulesProject->links()}}
                           
                          </div>
                    </div>
                  </div>
          
        </div>
    </div>
</div>

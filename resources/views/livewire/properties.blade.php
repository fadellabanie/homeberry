<div>

    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-4 subheader-transparent" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Details-->
                <div class="d-flex align-items-center flex-wrap mr-2">
                    <!--begin::Title-->
                    <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Properties</h5>
                    <!--end::Title-->
                    <!--begin::Separator-->
                    <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-5 bg-gray-200"></div>
                    <!--end::Separator-->
                    <!--begin::Search Form-->
                    <div class="d-flex align-items-center" id="kt_subheader_search">
                        <span class="text-dark-50 font-weight-bold" id="kt_subheader_total">{{$properties->total()}} Total</span>
                        <form class="ml-5">
                            <div class="input-group input-group-sm input-group-solid" style="max-width: 175px">
                                <input wire:model="search" type="text" class="form-control" id="kt_subheader_search_form" placeholder="Search...">
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <span class="svg-icon">
                                            <!--begin::Svg Icon | path:assets/media/svg/icons/General/Search.svg-->
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <path d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                                                    <path d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z" fill="#000000" fill-rule="nonzero"></path>
                                                </g>
                                            </svg>
                                            <!--end::Svg Icon-->
                                        </span>
                                        <!--<i class="flaticon2-search-1 icon-sm"></i>-->
                                    </span>
                                </div>
                            </div>
                        </form>
                       
                       
                    </div>

                  
                    <!--end::Search Form-->
                   
                </div>
             
                <!--end::Details-->
                <!--begin::Toolbar-->
                <div class="d-flex align-items-center">
                    <!--begin::Button-->
                    <!--end::Button-->
                    <div class="checkbox-inline">
                        <label class="checkbox checkbox-success">
                            <input wire:model="active" id="active" type="checkbox" name="active">
                            <span></span>Active</label>
                              <label class="checkbox checkbox-primary">
                            <input wire:model="priority" id="priority" type="checkbox" name="priority">
                            <span></span>Featured</label>
                    </div>
                    {{-- @can('add-location-properties')
                    <a href="/homeberry-portal/add-loaction-property" data-turbolinks="false" class="btn btn-light-primary font-weight-bold ml-2">Add Loaction Property</a>
                    @endcan --}}
                    <!--begin::Button-->
                    <a href="{{route('export-property')}}" class="btn btn-light-success font-weight-bold ml-2">Export</a>
                    @can('add-properties')
                    <a href="/homeberry-portal/add-property" class="btn btn-light-primary font-weight-bold ml-2">Add New Property</a>
                    @endcan
                    <!--end::Button-->

                </div>
                <!--end::Toolbar-->
            </div>
        </div>
        <!--end::Subheader-->
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Row-->
                <div class="row">
                    <!--begin::Col-->
                    @foreach ($properties as $property)
                    <div class="col-xl-4 col-lg-6 col-md-6 col-sm-6">
                        <!--begin::Card-->
                        <div class="card card-custom gutter-b card-stretch">
                            <!--begin::Body-->
                            <div class="card-body pt-4">
                                <!--begin::User-->
                                <div class="d-flex align-items-center mb-7">
                                    <!--begin::Pic-->
                                    <div class="flex-shrink-0 mr-4">
                                        <div class="symbol symbol-circle symbol-lg-75">
                                            <img src="{{ asset($property->medias->first() ? $property->medias->first()->file : 'https://lorempixel.com/640/480/?65383') }}" alt="image">
                                        </div>
                                    </div>
                                    <!--end::Pic-->
                                    <!--begin::Title-->
                                    <div class="d-flex flex-column">
                                        <a href="/homeberry-portal/show-property/{{$property->id}}" class="text-dark font-weight-bold text-hover-primary font-size-h4 mb-0">{{$property->name_ar}}</a>
                                        <span class="text-muted font-weight-bold">{{$property->name_en}}
                                             @if($property->status == 1)
                                            <i class="flaticon2-correct text-success icon-md ml-2"></i>
                                            @else
                                        <a href="" style="margin-left: 35px;" onMouseOver="this.style.color='#746fa3'"  onMouseOut="this.style.color='#ff0000'" wire:click="approve({{$property->id}})" title="approve">Approve</a>
                                           
                                        @endif</span>
                                    </div>
                                    <!--end::Title-->
                                </div>
                                <!--end::User-->
                                <!--begin::Desc-->
                                <p class="mb-7">{!! substr($property->description_ar, 0, 80) !!}
                                    {!! substr($property->description_en, 0, 80) !!}
                                    <span class="text-primary pr-1">{{$property->activityType->name_en}}</span>
                                </p>
                                <!--end::Desc-->
                                <!--begin::Info-->
                                <div class="mb-7">
                                    <div class="d-flex justify-content-between align-items-center">
                                        <span class="text-dark-75 font-weight-bolder mr-2">Payment Mathod:</span>
                                        <a href="/homeberry-portal/show-property/{{$property->id}}" class="text-muted text-hover-primary">{{$property->paymentMathod->name_en}}</a>
                                    </div>
                                    <div class="d-flex justify-content-between align-items-cente my-1">
                                        <span class="text-dark-75 font-weight-bolder mr-2">City:</span>
                                        <a href="#" class="text-muted text-hover-primary">{{$property->city->name_en}}</a>
                                    </div>
                                    <div class="d-flex justify-content-between align-items-cente my-1">
                                        <span class="text-dark-75 font-weight-bolder mr-2">Views:</span>
                                        <a href="#" class="text-muted text-hover-primary">
                                            
                                            {{$property->views}}
                                        </a>
                                    </div>
                                    <div class="d-flex justify-content-between align-items-center">
                                        <span class="text-dark-75 font-weight-bolder mr-2">Created Date:</span>
                                        <span class="text-danger text-hover-primary">{{$property->created_at->diffForHumans()}}</span>
                                    </div>
                                    <div class="separator separator-solid my-7"></div>
                                    <div class="col-12">
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="d-flex justify-content-between align-items-center">
                                                    <span class="text-dark-75 font-weight-bolder mr-2">
                                                        <a href="">
                                                        <span class="svg-icon svg-icon-primary svg-icon-2x" title="Bathroom">
                                                            <!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo5\dist/../src/media/svg/icons\Home\Bath.svg-->
                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                    <path d="M6,10 C6,10.5522847 5.55228475,11 5,11 C4.44771525,11 4,10.5522847 4,10 L4,6 C4,4.34314575 5.34314575,3 7,3 C8.65685425,3 10,4.34314575 10,6 C10,6.55228475 9.55228475,7 9,7 C8.44771525,7 8,6.55228475 8,6 C8,5.44771525 7.55228475,5 7,5 C6.44771525,5 6,5.44771525 6,6 L6,10 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                                                                    <path d="M6,20 L4,20 C4,17.2385763 6.23857625,16 9,16 L15,16 C17.7614237,16 20,17.2385763 20,20 L18,20 C18,18.3431458 16.6568542,18 15,18 L9,18 C7.34314575,18 6,18.3431458 6,20 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                                                                    <path d="M2,9 L23,9 L22.4942047,13.5521576 C22.2128532,16.084321 20.0725321,18 17.524786,18 L7.47521397,18 C4.92746788,18 2.78714678,16.084321 2.50579529,13.5521576 L2,9 Z M19.25,11.5 C19.25,12.9305987 18.694479,14.0416408 17.55,14.9 C17.2186292,15.1485281 17.1514719,15.6186292 17.4,15.95 C17.6485281,16.2813708 18.1186292,16.3485281 18.45,16.1 C19.9721877,14.9583592 20.75,13.4027346 20.75,11.5 C20.75,11.0857864 20.4142136,10.75 20,10.75 C19.5857864,10.75 19.25,11.0857864 19.25,11.5 Z" fill="#000000" />
                                                                </g>
                                                            </svg>
                                                            <!--end::Svg Icon-->
                                                        </span>
                                                    </a>
                                                    </span>
                                                    <span class="text-muted font-weight-bold">{{$property->propertyAttribute->bathroom ?? 0}}</span>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="d-flex justify-content-between align-items-center">
                                                    <span class="text-dark-75 font-weight-bolder mr-2">
                                                        <a href="">
                                                        <span class="svg-icon svg-icon-primary svg-icon-2x" title="Guest">
                                                            <!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo5\dist/../src/media/svg/icons\Communication\Group.svg-->
                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                    <polygon points="0 0 24 0 24 24 0 24" />
                                                                    <path d="M18,14 C16.3431458,14 15,12.6568542 15,11 C15,9.34314575 16.3431458,8 18,8 C19.6568542,8 21,9.34314575 21,11 C21,12.6568542 19.6568542,14 18,14 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                                                                    <path d="M17.6011961,15.0006174 C21.0077043,15.0378534 23.7891749,16.7601418 23.9984937,20.4 C24.0069246,20.5466056 23.9984937,21 23.4559499,21 L19.6,21 C19.6,18.7490654 18.8562935,16.6718327 17.6011961,15.0006174 Z M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero" />
                                                                </g>
                                                            </svg>
                                                            <!--end::Svg Icon-->
                                                        </span>
                                                        </a>
                                                    </span>
                                                    <span class="text-muted font-weight-bold">{{$property->propertyAttribute->guest ?? ""}}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="d-flex justify-content-between align-items-center">
                                                    <span class="text-dark-75 font-weight-bolder mr-2" title="ergerg">
                                                        <a href="">

                                                        <span class="svg-icon svg-icon-primary svg-icon-2x" title="Room">
                                                            <!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo5\dist/../src/media/svg/icons\Home\Couch.svg-->
                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                    <path d="M6,20 L4,20 C4,17.2385763 6.23857625,16 9,16 L15,16 C17.7614237,16 20,17.2385763 20,20 L18,20 C18,18.3431458 16.6568542,18 15,18 L9,18 C7.34314575,18 6,18.3431458 6,20 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                                                                    <path d="M23,8 L21.173913,8 C20.0693435,8 19.173913,8.8954305 19.173913,10 L19.173913,12 C19.173913,12.5522847 18.7261978,13 18.173913,13 L5.86956522,13 C5.31728047,13 4.86956522,12.5522847 4.86956522,12 L4.86956522,10 C4.86956522,8.8954305 3.97413472,8 2.86956522,8 L1,8 C1,6.34314575 2.34314575,5 4,5 L20,5 C21.6568542,5 23,6.34314575 23,8 Z" fill="#000000" opacity="0.3" />
                                                                    <path d="M23,10 L23,15 C23,16.6568542 21.6568542,18 20,18 L4,18 C2.34314575,18 1,16.6568542 1,15 L1,10 L2.86956522,10 L2.86956522,12 C2.86956522,13.6568542 4.21271097,15 5.86956522,15 L18.173913,15 C19.8307673,15 21.173913,13.6568542 21.173913,12 L21.173913,10 L23,10 Z" fill="#000000" />
                                                                </g>
                                                            </svg>
                                                            <!--end::Svg Icon-->
                                                        </span>
                                                    </a>
                                                    </span>
                                                    <span class="text-muted font-weight-bold">{{$property->propertyAttribute->room ?? " "}}</span>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="d-flex justify-content-between align-items-center">
                                                    <span class="text-dark-75 font-weight-bolder mr-2">
                                                        <a href="">

                                                        <span class="svg-icon svg-icon-primary svg-icon-2x" title="Area">
                                                            <!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo5\dist/../src/media/svg/icons\Home\Home.svg-->
                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                    <rect x="0" y="0" width="24" height="24" />
                                                                    <path d="M3.95709826,8.41510662 L11.47855,3.81866389 C11.7986624,3.62303967 12.2013376,3.62303967 12.52145,3.81866389 L20.0429,8.41510557 C20.6374094,8.77841684 21,9.42493654 21,10.1216692 L21,19.0000642 C21,20.1046337 20.1045695,21.0000642 19,21.0000642 L4.99998155,21.0000673 C3.89541205,21.0000673 2.99998155,20.1046368 2.99998155,19.0000673 L2.99999828,10.1216672 C2.99999935,9.42493561 3.36258984,8.77841732 3.95709826,8.41510662 Z M10,13 C9.44771525,13 9,13.4477153 9,14 L9,17 C9,17.5522847 9.44771525,18 10,18 L14,18 C14.5522847,18 15,17.5522847 15,17 L15,14 C15,13.4477153 14.5522847,13 14,13 L10,13 Z" fill="#000000" />
                                                                </g>
                                                            </svg>
                                                            <!--end::Svg Icon-->
                                                        </span>
                                                    </a>
                                                    </span>
                                                    <span class="text-muted font-weight-bold">{{$property->propertyAttribute->area ?? ""}}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="d-flex justify-content-between align-items-center">
                                                    <span class="svg-icon svg-icon-primary svg-icon-2x">
                                                        <a href="">
                                                        <span class="svg-icon svg-icon-primary svg-icon-2x" title="Number Of Parks"><!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo5\dist/../src/media/svg/icons\Shopping\Rouble.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                <rect x="0" y="0" width="24" height="24"/>
                                                                <path d="M5.3618034,16.2763932 L5.8618034,15.2763932 C5.94649941,15.1070012 6.11963097,15 6.30901699,15 L16.190983,15 C16.4671254,15 16.690983,15.2238576 16.690983,15.5 C16.690983,15.5776225 16.6729105,15.6541791 16.6381966,15.7236068 L16.1381966,16.7236068 C16.0535006,16.8929988 15.880369,17 15.690983,17 L5.80901699,17 C5.53287462,17 5.30901699,16.7761424 5.30901699,16.5 C5.30901699,16.4223775 5.32708954,16.3458209 5.3618034,16.2763932 Z" fill="#000000" opacity="0.3"/>
                                                                <path d="M8,3.716 L13.107,3.716 C14.042338,3.716 14.8856629,3.80033249 15.637,3.969 C16.3883371,4.13766751 17.0323306,4.41366475 17.569,4.797 C18.1056693,5.18033525 18.5196652,5.67099701 18.811,6.269 C19.1023348,6.86700299 19.248,7.58766245 19.248,8.431 C19.248,9.33567119 19.079335,10.0946636 18.742,10.708 C18.404665,11.3213364 17.9485029,11.8158315 17.3735,12.1915 C16.7984971,12.5671685 16.1276705,12.8393325 15.361,13.008 C14.5943295,13.1766675 13.781671,13.261 12.923,13.261 L10.692,13.261 L10.692,20 L8,20 L8,3.716 Z M12.716,10.823 C13.1913357,10.823 13.6436645,10.7885003 14.073,10.7195 C14.5023355,10.6504997 14.885665,10.5278342 15.223,10.3515 C15.560335,10.1751658 15.8286657,9.9336682 16.028,9.627 C16.2273343,9.3203318 16.327,8.92166912 16.327,8.431 C16.327,7.95566429 16.2273343,7.5685015 16.028,7.2695 C15.8286657,6.97049851 15.5641683,6.73666751 15.2345,6.568 C14.9048317,6.39933249 14.5291688,6.28816694 14.1075,6.2345 C13.6858312,6.18083307 13.2526689,6.154 12.808,6.154 L10.692,6.154 L10.692,10.823 L12.716,10.823 Z" fill="#000000"/>
                                                            </g>
                                                        </svg><!--end::Svg Icon--></span>
                                                    </a>
                                                    </span>
                                                    <span class="text-muted font-weight-bold">{{$property->propertyAttribute->park ?? ""}}</span>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="d-flex justify-content-between align-items-center">
                                                    <a href="">

                                                    <span class="text-dark-75 font-weight-bolder mr-2">
                                                        <span class="svg-icon svg-icon-primary svg-icon-2x" title="Price">
                                                            <!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo5\dist/../src/media/svg/icons\Shopping\Dollar.svg-->
                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                    <rect x="0" y="0" width="24" height="24" />
                                                                    <rect fill="#000000" opacity="0.3" x="11.5" y="2" width="2" height="4" rx="1" />
                                                                    <rect fill="#000000" opacity="0.3" x="11.5" y="16" width="2" height="5" rx="1" />
                                                                    <path d="M15.493,8.044 C15.2143319,7.68933156 14.8501689,7.40750104 14.4005,7.1985 C13.9508311,6.98949895 13.5170021,6.885 13.099,6.885 C12.8836656,6.885 12.6651678,6.90399981 12.4435,6.942 C12.2218322,6.98000019 12.0223342,7.05283279 11.845,7.1605 C11.6676658,7.2681672 11.5188339,7.40749914 11.3985,7.5785 C11.2781661,7.74950085 11.218,7.96799867 11.218,8.234 C11.218,8.46200114 11.2654995,8.65199924 11.3605,8.804 C11.4555005,8.95600076 11.5948324,9.08899943 11.7785,9.203 C11.9621676,9.31700057 12.1806654,9.42149952 12.434,9.5165 C12.6873346,9.61150047 12.9723317,9.70966616 13.289,9.811 C13.7450023,9.96300076 14.2199975,10.1308324 14.714,10.3145 C15.2080025,10.4981676 15.6576646,10.7419985 16.063,11.046 C16.4683354,11.3500015 16.8039987,11.7268311 17.07,12.1765 C17.3360013,12.6261689 17.469,13.1866633 17.469,13.858 C17.469,14.6306705 17.3265014,15.2988305 17.0415,15.8625 C16.7564986,16.4261695 16.3733357,16.8916648 15.892,17.259 C15.4106643,17.6263352 14.8596698,17.8986658 14.239,18.076 C13.6183302,18.2533342 12.97867,18.342 12.32,18.342 C11.3573285,18.342 10.4263378,18.1741683 9.527,17.8385 C8.62766217,17.5028317 7.88033631,17.0246698 7.285,16.404 L9.413,14.238 C9.74233498,14.6433354 10.176164,14.9821653 10.7145,15.2545 C11.252836,15.5268347 11.7879973,15.663 12.32,15.663 C12.5606679,15.663 12.7949989,15.6376669 13.023,15.587 C13.2510011,15.5363331 13.4504991,15.4540006 13.6215,15.34 C13.7925009,15.2259994 13.9286662,15.0740009 14.03,14.884 C14.1313338,14.693999 14.182,14.4660013 14.182,14.2 C14.182,13.9466654 14.1186673,13.7313342 13.992,13.554 C13.8653327,13.3766658 13.6848345,13.2151674 13.4505,13.0695 C13.2161655,12.9238326 12.9248351,12.7908339 12.5765,12.6705 C12.2281649,12.5501661 11.8323355,12.420334 11.389,12.281 C10.9583312,12.141666 10.5371687,11.9770009 10.1255,11.787 C9.71383127,11.596999 9.34650161,11.3531682 9.0235,11.0555 C8.70049838,10.7578318 8.44083431,10.3968355 8.2445,9.9725 C8.04816568,9.54816454 7.95,9.03200304 7.95,8.424 C7.95,7.67666293 8.10199848,7.03700266 8.406,6.505 C8.71000152,5.97299734 9.10899753,5.53600171 9.603,5.194 C10.0970025,4.85199829 10.6543302,4.60183412 11.275,4.4435 C11.8956698,4.28516587 12.5226635,4.206 13.156,4.206 C13.9160038,4.206 14.6918294,4.34533194 15.4835,4.624 C16.2751706,4.90266806 16.9686637,5.31433061 17.564,5.859 L15.493,8.044 Z" fill="#000000" />
                                                                </g>
                                                            </svg>
                                                            <!--end::Svg Icon-->
                                                        </span>
                                                    </span>
                                                    </a>
                                                    <span class="text-muted font-weight-bold">{{$property->price}}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="separator separator-solid my-7"></div>
                                    {{-- <span class="label label-lg label-light-warning label-inline">{{strtoUpper($property->propertyAttribute->furnished) ?? " "}}</span> --}}
                                    <span class="label label-lg label-light-primary label-inline">{{strtoUpper($property->propertyAttribute->type) ?? ""}}</span>
                                    <span class="label label-lg label-light-info label-inline">Availability: {{$property->propertyAttribute->availability ?? ""}}</span>
                                </div>
                                <!--end::Info-->
                                <a href="/homeberry-portal/show-property/{{$property->id}}" class="btn btn-block btn-sm btn-light-success font-weight-bolder text-uppercase py-4">Show</a>
                                @can('delete-properties')
                                <a onclick="confirm('Confirm delete?') || event.stopImmediatePropagation()" wire:click="destroy({{$property->id}})"  class="btn btn-block btn-sm btn-light-danger font-weight-bolder text-uppercase py-4">Delete</a>
                                @endcan()
                            </div>
                          
                            <!--end::Body-->
                        </div>
                        <!--end:: Card-->
                    </div>
                    <!--end::Col-->
                    @endforeach
                </div>
                <!--end::Row-->
                <!--begin::Pagination-->
                <div class="card card-custom">
                    <div class="card-body py-7">
                        <!--begin::Pagination-->
                        <div class="d-flex justify-content-between align-items-center flex-wrap">
                            {{ $properties->links() }}
                        </div>
                        <!--end:: Pagination-->
                    </div>
                </div>
                <!--end::Pagination-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
</div>

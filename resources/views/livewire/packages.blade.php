<div>
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Subheader-->
        <div class="subheader py-3 py-lg-8 subheader-transparent" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h2 class="d-flex align-items-center text-dark font-weight-bold my-1 mr-3">Package Subscription</h2>
                        <!--end::Page Title-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
             
            </div>
        </div>
        <!--end::Subheader-->
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Card-->
                <div class="card card-custom">
                    <div class="card-header">
                        <div class="card-title">
                            <span class="card-icon">
                                <i class="flaticon2-box-1 text-success"></i>
                            </span>
                            <h3 class="card-label">Subscription price table</h3>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row my-10">
                          @foreach ($packages as $item)
                            <!--begin: Pricing-->
                            <div class="col-md-6 col-xxl-3 border-right-0 border-right-xxl border-bottom border-bottom-xxl-0">
                                <div class="pt-30 pt-md-25 pb-15 px-5 text-center">
                                    <div class="d-flex flex-center position-relative mb-25">
                                        <span class="svg svg-fill-primary opacity-4 position-absolute">
                                            <svg width="175" height="200">
                                                <polyline points="87,0 174,50 174,150 87,200 0,150 0,50 87,0"></polyline>
                                            </svg>
                                        </span>
                                        <span class="svg-icon svg-icon-5x svg-icon-success">
                                            <!--begin::Svg Icon | path:assets/media/svg/icons/Shopping/Box3.svg-->
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <path d="M20.4061385,6.73606154 C20.7672665,6.89656288 21,7.25468437 21,7.64987309 L21,16.4115967 C21,16.7747638 20.8031081,17.1093844 20.4856429,17.2857539 L12.4856429,21.7301984 C12.1836204,21.8979887 11.8163796,21.8979887 11.5143571,21.7301984 L3.51435707,17.2857539 C3.19689188,17.1093844 3,16.7747638 3,16.4115967 L3,7.64987309 C3,7.25468437 3.23273352,6.89656288 3.59386153,6.73606154 L11.5938615,3.18050598 C11.8524269,3.06558805 12.1475731,3.06558805 12.4061385,3.18050598 L20.4061385,6.73606154 Z" fill="#000000" opacity="0.3"></path>
                                                    <polygon fill="#000000" points="14.9671522 4.22441676 7.5999999 8.31727912 7.5999999 12.9056825 9.5999999 13.9056825 9.5999999 9.49408582 17.25507 5.24126912"></polygon>
                                                </g>
                                            </svg>
                                            <!--end::Svg Icon-->
                                        </span>
                                    </div>
                                    <span class="font-size-h1 d-block font-weight-boldest text-dark-75 py-2">{{$item->price}}
                                    <sup class="font-size-h3 font-weight-normal pl-1">$</sup></span>
                                    <h4 class="font-size-h6 d-block font-weight-bold mb-7 text-dark-50">{{$item->name_en}}</h4>
                                    <p class="mb-15 d-flex flex-column">
                                        <label class="font-size-h6 font-weight-bolder text-dark pt-5">Number: <span>{{$item->count}}</span></label>
                                    <span>
                                       
                                        <span> {{ mb_strimwidth($item->description_en, 0, 75, "...")}}</span>
                                    </span>
                                      
                                    </p>
                                   
                                    <a href="/homeberry-portal/edit-packages/{{$item->id}}" class="btn btn-block btn-sm btn-light-success font-weight-bolder text-uppercase py-4 ml-2">Edit</a>
                                    @can('delete-packages')
                                    <a onclick="confirm('Confirm delete?') || event.stopImmediatePropagation()" wire:click="destroy({{$item->id}})"  class="btn btn-block btn-sm btn-light-danger font-weight-bolder text-uppercase py-4 ml-2 ">Delete</a>
                                    @endcan
                                </div>
                            </div>
                            <!--end: Pricing-->
                          
                            @endforeach
                          
                        </div>
                    </div>
                </div>
                <!--end::Card-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
</div>

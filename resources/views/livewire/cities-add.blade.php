<div>
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <div class="subheader py-3 py-lg-8 subheader-transparent" id="kt_subheader">
                <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                    <!--begin::Info-->
                    <div class="d-flex align-items-center mr-1">
                        <!--begin::Page Heading-->
                        <div class="d-flex align-items-baseline flex-wrap mr-5">
                            <!--begin::Page Title-->
                            <h2 class="d-flex align-items-center text-dark font-weight-bold my-1 mr-3">Cities</h2>
                            <!--end::Page Title-->
                            <!--begin::Breadcrumb-->
                            <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold my-2 p-0">
                                <li class="breadcrumb-item">
                                    <a href="/homeberry-portal/cities" class="text-muted">Cities List</a>
                                </li>
                                <li class="breadcrumb-item">
                                    <span class="text-muted">Add City</span>
                                </li>
                            </ul>
                            <!--end::Breadcrumb-->
                        </div>
                        <!--end::Page Heading-->
                    </div>
                </div>
            </div>
            <!--begin::Notice-->
            <div class="alert alert-custom alert-white alert-shadow gutter-b" role="alert">
                <div class="alert-icon">
                    <span class="svg-icon svg-icon-primary svg-icon-xl">
                        <!--begin::Svg Icon | path:assets/media/svg/icons/Tools/Compass.svg-->
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect x="0" y="0" width="24" height="24"></rect>
                                <path d="M7.07744993,12.3040451 C7.72444571,13.0716094 8.54044565,13.6920474 9.46808594,14.1079953 L5,23 L4.5,18 L7.07744993,12.3040451 Z M14.5865511,14.2597864 C15.5319561,13.9019016 16.375416,13.3366121 17.0614026,12.6194459 L19.5,18 L19,23 L14.5865511,14.2597864 Z M12,3.55271368e-14 C12.8284271,3.53749572e-14 13.5,0.671572875 13.5,1.5 L13.5,4 L10.5,4 L10.5,1.5 C10.5,0.671572875 11.1715729,3.56793164e-14 12,3.55271368e-14 Z" fill="#000000" opacity="0.3"></path>
                                <path d="M12,10 C13.1045695,10 14,9.1045695 14,8 C14,6.8954305 13.1045695,6 12,6 C10.8954305,6 10,6.8954305 10,8 C10,9.1045695 10.8954305,10 12,10 Z M12,13 C9.23857625,13 7,10.7614237 7,8 C7,5.23857625 9.23857625,3 12,3 C14.7614237,3 17,5.23857625 17,8 C17,10.7614237 14.7614237,13 12,13 Z" fill="#000000" fill-rule="nonzero"></path>
                            </g>
                        </svg>
                        <!--end::Svg Icon-->
                    </span>
                </div>
                <div class="alert-text">
                    <b class="mr-2">Cities</b>Add City.
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <!--begin::Card-->
                        <div class="card card-custom gutter-b example example-compact">
                            <form wire:submit.prevent="submit" action="/addCity" method="POST" class="grid grid-cols-1 row-gap-6">
                                @csrf
                                @if (session()->has('message'))
                                <div class="alert alert-success">
                                    {{ session('message') }}
                                </div>
                                @endif
                                <div class="ml-3">
                                    <p class="text-sm leading-5 font-medium text-green-800">
                                        {{ $successMessage }}
                                    </p>
                                </div>
                               
                                <div class="card-body">
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label text-lg-right"> Name Of City in Arabic:</label>
                                        <div class="col-lg-3">
                                            <input type="text" wire:model="name_ar" name="name_ar" class="form-control @error('name_ar') is-invalid @enderror" placeholder="Enter Arabic Name">
                                            <span class="form-text text-muted">Enter Arabic Name</span>
                                            @error('name_ar')
                                            <p class="text-red-500 mt-1">{{ $message }}</p>
                                            @enderror
                                        </div>
                                        <label class="col-lg-2 col-form-label text-lg-right">Name Of City in English:</label>
                                        <div class="col-lg-3">
                                            <input type="text" wire:model="name_en" name="name_en" class="form-control @error('name_en') is-invalid @enderror" placeholder="Enter English Name">
                                            <span class="form-text text-muted">Enter English Name</span>
                                            @error('name_ar')
                                            <p class="text-red-500 mt-1">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label text-lg-right">Governorate:</label>
                                        <div class="col-lg-8">
                                            <select wire:model="governorate_id" name="governorate_id" id="governorate_id" class="form-control @error('governorate_id')border border-red-500 @enderror" data-size="5" data-live-search="true">
                                                <option selected>Select Item</option>
                                                @foreach ($governorates as $item)
                                                <option value="{{$item->id}}">{{$item->name_en}}</option>
                                                @endforeach
                                            </select>
                                            @error('governorate_id')
                                            <p class="text-red-500 mt-1">{{ $message }}</p>
                                            @enderror
                                            <span class="form-text text-muted">Please select Governorate group</span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-1 col-form-label text-lg-right">Featured: <span class="text-danger">*</span></label>
                                        <div class="col-lg-3">
                                            <div class="radio-inline">
                                                <label class="radio radio-solid">
                                                    <input type="radio" name="priority" wire:model="priority" checked="checked" value="1">
                                                    <span></span>Yes</label>
                                                <label class="radio radio-solid">
                                                    <input type="radio" name="priority" wire:model="priority" value="0">
                                                    <span></span>No</label>
                                            </div>
                                            <span class="form-text text-muted">Please select Yes & No</span>
                                        </div>
                                        <label class="col-lg-2 col-form-label text-lg-right">Status:</label>
                                        <div class="col-lg-3">
                                            <div class="radio-inline">
                                                <label class="radio radio-solid">
                                                    <input type="radio" name="status" wire:model="status" checked="checked" value="1">
                                                    <span></span>Active</label>
                                                <label class="radio radio-solid">
                                                    <input type="radio" name="status" wire:model="status" value="0">
                                                    <span></span>In Active</label>
                                            </div>
                                            <span class="form-text text-muted">Please select Status</span>
                                        </div>
                                    </div>

                                   
                                   

                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label text-lg-right">image <span class="text-danger">*</span></label>
                                        <div class="custom-file col-lg-8">
                                            <input required type="file" wire:model="image" name="image"  class="custom-file-input  @error('image') is-invalid @enderror" id="customFile" accept=".png, .jpg, .jpeg" >
                                            <div wire:loading wire:target="image" class="text-sm text-gray-500 italic">Uploading...</div>
                                            
                                            @error('image')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                            @enderror
                                            <label class="custom-file-label" for="customFile">Choose file</label>
                                        </div>
                                        
                                    </div>
                                   
                                    @if ($image)
                                    <div class="form-group row">
                                        Photo Preview:
                                        <div class="col-lg-2">
                                            <img src="{{ $image->temporaryUrl() }}">
                                        </div>
                                    </div>
                                    @endif
                                   
                                </div>
                                <div class="card-footer">
                                    <div class="row">
                                        <div class="col-lg-2"></div>
                                        <div class="col-lg-10">
                                            <button class="btn btn-primary btn-flat" type="submit"><i class="fa fa-save"></i>Submit</button>
                                            {{-- <button  type="button" id="submit" class="btn btn-primary btn-flat "><i class="fa fa-save"></i> Save</button> --}}
                                            <a href="/homeberry-portal/cities" class="btn btn-default btn-flat pull-left"><i class="fa fa-close"></i> Back</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- end: Example Code-->
                        </div>
                        <!--end::Form-->
                    </div>
                </div>
            </div>
        </div>
    </div>

    @section('scripts')
<script>
    let file = document.querySelector('input[type="file"]').files[0]

    // Upload a file:
    @this.upload('photo', file, (uploadedFilename) => {
        // Success callback.
    }, () => {
        // Error callback.
    }, (event) => {
        // Progress callback.
        // event.detail.progress contains a number between 1 and 100 as the upload progresses.
    })

    // Upload multiple files:
    @this.uploadMultiple('photos', [file], successCallback, errorCallback, progressCallback)

    // Remove single file from multiple uploaded files
    @this.removeUpload('photos', uploadedFilename, successCallback)
</script>
    <script>
        window.addEventListener('alert', event => {
            Swal.fire({
                icon: event.detail.type,
                title: event.detail.title,
                text: event.detail.message,
                type: event.detail.type,
               // showCancelButton: true,
                confirmButtonColor: "#F64E60",
                confirmButtonText: "Go To cities Table",
              //  closeOnConfirm: true
              allowOutsideClick: false
            }).then((result) => {
                if (result.isConfirmed) {
                    window.location.href = "/homeberry-portal/cities";
                }
            });
        })
    </script>

    @stop

     <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
         <!--begin::Subheader-->
         <div class="subheader py-2 py-lg-4 subheader-transparent" id="kt_subheader">
             <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                 <!--begin::Details-->
                 <div class="d-flex align-items-center flex-wrap mr-2">
                     <!--begin::Title-->
                     <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Projects</h5>
                     <!--end::Title-->
                     <!--begin::Separator-->
                     <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-5 bg-gray-200"></div>
                     <!--end::Separator-->
                     <!--begin::Search Form-->
                     <div class="d-flex align-items-center" id="kt_subheader_search">
                         <span class="text-dark-50 font-weight-bold" id="kt_subheader_total">690 Total</span>
                         <form class="ml-5">
                             <div class="input-group input-group-sm input-group-solid" style="max-width: 175px">
                                 <input type="text" class="form-control" id="kt_subheader_search_form" placeholder="Search..." />
                                 <div class="input-group-append">
                                     <span class="input-group-text">
                                         <span class="svg-icon">
                                             <!--begin::Svg Icon | path:assets/media/svg/icons/General/Search.svg-->
                                             <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                 <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                     <rect x="0" y="0" width="24" height="24" />
                                                     <path d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                                                     <path d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z" fill="#000000" fill-rule="nonzero" />
                                                 </g>
                                             </svg>
                                             <!--end::Svg Icon-->
                                         </span>
                                         <!--<i class="flaticon2-search-1 icon-sm"></i>-->
                                     </span>
                                 </div>
                             </div>
                         </form>
                     </div>
                     <!--end::Search Form-->
                     <!--begin::Group Actions-->
                     <div class="d-flex- align-items-center flex-wrap mr-2 d-none" id="kt_subheader_group_actions">
                         <div class="text-dark-50 font-weight-bold">
                             <span id="kt_subheader_group_selected_rows">23</span>Selected:</div>
                         <div class="d-flex ml-6">
                             <div class="dropdown mr-2" id="kt_subheader_group_actions_status_change">
                                 <button type="button" class="btn btn-light-primary font-weight-bolder btn-sm dropdown-toggle" data-toggle="dropdown">Update Status</button>
                                 <div class="dropdown-menu p-0 m-0 dropdown-menu-sm">
                                     <ul class="navi navi-hover pt-3 pb-4">
                                         <li class="navi-header font-weight-bolder text-uppercase text-primary font-size-lg pb-0">Change status to:</li>
                                         <li class="navi-item">
                                             <a href="#" class="navi-link" data-toggle="status-change" data-status="1">
                                                 <span class="navi-text">
                                                     <span class="label label-light-success label-inline font-weight-bold">Approved</span>
                                                 </span>
                                             </a>
                                         </li>
                                         <li class="navi-item">
                                             <a href="#" class="navi-link" data-toggle="status-change" data-status="2">
                                                 <span class="navi-text">
                                                     <span class="label label-light-danger label-inline font-weight-bold">Rejected</span>
                                                 </span>
                                             </a>
                                         </li>
                                         <li class="navi-item">
                                             <a href="#" class="navi-link" data-toggle="status-change" data-status="3">
                                                 <span class="navi-text">
                                                     <span class="label label-light-warning label-inline font-weight-bold">Pending</span>
                                                 </span>
                                             </a>
                                         </li>
                                         <li class="navi-item">
                                             <a href="#" class="navi-link" data-toggle="status-change" data-status="4">
                                                 <span class="navi-text">
                                                     <span class="label label-light-info label-inline font-weight-bold">On Hold</span>
                                                 </span>
                                             </a>
                                         </li>
                                     </ul>
                                 </div>
                             </div>
                             <button class="btn btn-light-success font-weight-bolder btn-sm mr-2" id="kt_subheader_group_actions_fetch" data-toggle="modal" data-target="#kt_datatable_records_fetch_modal">Fetch Selected</button>
                             <button class="btn btn-light-danger font-weight-bolder btn-sm mr-2" id="kt_subheader_group_actions_delete_all">Delete All</button>
                         </div>
                     </div>
                     <!--end::Group Actions-->
                 </div>
                 <!--end::Details-->
                 <!--begin::Toolbar-->
                 <div class="d-flex align-items-center">
                     <!--begin::Button-->
                     @can('edit-projects')
                     <a href="{{route('edit-project',$project)}}" class="btn btn-light-primary font-weight-bold ml-2">Edit Project</a>
                     @endcan
                     <!--end::Button-->
                     <a href="/homeberry-portal/projects" class="btn btn-default btn-flat pull-left ml-2"><i class="fa fa-close"></i> Back</a>

                 </div>
                 <!--end::Toolbar-->
             </div>
         </div>
         <!--end::Subheader-->

         <!--begin::Entry-->
         <div class="d-flex flex-column-fluid">
             <!--begin::Container-->
             <div class="container-fluid">
                 <!--begin::Card-->
                 <div class="card card-custom gutter-b">
                     <div class="card-body">
                         <div class="d-flex">
                             <!--begin: Pic-->
                             <div class="flex-shrink-0 mr-7 mt-lg-0 mt-3">
                                 <div class="symbol symbol-50 symbol-lg-120">
                                     <img alt="Pic" src="{{asset($project->medias->first() ? $project->medias->first()->file : 'https://lorempixel.com/640/480/?65383')}}" />
                                 </div>
                               
                             </div>
                             <!--end: Pic-->
                             <!--begin: Info-->
                             <div class="flex-grow-1">
                                 <!--begin: Title-->
                                 <div class="d-flex align-items-center justify-content-between flex-wrap">
                                     <div class="mr-3">
                                         <!--begin::Name-->
                                         <span class="d-flex align-items-center text-dark text-hover-primary font-size-h5 font-weight-bold mr-3">{{$project->name_en}}
                                             <i class="flaticon2-correct text-success icon-md ml-2"></i></span>
                                         <!--end::Name-->
                                         <!--begin::Contacts-->
                                         {{-- <div class="d-flex flex-wrap my-2">
                                             <a href="#" class="text-muted text-hover-primary font-weight-bold mr-lg-8 mr-5 mb-lg-0 mb-2">
                                                 <span class="svg-icon svg-icon-md svg-icon-gray-500 mr-1">
                                                     <!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Mail-notification.svg-->
                                                     <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                         <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                             <rect x="0" y="0" width="24" height="24" />
                                                             <path d="M21,12.0829584 C20.6747915,12.0283988 20.3407122,12 20,12 C16.6862915,12 14,14.6862915 14,18 C14,18.3407122 14.0283988,18.6747915 14.0829584,19 L5,19 C3.8954305,19 3,18.1045695 3,17 L3,8 C3,6.8954305 3.8954305,6 5,6 L19,6 C20.1045695,6 21,6.8954305 21,8 L21,12.0829584 Z M18.1444251,7.83964668 L12,11.1481833 L5.85557487,7.83964668 C5.4908718,7.6432681 5.03602525,7.77972206 4.83964668,8.14442513 C4.6432681,8.5091282 4.77972206,8.96397475 5.14442513,9.16035332 L11.6444251,12.6603533 C11.8664074,12.7798822 12.1335926,12.7798822 12.3555749,12.6603533 L18.8555749,9.16035332 C19.2202779,8.96397475 19.3567319,8.5091282 19.1603533,8.14442513 C18.9639747,7.77972206 18.5091282,7.6432681 18.1444251,7.83964668 Z" fill="#000000" />
                                                             <circle fill="#000000" opacity="0.3" cx="19.5" cy="17.5" r="2.5" />
                                                         </g>
                                                     </svg>
                                                     <!--end::Svg Icon-->
                                                 </span>jason@siastudio.com</a>
                                             <a href="#" class="text-muted text-hover-primary font-weight-bold mr-lg-8 mr-5 mb-lg-0 mb-2">
                                                 <span class="svg-icon svg-icon-md svg-icon-gray-500 mr-1">
                                                     <!--begin::Svg Icon | path:assets/media/svg/icons/General/Lock.svg-->
                                                     <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                         <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                             <mask fill="white">
                                                                 <use xlink:href="#path-1" />
                                                             </mask>
                                                             <g />
                                                             <path d="M7,10 L7,8 C7,5.23857625 9.23857625,3 12,3 C14.7614237,3 17,5.23857625 17,8 L17,10 L18,10 C19.1045695,10 20,10.8954305 20,12 L20,18 C20,19.1045695 19.1045695,20 18,20 L6,20 C4.8954305,20 4,19.1045695 4,18 L4,12 C4,10.8954305 4.8954305,10 6,10 L7,10 Z M12,5 C10.3431458,5 9,6.34314575 9,8 L9,10 L15,10 L15,8 C15,6.34314575 13.6568542,5 12,5 Z" fill="#000000" />
                                                         </g>
                                                     </svg>
                                                     <!--end::Svg Icon-->
                                                 </span>PR Manager</a>
                                             <a href="#" class="text-muted text-hover-primary font-weight-bold">
                                                 <span class="svg-icon svg-icon-md svg-icon-gray-500 mr-1">
                                                     <!--begin::Svg Icon | path:assets/media/svg/icons/Map/Marker2.svg-->
                                                     <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                         <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                             <rect x="0" y="0" width="24" height="24" />
                                                             <path d="M9.82829464,16.6565893 C7.02541569,15.7427556 5,13.1079084 5,10 C5,6.13400675 8.13400675,3 12,3 C15.8659932,3 19,6.13400675 19,10 C19,13.1079084 16.9745843,15.7427556 14.1717054,16.6565893 L12,21 L9.82829464,16.6565893 Z M12,12 C13.1045695,12 14,11.1045695 14,10 C14,8.8954305 13.1045695,8 12,8 C10.8954305,8 10,8.8954305 10,10 C10,11.1045695 10.8954305,12 12,12 Z" fill="#000000" />
                                                         </g>
                                                     </svg>
                                                     <!--end::Svg Icon-->
                                                 </span>Melbourne</a>
                                         </div> --}}
                                         <!--end::Contacts-->
                                     </div>

                                 </div>
                                 <!--end: Title-->
                                 <!--begin: Content-->
                                 <div class="d-flex align-items-center flex-wrap justify-content-between">
                                     <div class="flex-grow-1 font-weight-bold text-dark-50 py-5 py-lg-2 mr-5">{{$project->description_en}}</div>

                                 </div>
                                 <!--end: Content-->
                             </div>
                             <div class="flex-grow-1">
                                 <!--begin: Title-->
                                 <div class="d-flex align-items-center justify-content-between flex-wrap">
                                     <div class="mr-3">
                                         <!--begin::Name-->
                                         <span class="d-flex align-items-center text-dark text-hover-primary font-size-h5 font-weight-bold mr-3">{{$project->name_ar}}
                                         </span>
                                         <!--end::Name-->
                                     </div>

                                 </div>
                                 <!--end: Title-->
                                 <!--begin: Content-->
                                 <div class="d-flex align-items-center flex-wrap justify-content-between">
                                     <div class="flex-grow-1 font-weight-bold text-dark-50 py-5 py-lg-2 mr-5">{{$project->description_ar}}</div>

                                 </div>
                                 <!--end: Content-->
                             </div>
                             <!--end: Info-->
                         </div>
                         <div class="separator separator-solid my-7"></div>
                         <div class="mb-7 row ">
                             <div class="col-6">

                                 <div class="d-flex justify-content-between align-items-cente my-1">
                                     <span class="text-dark-75 font-weight-bolder mr-2">City:</span>
                                     <span class="text-muted text-hover-primary">{{$project->city->name_en}}</span>
                                 </div>
                                 <div class="d-flex justify-content-between align-items-center">
                                     <span class="text-dark-75 font-weight-bolder mr-2">Developer Name:</span>
                                     <span class="text-muted text-hover-primary">{{$project->devolper->name}}</span>
                                 </div>

                                 <div class="d-flex justify-content-between align-items-center">
                                     <span class="text-dark-75 font-weight-bolder mr-2">Types :</span>
                                     <span class="text-muted text-hover-primary">
                                         @foreach($project->activitytypes as $item)
                                          <span> {{$item->name_en}} , </span>
                                        @endforeach
                                     </span>
                                 </div>
                                 <div class="d-flex justify-content-between align-items-cente my-1">
                                     <span class="text-dark-75 font-weight-bolder mr-2">Views:</span>
                                     <span class="label label-warning label-pill label-inline mr-2">{{$project->views}}</span>
                                     
                                 </div> 
                                 <div class="d-flex justify-content-between align-items-cente my-1">
                                     <span class="text-dark-75 font-weight-bolder mr-2">Clicked:</span>
                                     <span class="label label-rounded label-primary mr-2">{{$project->clicked}}</span>

                                 
                                 </div> 
                                 <div class="d-flex justify-content-between align-items-cente my-1">
                                     <span class="text-dark-75 font-weight-bolder mr-2">Constructed:</span>
                                     <span class="label label-lg label-light-success label-inline">{{$project->is_constructed == 1 ? 'Constructed' : 'Not Constructed'}}</span>
                                 </div>
                                 
                                 <div class="d-flex justify-content-between align-items-center">
                                     <span class="text-dark-75 font-weight-bolder mr-2">Created Date:</span>
                                     <span class="text-danger text-hover-primary">{{$project->created_at}}</span>
                                 </div>

                             </div>
                             <div class="col-6">
                                <div class="text-center">
                                <div class="d-flex justify-content-between align-items-cente my-1">
                                    <span class="text-dark-75 font-weight-bolder mr-2">Furnished Types:</span>
                                    @foreach($project->furnishedTypes as $item)
                                    <span class="label label-lg label-light-warning label-inline"> {{$item->name_en}} </span>
                                    @endforeach
                                </div> 
                                  <div class="d-flex justify-content-between align-items-cente my-1">
                                    <span class="text-dark-75 font-weight-bolder mr-2">Delivery:</span>
                                    <span class="label label-lg label-light-info label-inline"> {{$project->delivery}} </span>
                                </div>
                                <div class="d-flex justify-content-between align-items-cente my-1">
                                    <span class="text-dark-75 font-weight-bolder mr-2">Virtual Tour Url:</span>
                                    <span class="label label-lg label-light-info label-inline"> {{$project->virtual_tour_url ?? " "}} </span>
                                </div>
                                <div class="d-flex justify-content-between align-items-cente my-1">
                                    <span class="text-dark-75 font-weight-bolder mr-2">Youtube:</span>
                                    <span class="label label-lg label-light-info label-inline"> {{$project->youtube ?? " "}} </span>
                                </div>
                                <div class="d-flex justify-content-between align-items-cente my-1">
                                    <span class="text-dark-75 font-weight-bolder mr-2">Link:</span>
                                    <span class="label label-lg label-light-info label-inline"> {{$project->link ?? " "}} </span>
                                </div>
                             

                            </div>
                            </div>
                         </div>
                         <div class="separator separator-solid my-7"></div>
                         <div class="flex-shrink-0 mr-7 mt-lg-0 mt-3">
                            <div class="symbol symbol-50 symbol-lg-120">
                                @foreach ($project->medias as $item)
                                <img alt="Pic" src="{{asset($item->file)}}" />
                                @endforeach
                            </div>
                        </div>


                     </div>
                 </div>
                 <!--end::Card-->

             </div>
             <!--end::Container-->
         </div>
         <!--end::Entry-->
     </div>



     @section('scripts')

     <!--begin::Page Scripts(used by this page)-->
     <script src="assets/js/pages/widgets.js"></script>
     <!--end::Page Scripts-->

     @stop

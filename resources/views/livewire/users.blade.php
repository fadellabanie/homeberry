<div>
    <div class="d-flex flex-column-fluid">
       <!--begin::Container-->
       <div class="container">
          <div class="subheader py-2 py-lg-4 subheader-transparent" id="kt_subheader">
             <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Details-->
                <div class="d-flex align-items-center flex-wrap mr-2">
                   <!--begin::Title-->
                   <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Users</h5>
                   <!--end::Title-->
                   <!--begin::Separator-->
                   <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-5 bg-gray-200"></div>
                   <!--end::Separator-->
                   <!--begin::Search Form-->
                   <div class="d-flex align-items-center" id="kt_subheader_search">
                      <span class="text-dark-50 font-weight-bold" id="kt_subheader_total">{{$users->total()}}</span>
                      <form class="ml-5">
                         <div class="input-group input-group-sm input-group-solid" style="max-width: 175px">
                            <input wire:model="search" type="text" class="form-control" id="kt_subheader_search_form" placeholder="Search...">
                            <div class="input-group-append">
                               <span class="input-group-text">
                                  <span class="svg-icon">
                                     <!--begin::Svg Icon | path:assets/media/svg/icons/General/Search.svg-->
                                     <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                           <rect x="0" y="0" width="24" height="24"></rect>
                                           <path d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                                           <path d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z" fill="#000000" fill-rule="nonzero"></path>
                                        </g>
                                     </svg>
                                     <!--end::Svg Icon-->
                                  </span>
                                  <!--<i class="flaticon2-search-1 icon-sm"></i>-->
                               </span>
                            </div>
                         </div>
                      </form>
                   </div>
                   <!--end::Search Form-->
                </div>
                <!--end::Details-->
                <!--begin::Toolbar-->
                <div class="d-flex align-items-center">
                   <!--begin::Button-->
                  
                   <!--end::Button-->
                   <!--begin::Button-->
                   <a href="{{route('export-user')}}" class="btn btn-light-success font-weight-bold ml-2 mr-2">Export</a>
                   <!--end::Button-->
                   @can('all-groups')
                   <a href="{{route('groups')}}" class="btn btn-primary btn-fixed-height font-weight-bold px-2 px-lg-5 mr-2">Groups</a>
                   @endcan
                   @can('all-permissions')
                   <a href="{{route('permissions')}}" class="btn btn-primary btn-fixed-height font-weight-bold px-2 px-lg-5 mr-2">Permissions</a>
                   @endcan
                   @can('add-user')
                   <a href="{{route('add-users')}}" class="btn btn-light-success font-weight-bold ml-2 mr-2">Add User</a>
                  @endcan
                  </div>
                <!--end::Toolbar-->
             </div>
          </div>
          <div class="container">
             @foreach($users as $user)
             <div class="card card-custom gutter-b">
                <div class="card-body">
                   <!--begin::Top-->
                   <div class="d-flex">
                      <!--begin::Pic-->
                      <div class="flex-shrink-0 mr-7">
                         <div class="symbol symbol-50 symbol-lg-120 symbol-light-danger">
                            <span class="font-size-h3 symbol-label font-weight-boldest">{{strtoupper(Str::substr($user->name,0,3))}}</span>
                         </div>
                      </div>
                      <!--end::Pic-->
                      <!--begin: Info-->
                      <div class="flex-grow-1">
                         <!--begin::Title-->
                         <div class="d-flex align-items-center justify-content-between flex-wrap mt-2">
                            <!--begin::User-->
                            <div class="mr-3">
                               <!--begin::Name-->
                               <a href="{{route('edit-users',$user->id)}}" class="d-flex align-items-center text-dark text-hover-primary font-size-h5 font-weight-bold mr-3">{{$user->name.' '.$user->last_name}}
                               <i class="flaticon2-correct text-success icon-md ml-2"></i></a>
                               <!--end::Name-->
                               <!--begin::Contacts-->
                               <div class="d-flex flex-wrap my-2">
                                  <span class="text-muted text-hover-primary font-weight-bold mr-lg-8 mr-5 mb-lg-0 mb-2">
                                     <span class="svg-icon svg-icon-md svg-icon-gray-500 mr-1">
                                        <!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Mail-notification.svg-->
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                           <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                              <rect x="0" y="0" width="24" height="24"></rect>
                                              <path d="M21,12.0829584 C20.6747915,12.0283988 20.3407122,12 20,12 C16.6862915,12 14,14.6862915 14,18 C14,18.3407122 14.0283988,18.6747915 14.0829584,19 L5,19 C3.8954305,19 3,18.1045695 3,17 L3,8 C3,6.8954305 3.8954305,6 5,6 L19,6 C20.1045695,6 21,6.8954305 21,8 L21,12.0829584 Z M18.1444251,7.83964668 L12,11.1481833 L5.85557487,7.83964668 C5.4908718,7.6432681 5.03602525,7.77972206 4.83964668,8.14442513 C4.6432681,8.5091282 4.77972206,8.96397475 5.14442513,9.16035332 L11.6444251,12.6603533 C11.8664074,12.7798822 12.1335926,12.7798822 12.3555749,12.6603533 L18.8555749,9.16035332 C19.2202779,8.96397475 19.3567319,8.5091282 19.1603533,8.14442513 C18.9639747,7.77972206 18.5091282,7.6432681 18.1444251,7.83964668 Z" fill="#000000"></path>
                                              <circle fill="#000000" opacity="0.3" cx="19.5" cy="17.5" r="2.5"></circle>
                                           </g>
                                        </svg>
                                        <!--end::Svg Icon-->
                                     </span>
                                     {{$user->email ?? ''}}
                                  </span>
                                  <span class="text-muted text-hover-primary font-weight-bold mr-lg-8 mr-5 mb-lg-0 mb-2">
                                  <span class="text-muted text-hover-primary font-weight-bold">
                                     <span class="svg-icon svg-icon-md svg-icon-gray-500 mr-1">
                                        <!--begin::Svg Icon | path:assets/media/svg/icons/Map/Marker2.svg-->
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                           <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                              <rect x="0" y="0" width="24" height="24"></rect>
                                              <path d="M9.82829464,16.6565893 C7.02541569,15.7427556 5,13.1079084 5,10 C5,6.13400675 8.13400675,3 12,3 C15.8659932,3 19,6.13400675 19,10 C19,13.1079084 16.9745843,15.7427556 14.1717054,16.6565893 L12,21 L9.82829464,16.6565893 Z M12,12 C13.1045695,12 14,11.1045695 14,10 C14,8.8954305 13.1045695,8 12,8 C10.8954305,8 10,8.8954305 10,10 C10,11.1045695 10.8954305,12 12,12 Z" fill="#000000"></path>
                                           </g>
                                        </svg>
                                        <!--end::Svg Icon-->
                                     </span>
                                     {{$user->country_code}}
                                  </span>
                               </div>
                               <!--end::Contacts-->
                            </div>
                            <!--begin::User-->
                            @can('edit-user')
                            <div class="my-lg-0 my-1">
                               <a href="{{route('edit-users',$user->id)}}" class="btn btn-sm btn-light-primary font-weight-bolder text-uppercase mr-2">Edit</a>
                            </div>
                            @endcan
                         </div>
                         <!--end::Title-->
                         <!--begin::Content-->
                         <div class="d-flex align-items-center flex-wrap justify-content-between">
                            <div class="d-flex justify-content-between align-items-cente my-1">
                               <span class="text-dark-75 font-weight-bolder mr-2">Phone:</span>
                               <span class="text-muted text-hover-primary">{{$user->mobile}}</span>
                            </div>
                            <div class="d-flex justify-content-between align-items-cente my-1">
                               <span class="text-dark-75 font-weight-bolder mr-2">Whatsapp:</span>
                               <span class="text-muted text-hover-primary">{{$user->whatsapp_mobile}}</span>
                            </div>
                            <!--begin::Progress-->
                            <div class="d-flex mt-4 mt-sm-0">
                               <span class="font-weight-bold mr-4">Number Of Property</span>
                              
                               <span class="label label-lg label-light-success label-inline">{{$user->property_count ?? 0}}</span>
                            </div>
                            <!--end::Progress-->
                         </div>
                         <!--end::Content-->
                      </div>
                      <!--end::Info-->
                   </div>
                   <!--end::Top-->
                   <!--begin::Separator-->
                   <div class="separator separator-solid my-7"></div>
                   <!--end::Separator-->
                </div>
             </div>
             @endforeach
             <div class="card card-custom">
                <div class="card-body py-7">
                   <!--begin::Pagination-->
                   <div class="d-flex justify-content-between align-items-center flex-wrap">
                      <div class="d-flex flex-wrap mr-3">
                         {{ $users->links() }}
                      </div>
                   </div>
                   <!--end:: Pagination-->
                </div>
             </div>
          </div>
       </div>
    </div>
 </div>
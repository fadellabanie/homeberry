<div>

    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <div class="subheader py-3 py-lg-8 subheader-transparent" id="kt_subheader">
                <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                    <!--begin::Info-->
                    <div class="d-flex align-items-center mr-1">
                        <!--begin::Page Heading-->
                        <div class="d-flex align-items-baseline flex-wrap mr-5">
                            <!--begin::Page Title-->
                            <h2 class="d-flex align-items-center text-dark font-weight-bold my-1 mr-3">Photo Sessions</h2>
                            <!--end::Page Title-->
                            <!--begin::Breadcrumb-->
                            <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold my-2 p-0">
                                <li class="breadcrumb-item">
                                    <a href="" class="text-muted">Photo Sessions List</a>
                                </li>
                              
                            </ul>
                            <!--end::Breadcrumb-->
                        </div>
                        <!--end::Page Heading-->
                    </div>
                </div>
            </div>
            <!--begin::Notice-->
            <div class="alert alert-custom alert-white alert-shadow gutter-b" role="alert">
                <div class="alert-icon">
                    <span class="svg-icon svg-icon-primary svg-icon-xl">
                        <!--begin::Svg Icon | path:assets/media/svg/icons/Tools/Compass.svg-->
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect x="0" y="0" width="24" height="24"></rect>
                                <path d="M7.07744993,12.3040451 C7.72444571,13.0716094 8.54044565,13.6920474 9.46808594,14.1079953 L5,23 L4.5,18 L7.07744993,12.3040451 Z M14.5865511,14.2597864 C15.5319561,13.9019016 16.375416,13.3366121 17.0614026,12.6194459 L19.5,18 L19,23 L14.5865511,14.2597864 Z M12,3.55271368e-14 C12.8284271,3.53749572e-14 13.5,0.671572875 13.5,1.5 L13.5,4 L10.5,4 L10.5,1.5 C10.5,0.671572875 11.1715729,3.56793164e-14 12,3.55271368e-14 Z" fill="#000000" opacity="0.3"></path>
                                <path d="M12,10 C13.1045695,10 14,9.1045695 14,8 C14,6.8954305 13.1045695,6 12,6 C10.8954305,6 10,6.8954305 10,8 C10,9.1045695 10.8954305,10 12,10 Z M12,13 C9.23857625,13 7,10.7614237 7,8 C7,5.23857625 9.23857625,3 12,3 C14.7614237,3 17,5.23857625 17,8 C17,10.7614237 14.7614237,13 12,13 Z" fill="#000000" fill-rule="nonzero"></path>
                            </g>
                        </svg>
                        <!--end::Svg Icon-->
                    </span>
                </div>
                <div class="alert-text">
                    <b class="mr-2">Photo Sessions</b>List Photo Sessions.
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="flex flex-col mt-8">
                            <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                                <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                                    <div class="flex items-center justify-between">
                                        <div class="max-w-lg w-full lg:max-w-xs">
                                            <label for="search" class="sr-only">Search</label>
                                            <div class="relative">
                                                <div class="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                                                    <svg class="h-5 w-5 text-gray-400" fill="currentColor" viewBox="0 0 20 20">
                                                        <path fill-rule="evenodd" d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z" clip-rule="evenodd"></path>
                                                    </svg>
                                                </div>
                                                <input wire:model="search" id="search" class="block w-full pl-10 pr-3 py-2 border border-gray-300 rounded-md leading-5 bg-white placeholder-gray-500 focus:outline-none focus:placeholder-gray-400 focus:border-blue-300 focus:shadow-outline-blue sm:text-sm transition duration-150 ease-in-out" placeholder="Search" type="search">
                                            </div>
                                        </div>
                                        <div class="checkbox-inline">
                                        
                                            <label class="checkbox checkbox-success">
                                                <input wire:model="active" id="active" type="checkbox" name="active">
                                                <span></span>Active</label>
                                        </div>

                                    </div>

                                    <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg mt-4">

                                        <table class="min-w-full divide-y divide-gray-200">
                                            <thead>
                                                <tr>
                                                    <th class="px-6 py-3 text-left">
                                                        <div class="flex items-center">
                                                            <button class="bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">USER</button>
                                                        </div>
                                                    </th>
                                                    {{-- <th class="px-6 py-3 text-left">
                                                        <div class="flex items-center">
                                                            <button class="bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">Propery</button>
                                                        </div>
                                                    </th> --}}
                                                    <th class="px-6 py-3 text-left">
                                                        <div class="flex items-center">
                                                            <button class="bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">PACKAGE</button>
                                                        </div>
                                                    </th>
                                                    <th class="px-6 py-3 text-left">
                                                        <div class="flex items-center">
                                                            <button wire:click="sortBy('date')" class="bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">DATE</button>
                                                            <x-sort-icon field="date" :sortField="$sortField" :sortAsc="$sortAsc" />
                                                        </div>
                                                    </th>
                                                    <th class="px-6 py-3 text-left">
                                                        <div class="flex items-center">
                                                            <button wire:click="sortBy('time')" class="bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">TIME</button>
                                                            <x-sort-icon field="time" :sortField="$sortField" :sortAsc="$sortAsc" />
                                                        </div>
                                                    </th>
                                                    <th class="px-6 py-3 text-left">
                                                        <div class="flex items-center">
                                                            <button class="bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">Location</button>
                                                        </div>
                                                    </th>
                                                    <th class="px-6 py-3 text-left">
                                                        <div class="flex items-center">
                                                            <button class="bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">Status</button>
                                                        </div>
                                                    </th>
                                                     <th class="px-6 py-3 text-left">
                                                        <div class="flex items-center">
                                                            <button class="bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">Created</button>
                                                        </div>
                                                    </th> 
                                                    <th class="px-6 py-3 text-left">
                                                        <div class="flex items-center">
                                                            <button class="bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">Approve</button>
                                                        </div>
                                                    </th>


                                                    <th class="px-6 py-3 bg-gray-50"></th>
                                                </tr>
                                            </thead>
                                            <tbody class="bg-white divide-y divide-gray-200">
                                                @foreach ($PhotoSessions as $item)
                                                <tr>

                                                    <td class="w-4/12 px-6 py-4 whitespace-no-wrap">
                                                        <div class="flex items-center">
                                                            <div class="ml-4">
                                                                <div class="text-sm leading-5 font-medium text-gray-900">
                                                                    {{ $item->user->name ?? $item->user->mobile }}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>

                                                    {{-- <td class="w-4/12 px-6 py-4 whitespace-no-wrap">
                                                        <div class="text-sm leading-5 text-gray-900">
                                                            {{ $item->property->name_en === null ? $item->property->name_ar : $item->property->name_en}}
                                                        </div>
                                                    </td> --}}

                                                    <td class="w-4/12 px-6 py-4 whitespace-no-wrap">
                                                        <div class="text-sm leading-5 text-gray-900">
                                                            {{ $item->package->name_en ?? "" }}
                                                        </div>
                                                    </td>

                                                    <td class="w-4/12 px-6 py-4 whitespace-no-wrap">
                                                        <div class="text-sm leading-5 text-gray-900">
                                                            {{ $item->date }}
                                                        </div>
                                                    </td>

                                                    <td class="w-4/12 px-6 py-4 whitespace-no-wrap">
                                                        <div class="text-sm leading-5 text-gray-900">
                                                            {{ $item->time}}
                                                        </div>
                                                    </td>

                                                    <td class="w-4/12 px-6 py-4 whitespace-no-wrap">
                                                        <div class="text-sm leading-5 text-gray-900">
                                                            {{ $item->location_name}}
                                                        </div>
                                                    </td>
                                                    <td class="px-6 py-4 whitespace-no-wrap">
                                                        @if ($item->status)
                                                        <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800">
                                                            Active
                                                        </span>
                                                        {{-- <span class="inline-flex items-center px-2.5 py-0.5 rounded-full text-xs font-medium leading-4 bg-red-100 text-red-800">
                                        Inactive
                                    </span> --}}
                                                        @else
                                                        <span class="inline-flex items-center px-2.5 py-0.5 rounded-full text-xs font-medium leading-4 bg-red-100 text-red-800">
                                                            Inactive
                                                        </span>
                                                        @endif
                                                    </td>
                                                      <td class="w-4/12 px-6 py-4 whitespace-no-wrap">
                                                        <div class="text-sm leading-5 text-gray-900">
                                                            {{ $item->created_at->diffForHumans()}}
                                                        </div>
                                                    </td> 
                                                    
                                                    <td class="w-4/12 px-6 py-4 whitespace-no-wrap">
                                                        <div class="text-sm leading-5 text-gray-900">
                                                            @if($item->status)
                                                            {{-- Refuse --}} Done
                                                            @else
                                                            <a href=""  wire:click="approve({{$item->id}})"><span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800">
                                                                Approve
                                                            </span></a>
                                                          
                                                            @endif
                                                        </div>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>

                                    <div class="mt-8">
                                        {{ $PhotoSessions->links() }}
                                    </div>
                                </div>
                            </div>
                            <div class="h-96"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div>
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <div class="subheader py-3 py-lg-8 subheader-transparent" id="kt_subheader">
                <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                    <!--begin::Info-->
                    <div class="d-flex align-items-center mr-1">
                        <!--begin::Page Heading-->
                        <div class="d-flex align-items-baseline flex-wrap mr-5">
                            <!--begin::Page Title-->
                            <h2 class="d-flex align-items-center text-dark font-weight-bold my-1 mr-3">Properties</h2>
                            <!--end::Page Title-->
                            <!--begin::Breadcrumb-->
                            <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold my-2 p-0">
                                <li class="breadcrumb-item">
                                    <a href="/homeberry-portal/properties" class="text-muted">Properties List</a>
                                </li>
                                <li class="breadcrumb-item">
                                    <span class="text-muted">Add Property</span>
                                </li>
                            </ul>
                            <!--end::Breadcrumb-->
                        </div>
                        <!--end::Page Heading-->
                    </div>
                </div>
            </div>
            <!--begin::Notice-->
            <div class="alert alert-custom alert-white alert-shadow gutter-b" role="alert">
                <div class="alert-icon">
                    <span class="svg-icon svg-icon-primary svg-icon-xl">
                        <!--begin::Svg Icon | path:assets/media/svg/icons/Tools/Compass.svg-->
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect x="0" y="0" width="24" height="24"></rect>
                                <path d="M7.07744993,12.3040451 C7.72444571,13.0716094 8.54044565,13.6920474 9.46808594,14.1079953 L5,23 L4.5,18 L7.07744993,12.3040451 Z M14.5865511,14.2597864 C15.5319561,13.9019016 16.375416,13.3366121 17.0614026,12.6194459 L19.5,18 L19,23 L14.5865511,14.2597864 Z M12,3.55271368e-14 C12.8284271,3.53749572e-14 13.5,0.671572875 13.5,1.5 L13.5,4 L10.5,4 L10.5,1.5 C10.5,0.671572875 11.1715729,3.56793164e-14 12,3.55271368e-14 Z" fill="#000000" opacity="0.3"></path>
                                <path d="M12,10 C13.1045695,10 14,9.1045695 14,8 C14,6.8954305 13.1045695,6 12,6 C10.8954305,6 10,6.8954305 10,8 C10,9.1045695 10.8954305,10 12,10 Z M12,13 C9.23857625,13 7,10.7614237 7,8 C7,5.23857625 9.23857625,3 12,3 C14.7614237,3 17,5.23857625 17,8 C17,10.7614237 14.7614237,13 12,13 Z" fill="#000000" fill-rule="nonzero"></path>
                            </g>
                        </svg>
                        <!--end::Svg Icon-->
                    </span>
                </div>
                <div class="alert-text">
                    <b class="mr-2">Properties</b>Add Property.
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <!--begin::Card-->
                        <div class="card card-custom gutter-b example example-compact">
                            <form wire:submit.prevent="submitForm" action="/addProperty" method="POST" class="grid grid-cols-1 row-gap-6">
                                @csrf
                                @if (session()->has('message'))
                                <div class="alert alert-success">
                                    {{ session('message') }}
                                </div>
                                @endif
                                <div class="ml-3">
                                    <p class="text-sm leading-5 font-medium text-green-800">
                                        {{ $successMessage }}
                                    </p>
                                </div>
                               
                                <div class="card-body">
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label text-lg-right"> Name Of Properties in Arabic: <span class="text-danger">*</span></label>
                                        <div class="col-lg-3">
                                            <input type="text" wire:model="name_ar" name="name_ar" class="form-control @error('name_ar') is-invalid @enderror" placeholder="Enter Arabic Name">
                                            @error('name_ar')
                                            <p class="text-red-500 mt-1">{{ $message }}</p>
                                            @enderror
                                        </div>
                                        <label class="col-lg-2 col-form-label text-lg-right">Name Of Properties in English: <span class="text-danger">*</span></label>
                                        <div class="col-lg-3">
                                            <input type="text" wire:model="name_en" name="name_en" class="form-control @error('name_en') is-invalid @enderror" placeholder="Enter English Name">
                                            @error('name_ar')
                                            <p class="text-red-500 mt-1">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">

                                        <label class="col-lg-2 col-form-label text-lg-right"> Arabic Description: <span class="text-danger">*</span></label>
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <textarea wire:model="description_ar" name="description_ar" class="form-control" rows="3"></textarea>
                                            </div>
                                            @error('description_ar')
                                            <p class="text-red-500 mt-1">{{ $message }}</p>
                                            @enderror
                                        </div>

                                        <label class="col-lg-2 col-form-label text-lg-right"> English Description: <span class="text-danger">*</span></label>
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <textarea wire:model="description_en" name="description_en" class="form-control" rows="3"></textarea>
                                            </div>
                                            @error('description_en')
                                            <p class="text-red-500 mt-1">{{ $message }}</p>
                                            @enderror
                                        </div>

                                    </div>
                                    <div class="separator separator-solid my-7"></div>
                                    <div class="form-group row">
                                        <label class="col-lg-1 col-form-label text-lg-right"> Type: <span class="text-danger">*</span></label>
                                        <div class="col-lg-5">
                                            <div class="radio-inline">

                                                <label class="radio radio-primary">
                                                    <input type="radio" name="type" value="buy" wire:model="type" checked="checked">
                                                    <span></span>Buy</label>
                                                    
                                                    <label class="radio radio-primary">
                                                    <input type="radio"name="type"  value="rent" wire:model="type"  checked="checked">
                                                    <span></span>Rent</label>
                                              
                                                @error('type')
                                                <p class="text-red-500 mt-1">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="separator separator-solid my-7"></div>
                                    <div class="form-group row">
                                        <label class="col-lg-1 col-form-label text-lg-right"> Price: <span class="text-danger">*</span></label>
                                        <div class="col-lg-3">
                                            <input type="number" wire:model="price" name="price" class="form-control @error('price') is-invalid @enderror" placeholder="Enter Price" min="0">
                                            @error('price')
                                            <p class="text-red-500 mt-1">{{ $message }}</p>
                                            @enderror
                                        </div>
                                        <label class="col-lg-1 col-form-label text-lg-right">Guest: <span class="text-danger">*</span></label>
                                        <div class="col-lg-3">
                                            <input type="number" wire:model="guest" name="guest" class="form-control @error('guest') is-invalid @enderror" placeholder="Enter num of guests" min="0">
                                            @error('guest')
                                            <p class="text-red-500 mt-1">{{ $message }}</p>
                                            @enderror
                                        </div>
                                        <label class="col-lg-1 col-form-label text-lg-right">Room: <span class="text-danger">*</span></label>
                                        <div class="col-lg-3">
                                            <input type="number" wire:model="room" name="room" class="form-control @error('room') is-invalid @enderror" placeholder="Enter Room" min="0">
                                            @error('room')
                                            <p class="text-red-500 mt-1">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-1 col-form-label text-lg-right">Bathroom: <span class="text-danger">*</span></label>
                                        <div class="col-lg-3">
                                            <input type="number" wire:model="bathroom" name="bathroom" class="form-control @error('bathroom') is-invalid @enderror" placeholder="Enter Bathroom" min="0">
                                            @error('bathroom')
                                            <p class="text-red-500 mt-1">{{ $message }}</p>
                                            @enderror
                                        </div>
                                        <label class="col-lg-1 col-form-label text-lg-right">Park: <span class="text-danger">*</span></label>
                                        <div class="col-lg-3">
                                            <input type="number" wire:model="park" name="park" class="form-control @error('park') is-invalid @enderror" placeholder="Enter park" min="0">
                                            @error('park')
                                            <p class="text-red-500 mt-1">{{ $message }}</p>
                                            @enderror
                                        </div>
                                       
                                        <label class="col-lg-1 col-form-label text-lg-right">Furnished: <span class="text-danger">*</span></label>
                                        <div class="col-lg-3 ">
                                            <select wire:model="furnished" name="furnished" class="form-control @error('furnished') is-invalid @enderror" data-live-search="true">
                                                <option value="" selected>Select</option>
                                                <option value="furnished">Furnished</option>
                                                <option value="semi furnished">Semi Furnished</option>
                                                <option value="unfurnished">Unfurnished</option>
                                            </select>
                                            @error('furnished')
                                            <p class="text-red-500 mt-1">{{ $message }}</p>
                                            @enderror
                                        </div>

                                    </div>
                                    <div class="separator separator-solid my-7"></div>
                                    <div class="form-group row">
                                       
                                        <label class="col-lg-1 col-form-label text-lg-right">Payment: <span class="text-danger">*</span></label>
                                        <div class="col-lg-3">
                                            <select wire:model="payment_id" name="payment_id" class="form-control @error('payment_id') is-invalid @enderror" data-live-search="true">
                                                <option value="" selected>Select</option>
                                                @foreach ($payments as $item)
                                                <option value="{{$item->id}}">{{$item->name_en}}</option>
                                                @endforeach
                                            </select>
                                            @error('payment_id')
                                            <p class="text-red-500 mt-1">{{ $message }}</p>
                                            @enderror
                                        </div>


                                        <label class="col-lg-1 col-form-label text-lg-right">Cities: <span class="text-danger">*</span></label>
                                        <div class="col-lg-3">
                                            <select wire:model="city_id" name="city_id" class="form-control @error('city_id') is-invalid @enderror" data-live-search="true">
                                                <option value="" selected>Select</option>
                                                @foreach ($cities as $item)
                                                <option value="{{$item->id}}">{{$item->name_en}}</option>
                                                @endforeach
                                            </select>
                                            @error('city_id')
                                            <p class="text-red-500 mt-1">{{ $message }}</p>
                                            @enderror
                                        </div>
                                        <label class="col-lg-1 col-form-label text-lg-right">Activity Types: <span class="text-danger">*</span></label>
                                        <div class="col-lg-3">
                                            <select wire:model="activity_type_id" name="activity_type_id" class="form-control  @error('activity_type_id') is-invalid @enderror" data-live-search="true">
                                                <option value="" selected>Select</option>
                                                @foreach ($activity_types as $item)
                                                <option value="{{$item->id}}">{{$item->name_en}}</option>
                                                @endforeach
                                            </select>
                                            @error('activity_type_id')
                                            <p class="text-red-500 mt-1">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    @if($type =="rent")
                                    <div class="separator separator-solid my-7"></div>
                                    <div class="form-group row">
                                        <label class="col-lg-1 col-form-label text-lg-right">Rental: <span class="text-danger">*</span></label>
                                        <div class="col-lg-3">
                                            <select wire:model="rental" name="rental" class="form-control @error('rental') is-invalid @enderror" data-live-search="true">
                                               <option value="" selected>Select</option>
                                                    <option value="daily">Daily</option>
                                                    <option value="weekly">Weekly</option>
                                                    <option value="monthly">Monthly</option>
                                                    <option value="yearly">Yearly</option>
                                            </select>
                                            @error('rental')
                                            <p class="text-red-500 mt-1">{{ $message }}</p>
                                            @enderror
                                        </div>
                                      <label class="col-lg-1 col-form-label text-lg-right">Availability: <span class="text-danger">*</span></label>
                                        <div class="col-lg-3">
                                            <input wire:model="availability" name="availability" type="date" class="form-control @error('availability') is-invalid @enderror" placeholder="Select date">
                                            @error('availability')
                                            <div class="invalid-feedback" required>
                                                {{ $message }}
                                            </div>
                                            @enderror
                                        </div>
                                    </div>
                                    @endif
                                    <div class="separator separator-solid my-7"></div>
                                    <div class="form-group row">
                                        <label class="col-lg-1 col-form-label text-lg-right"> Area: <span class="text-danger">*</span></label>
                                        <div class="col-lg-3">
                                            <input type="number" wire:model="area" name="area" class="form-control @error('area') is-invalid @enderror" placeholder="Enter Area" min="0">
                                            @error('area')
                                            <p class="text-red-500 mt-1">{{ $message }}</p>
                                            @enderror
                                        </div>
                                        {{-- <label class="col-lg-1 col-form-label text-lg-right"> Link: <span class="text-danger">*</span></label>
                                        <div class="col-lg-3">
                                            <input type="url" wire:model="url" name="url" class="form-control @error('url') is-invalid @enderror" placeholder="Enter Link" min="0">
                                            @error('url')
                                            <p class="text-red-500 mt-1">{{ $message }}</p>
                                            @enderror
                                        </div> --}}

                                    </div>
                                  
                                    <div class="separator separator-solid my-7"></div>
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label text-lg-right"> Location name: <span class="text-danger">*</span></label>
                                        <div class="col-lg-3">
                                            <input type="text" wire:model="location_name" name="location_name" class="form-control @error('location_name') is-invalid @enderror" placeholder="Enter location name" min="0">
                                            @error('location_name')
                                            <p class="text-red-500 mt-1">{{ $message }}</p>
                                            @enderror
                                        </div>
                                        <label class="col-lg-1 col-form-label text-lg-right"> Youtube: <span class="text-danger">*</span></label>
                                        <div class="col-lg-3">
                                            <input type="url" wire:model="youtube" name="youtube" class="form-control @error('youtube') is-invalid @enderror" placeholder="Enter youtube" >
                                            @error('youtube')
                                            <p class="text-red-500 mt-1">{{ $message }}</p>
                                            @enderror
                                        </div>

                                    </div>
                                    <div class="separator separator-solid my-7"></div>

                                    <div class="form-group row">

                                        <label class="col-lg-2 col-form-label text-lg-right" for="amenities"> Amenities </label>
                                        <div class="col-lg-4">
                                            <select multiple="multiple" wire:model="amenities" name="amenities" class="form-control " data-live-search="true" id="amenities">
                                                @foreach ($amenitiesArr as $item)
                                                <option value="{{$item->id}}">{{$item->name_en}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <label class="col-lg-2 col-form-label text-lg-right" for="services"> Near By Services </label>
                                        <div class="col-lg-4">
                                            <select multiple="multiple" wire:model="services" name="services" class="form-control " data-live-search="true" id="services">
                                                @foreach ($servicesArr as $item)
                                                <option value="{{$item->id}}">{{$item->name_en}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="separator separator-solid my-7"></div>

                                    <div class="form-group row">

                                        <label class="col-lg-1 col-form-label text-lg-right">Status: <span class="text-danger">*</span></label>
                                        <div class="col-lg-3">
                                            <div class="radio-inline">
                                                <label class="radio radio-solid">
                                                    <input type="radio" name="status" wire:model="status" checked="checked" value="1">
                                                    <span></span>Active</label>
                                                <label class="radio radio-solid">
                                                    <input type="radio" name="status" wire:model="status" value="0">
                                                    <span></span>In Active</label>
                                            </div>
                                            <span class="form-text text-muted">Please select Active & In Active</span>
                                        </div>

                                        <label class="col-lg-2 col-form-label text-lg-right">Featured: <span class="text-danger">*</span></label>
                                        <div class="col-lg-3">
                                            <div class="radio-inline">
                                                <label class="radio radio-solid">
                                                    <input type="radio" name="priority" wire:model="priority" checked="checked" value="1">
                                                    <span></span>Yes</label>
                                                <label class="radio radio-solid">
                                                    <input type="radio" name="priority" wire:model="priority" value="0">
                                                    <span></span>No</label>
                                            </div>
                                            <span class="form-text text-muted">Please select Featured</span>
                                        </div>
                                    </div>
                                   
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label text-lg-right">Images <span class="text-danger">*</span></label>
                                        <div class="custom-file col-lg-8">
                                            <input required type="file" accept=".png, .jpg, .jpeg"  wire:model="images" name="images" multiple class="custom-file-input  @error('images') is-invalid @enderror" id="customFile">
                                            <div wire:loading wire:target="images" class="text-sm text-gray-500 italic">Uploading...</div>

                                            @error('images')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                            @enderror
                                            <label class="custom-file-label" for="customFile">Choose file</label>
                                        </div>

                                    </div>

                                    @if ($images)
                                    <div class="form-group row">
                                        Photo Preview:
                                        @foreach($images as $image)
                                        <div class="col-lg-2">
                                            <img src="{{ $image->temporaryUrl() }}">
                                        </div>
                                        @endforeach
                                    </div>
                                    @endif

                                    <div class="card-footer">
                                        <div class="row">
                                            <div class="col-lg-2"></div>
                                            <div class="col-lg-10">
                                                <button class="btn btn-primary btn-flat" type="submit" id="submit" wire:target="submitForm"><i class="fa fa-save"></i>Next</button>
                                                {{-- <button  type="button" id="submit" class="btn btn-primary btn-flat "><i class="fa fa-save"></i> Save</button> --}}
                                                <a href="/homeberry-portal/properties" class="btn btn-default btn-flat pull-left"><i class="fa fa-close"></i> Back</a>
                                            </div>
                                        </div>
                                    </div>
                            </form>
                            <!-- end: Example Code-->
                        </div>
                        <!--end::Form-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@section('scripts')

<script>
    window.addEventListener('alert', event => {
        Swal.fire({
            icon: event.detail.type,
            title: event.detail.title,
            text: event.detail.message,
            type: event.detail.type,
            //showCancelButton: true,
            confirmButtonColor: "#F64E60",
            confirmButtonText: "Go To Add Location",
            allowOutsideClick: false,
        }).then((result) => {
            if (result.isConfirmed) {
                window.location.href = "/homeberry-portal/add-loaction-property";
            }
        });
    })
</script>

@stop

@push('scripts')
<script src="{{asset('admin/assets/js/pages/crud/forms/widgets/bootstrap-datepicker.js')}}"></script>

<script src="{{asset('admin/assets/js/pages/crud/forms/widgets/bootstrap-select.js')}}"></script>


@endpush

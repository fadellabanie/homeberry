
<div>

<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Dashboard-->
           
            <!--begin::Row-->
            <div class="row">
             
                <div class="col-xl-3">
                    <!--begin::Stats Widget 26-->
                    <div class="card card-custom bg-light-danger card-stretch gutter-b">
                        <!--begin::ody-->
                        <div class="card-body">
                            <span class="svg-icon svg-icon-2x svg-icon-danger">
                                <!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Group.svg-->
                                <a href="{{route('users')}}"> <img src="{{asset('admin/assets/users.png')}}"/></a>

                                <!--end::Svg Icon-->
                            </span>
                        <span class="card-title font-weight-bolder text-dark-75 font-size-h2 mb-0 mt-6 d-block">{{$countOfUserActive}}</span>
                            <span class="font-weight-bold text-muted font-size-sm">Active User</span>
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Stats Widget 26-->
                </div>
                <div class="col-xl-3">
                    <!--begin::Stats Widget 27-->
                    <div class="card card-custom bg-light-info card-stretch gutter-b">
                        <!--begin::Body-->
                        <div class="card-body">
                            <span class="svg-icon svg-icon-2x svg-icon-info">
                                <!--begin::Svg Icon | path:assets/media/svg/icons/Media/Equalizer.svg-->
                            <a href="{{route('users')}}"><img src="{{asset('admin/assets/user-d.png')}}"/></a>

                                <!--end::Svg Icon-->
                            </span>
                            <span class="card-title font-weight-bolder text-dark-75 font-size-h2 mb-0 mt-6 d-block">{{$countOfNotUserActive}}</span>
                            <span class="font-weight-bold text-muted font-size-sm">Not Active User</span>
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Stats Widget 27-->
                </div>
                <div class="col-xl-3">
                    <!--begin::Stats Widget 25-->
                    <div class="card card-custom bg-light-success card-stretch gutter-b">
                        <!--begin::Body-->
                        <div class="card-body">
                            <span class="svg-icon svg-icon-2x svg-icon-success">
                                <a href="{{route('projects')}}"> <img src="{{asset('admin/assets/city.png')}}"/></a>
                                <!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Mail-opened.svg-->
                                {{-- <span class="svg-icon svg-icon-primary svg-icon-2x"><!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo5\dist/../src/media/svg/icons\Home\Building.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect x="0" y="0" width="24" height="24"/>
                                        <path d="M13.5,21 L13.5,18 C13.5,17.4477153 13.0522847,17 12.5,17 L11.5,17 C10.9477153,17 10.5,17.4477153 10.5,18 L10.5,21 L5,21 L5,4 C5,2.8954305 5.8954305,2 7,2 L17,2 C18.1045695,2 19,2.8954305 19,4 L19,21 L13.5,21 Z M9,4 C8.44771525,4 8,4.44771525 8,5 L8,6 C8,6.55228475 8.44771525,7 9,7 L10,7 C10.5522847,7 11,6.55228475 11,6 L11,5 C11,4.44771525 10.5522847,4 10,4 L9,4 Z M14,4 C13.4477153,4 13,4.44771525 13,5 L13,6 C13,6.55228475 13.4477153,7 14,7 L15,7 C15.5522847,7 16,6.55228475 16,6 L16,5 C16,4.44771525 15.5522847,4 15,4 L14,4 Z M9,8 C8.44771525,8 8,8.44771525 8,9 L8,10 C8,10.5522847 8.44771525,11 9,11 L10,11 C10.5522847,11 11,10.5522847 11,10 L11,9 C11,8.44771525 10.5522847,8 10,8 L9,8 Z M9,12 C8.44771525,12 8,12.4477153 8,13 L8,14 C8,14.5522847 8.44771525,15 9,15 L10,15 C10.5522847,15 11,14.5522847 11,14 L11,13 C11,12.4477153 10.5522847,12 10,12 L9,12 Z M14,12 C13.4477153,12 13,12.4477153 13,13 L13,14 C13,14.5522847 13.4477153,15 14,15 L15,15 C15.5522847,15 16,14.5522847 16,14 L16,13 C16,12.4477153 15.5522847,12 15,12 L14,12 Z" fill="#000000"/>
                                        <rect fill="#FFFFFF" x="13" y="8" width="3" height="3" rx="1"/>
                                        <path d="M4,21 L20,21 C20.5522847,21 21,21.4477153 21,22 L21,22.4 C21,22.7313708 20.7313708,23 20.4,23 L3.6,23 C3.26862915,23 3,22.7313708 3,22.4 L3,22 C3,21.4477153 3.44771525,21 4,21 Z" fill="#000000" opacity="0.3"/>
                                    </g>
                                </svg><!--end::Svg Icon--></span> --}}
                                
                                <!--end::Svg Icon-->
                            </span>
                            <span class="card-title font-weight-bolder text-dark-75 font-size-h2 mb-0 mt-6 d-block">{{$totalProjects}}</span>
                            <span class="font-weight-bold text-muted font-size-sm">Total Projects</span>
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Stats Widget 25-->
                </div>
                <div class="col-xl-3">
                    <!--begin::Stats Widget 28-->
                    <div class="card card-custom bg-light-warning card-stretch gutter-b">
                        <!--begin::Body-->
                        <div class="card-body">
                            <a href="{{route('properties')}}"> <img src="{{asset('admin/assets/home.png')}}"/></a>
                                <!--end::Svg Icon-->
                            </span>
                        <span class="card-title font-weight-bolder text-dark-75 font-size-h2 mb-0 mt-6 d-block">{{$totalProperties}}</span>
                            <span class="font-weight-bold text-muted font-size-sm">Total Properties</span>
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Stat: Widget 28-->
                </div>
            </div>
             <!--end::Row-->
             <div class="container py-8">
                <div class="row">
                    <div class="col-lg-4">
                        <!--begin::Callout-->
                        <div class="card card-custom wave wave-animate-slow wave-primary mb-8 mb-lg-0">
                            <div class="card-body">
                                <div class="d-flex align-items-center p-5">
                                    <!--begin::Icon-->
                                    <div class="mr-6">
                                        <span class="svg-icon svg-icon-primary svg-icon-4x">
                                            <!--begin::Svg Icon | path:assets/media/svg/icons/Home/Mirror.svg-->
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <path d="M10.9630156,7.5 L11.0475062,7.5 C11.3043819,7.5 11.5194647,7.69464724 11.5450248,7.95024814 L12,12.5 L15.2480695,14.3560397 C15.403857,14.4450611 15.5,14.6107328 15.5,14.7901613 L15.5,15 C15.5,15.2109164 15.3290185,15.3818979 15.1181021,15.3818979 C15.0841582,15.3818979 15.0503659,15.3773725 15.0176181,15.3684413 L10.3986612,14.1087258 C10.1672824,14.0456225 10.0132986,13.8271186 10.0316926,13.5879956 L10.4644883,7.96165175 C10.4845267,7.70115317 10.7017474,7.5 10.9630156,7.5 Z" fill="#000000"></path>
                                                    <path d="M7.38979581,2.8349582 C8.65216735,2.29743306 10.0413491,2 11.5,2 C17.2989899,2 22,6.70101013 22,12.5 C22,18.2989899 17.2989899,23 11.5,23 C5.70101013,23 1,18.2989899 1,12.5 C1,11.5151324 1.13559454,10.5619345 1.38913364,9.65805651 L3.31481075,10.1982117 C3.10672013,10.940064 3,11.7119264 3,12.5 C3,17.1944204 6.80557963,21 11.5,21 C16.1944204,21 20,17.1944204 20,12.5 C20,7.80557963 16.1944204,4 11.5,4 C10.54876,4 9.62236069,4.15592757 8.74872191,4.45446326 L9.93948308,5.87355717 C10.0088058,5.95617272 10.0495583,6.05898805 10.05566,6.16666224 C10.0712834,6.4423623 9.86044965,6.67852665 9.5847496,6.69415008 L4.71777931,6.96995273 C4.66931162,6.97269931 4.62070229,6.96837279 4.57348157,6.95710938 C4.30487471,6.89303938 4.13906482,6.62335149 4.20313482,6.35474463 L5.33163823,1.62361064 C5.35654118,1.51920756 5.41437908,1.4255891 5.49660017,1.35659741 C5.7081375,1.17909652 6.0235153,1.2066885 6.2010162,1.41822583 L7.38979581,2.8349582 Z" fill="#000000" opacity="0.3"></path>
                                                </g>
                                            </svg>
                                            <!--end::Svg Icon-->
                                        </span>
                                    </div>
                                    <!--end::Icon-->
                                    <!--begin::Content-->
                                    <div class="d-flex flex-column">
                                        <a href="{{route('schedules')}}" class="text-dark text-hover-primary font-weight-bold font-size-h4 mb-3">Schedules</a>
                                    <div class="text-dark-75">
                                        <span class="card-title font-weight-bolder text-dark-75 font-size-h2 mb-0 mt-6 d-block"> {{$schedules}}</span>
                                    </div>
                                    </div>
                                    <!--end::Content-->
                                </div>
                            </div>
                        </div>
                        <!--end::Callout-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Callout-->
                        <div class="card card-custom wave wave-animate-slow wave-danger mb-8 mb-lg-0">
                            <div class="card-body">
                                <div class="d-flex align-items-center p-5">
                                    <!--begin::Icon-->
                                    <div class="mr-6">
                                        <span class="svg-icon svg-icon-danger svg-icon-4x">
                                            <!--begin::Svg Icon | path:assets/media/svg/icons/General/Thunder-move.svg-->
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <path d="M8,4 L16,4 C17.1045695,4 18,4.8954305 18,6 L18,17.726765 C18,18.2790497 17.5522847,18.726765 17,18.726765 C16.7498083,18.726765 16.5087052,18.6329798 16.3242754,18.4639191 L12.6757246,15.1194142 C12.2934034,14.7689531 11.7065966,14.7689531 11.3242754,15.1194142 L7.67572463,18.4639191 C7.26860564,18.8371115 6.63603827,18.8096086 6.26284586,18.4024896 C6.09378519,18.2180598 6,17.9769566 6,17.726765 L6,6 C6,4.8954305 6.8954305,4 8,4 Z" fill="#000000"></path>
                                                </g>
                                            </svg>
                                            <!--end::Svg Icon-->
                                        </span>
                                    </div>
                                    <!--end::Icon-->
                                    <!--begin::Content-->
                                    <div class="d-flex flex-column">
                                    <a href="{{route('booking')}}" class="text-dark text-hover-primary font-weight-bold font-size-h4 mb-3">Bookings</a>
                                        <div class="text-dark-75">
                                            <div class="text-dark-75">
                                                <span class="card-title font-weight-bolder text-dark-75 font-size-h2 mb-0 mt-6 d-block"> {{$bookings}}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end::Content-->
                                </div>
                            </div>
                        </div>
                        <!--end::Callout-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Callout-->
                        <div class="card card-custom wave wave-animate-slow wave-success mb-8 mb-lg-0">
                            <div class="card-body">
                                <div class="d-flex align-items-center p-5">
                                    <!--begin::Icon-->
                                    <div class="mr-6">
                                        <span class="svg-icon svg-icon-success svg-icon-4x">
                                            <!--begin::Svg Icon | path:assets/media/svg/icons/Design/Sketch.svg-->
                                            <svg xmlns="http:/www.w3.org/2000/svg" xmlns:xlink="http:/www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <path d="M5,7 L19,7 C20.1045695,7 21,7.8954305 21,9 L21,17 C21,18.1045695 20.1045695,19 19,19 L5,19 C3.8954305,19 3,18.1045695 3,17 L3,9 C3,7.8954305 3.8954305,7 5,7 Z M12,17 C14.209139,17 16,15.209139 16,13 C16,10.790861 14.209139,9 12,9 C9.790861,9 8,10.790861 8,13 C8,15.209139 9.790861,17 12,17 Z" fill="#000000"></path>
                                                    <rect fill="#000000" opacity="0.3" x="9" y="4" width="6" height="2" rx="1"></rect>
                                                    <circle fill="#000000" opacity="0.3" cx="12" cy="13" r="2"></circle>
                                                </g>
                                            </svg>
                                            <!--end::Svg Icon-->
                                        </span>
                                    </div>
                                    <!--end::Icon-->
                                    <!--begin::Content-->
                                    <div class="d-flex flex-column">
                                        <a href="{{route('photo_sessions')}}" class="text-dark text-hover-primary font-weight-bold font-size-h4 mb-3">Photo Sessions</a>
                                        <div class="text-dark-75"> <div class="text-dark-75">
                                            <span class="card-title font-weight-bolder text-dark-75 font-size-h2 mb-0 mt-6 d-block"> {{$photoSessions}}</span>
                                        </div></div>
                                    </div>
                                    <!--end::Content-->
                                </div>
                            </div>
                        </div>
                        <!--end::Callout-->
                    </div>
                </div>
            </div>
            <!--begin::Row-->
            <div class="row">
                <div class="col-xl-8">
                    <!--begin::Advance Table Widget 1-->
                    <div class="card card-custom card-stretch gutter-b card-shadowless bg-light">
                        <!--begin::Header-->
                        <div class="card-header border-0 py-5">
                            <h3 class="card-title align-items-start flex-column">
                                <span class="card-label font-weight-bolder text-dark">Last Users Register</span>
                                <span class="text-muted mt-3 font-weight-bold font-size-sm"></span>
                            </h3>
                        </div>
                        <!--end::Header-->
                        <!--begin::Body-->
                        <div class="card-body py-0">
                            <!--begin::Table-->
                            <div class="table-responsive">
                                <table class="table table-head-custom table-vertical-center" id="kt_advance_table_widget_1">
                                    <thead>
                                        <tr class="text-left">
                                            <th class="pr-0" style="width: 50px">Users</th>
                                            <th style="min-width: 200px"></th>
                                            <th style="min-width: 150px">Email</th>
                                            <th style="min-width: 150px">Phone</th>
                                            <th style="min-width: 150px">Device</th>
                                            <th style="min-width: 150px">Since</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($lastUsersRegister as $key => $user)
                                        <tr>
                                            <td class="pr-0">
                                                <div class="symbol symbol-50 symbol-light mt-1">
                                                    <span class="symbol-label">
                                                        
                                                        <img src="{{asset('admin/assets/media/svg/avatars/00'.$key.'-boy.svg')}}" class="h-75 align-self-end" alt="" />
                                                    </span>
                                                </div>
                                            </td>
                                            <td class="pl-0">
                                            <span class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">{{$user->name ?? "No Name"}}</span>
                                            <span class="text-muted font-weight-bold text-muted d-block">{{$user->created_at}}</span>
                                            </td>
                                            <td>
                                                <span class="text-dark-75 font-weight-bolder d-block font-size-lg">{{$user->email ?? "No Email"}}</span>
                                            </td>
                                            <td>
                                                <span class="text-dark-75 font-weight-bolder d-block font-size-lg">{{$user->mobile}}</span>
                                                <span class="text-muted font-weight-bold">{{$user->country_code}}</span>
                                            </td> 
                                             <td>
                                                <span class="text-dark-75 font-weight-bolder d-block font-size-lg">
                                                    {{$user->userToken != null ? $user->userToken->device_type: "No Device"}}
                                                </span>
                                             
                                            </td>
                                            <td>
                                                <span class="text-dark-75 font-weight-bolder d-block font-size-lg">
                                                    {{$user->created_at ?? ""}}
                                                </span>
                                             
                                            </td>
                                          
                                        </tr>
                                   
                                        @endforeach
                                       
                                    </tbody>
                                </table>
                            </div>
                            <!--end::Table-->
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Advance Table Widget 1-->
                </div>
                <div class="col-xl-4">
                    <!--begin::List Widget 7-->
                    <div class="card card-custom card-stretch gutter-b card-shadowless bg-light-danger">
                        <!--begin::Header-->
                        <div class="card-header border-0">
                            <h3 class="card-title font-weight-bolder text-dark">Last Properties Created</h3>
                        </div>
                        <!--end::Header-->
                        <!--begin::Body-->
                        <div class="card-body pt-0">
                            <!--begin::Item-->
                            @foreach ($lastProperties as $property)
                            <div class="d-flex align-items-center flex-wrap mb-10">
                            
                                <!--begin::Text-->
                                <div class="d-flex flex-column flex-grow-1 mr-2">
                                <a href="/homeberry-portal/show-property/{{$property->id}}" class="font-weight-bold text-dark-75 text-hover-primary font-size-lg mb-1">{{$property->name_en}}</a>
                                    <span class="text-muted font-weight-bold">{{$property->activityType->name_en}}, {{$property->city->name_en}}</span>
                                    <span class="text-muted font-weight-bold"> {{$property->created_at}}</span>
                                </div>
                                <!--end::Text-->
                                <span class="label label-xl label-light label-inline my-lg-0 my-2 text-dark-50 font-weight-bolder">LE {{$property->price}}</span>
                            </div>
                            @endforeach
                           
                            <!--end::Item-->
                          
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::List Widget 7-->
                </div>
            </div>
            <!--end::Row-->
            <!--end::Dashboard-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
<!--end::Content-->

</div>


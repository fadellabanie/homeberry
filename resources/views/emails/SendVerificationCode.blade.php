@component('mail::message')
# Verification Code Mail

Your Verification Code is {{$data}} ,
Please return to our app and Enter the verification code to process your request

Thanks,<br>
{{ config('app.name') }}
@endcomponent

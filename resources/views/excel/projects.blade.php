<table>
    <thead>
        <tr>
            <th>#</th>
            <th>devolper </th>
            <th>city </th>
            <th>activity_type</th>
            <th>name_ar</th>
            <th>name_en</th>
            <th>description_ar</th>
            <th>description_en</th>
            <th>finished</th>
            <th>delivery</th>
            <th>price</th>
            <th>link</th>
            <th>views</th>
            <th>lat</th>
            <th>lng</th>
            <th>priority</th>
            <th>status</th>
            <th>created_at</th>
            <th>updated_at</th>
        </tr>
    </thead>
    <tbody>
        @foreach($projects as $item)
        <tr>
            <td>{{ $item->id }}</td>
            <td>{{ $item->devolper->name }}</td>
            <td>{{ $item->city->name_en }}</td>
            <td>{{ $item->activityType->name_en }}</td>
            <td>{{ $item->name_ar }}</td>
            <td>{{ $item->name_en }}</td>
            <td>{{ $item->description_ar }}</td>
            <td>{{ $item->description_en }}</td>
            <td>{{ $item->finished }}</td>
            <td>{{ $item->delivery }}</td>
            <td>{{ $item->price }}</td>
            <td>{{ $item->link }}</td>
            <td>{{ $item->views }}</td>
            <td>{{ $item->lat }}</td>
            <td>{{ $item->lng }}</td>
            <td>{{ $item->priority }}</td>
            <td>{{ $item->status }}</td>
            <td>{{ $item->created_at }}</td>
            <td>{{ $item->updated_at }}</td>
        </tr>
        @endforeach
    </tbody>
</table>

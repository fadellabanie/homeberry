<table>
    <thead>
        <tr>
            <th>#</th>
            <th>city</th>
            <th>payment</th>
            <th>activity_type</th>
            <th>name_ar</th>
            <th>name_en</th>
            <th>description_ar</th>
            <th>description_en</th>
            <th>price</th>

            <th>availability</th>
            <th>guest</th>
            <th>room </th>
            <th>bathroom </th>
            <th>area </th>
            <th>park </th>
            <th>rental </th>
            <th>furnished </th>
            <th>type </th>
            <th>lng </th>
            <th>lat </th>

            <th>priority</th>
            <th>status</th>
            <th>views</th>
            <th>created_at</th>
            <th>updated_at</th>
        </tr>
    </thead>
    <tbody>
        @foreach($properties as $item)
        <tr>
            <td>{{ $item->id }}</td>
            <td>{{ $item->city->name_en }}</td>
            <td>{{ $item->paymentMathod->name_en }}</td>
            <td>{{ $item->activityType->name_en }}</td>
            <td>{{ $item->name_ar }}</td>
            <td>{{ $item->name_en }}</td>
            <td>{{ $item->description_ar }}</td>
            <td>{{ $item->description_en }}</td>
            <td>{{ $item->price }}</td>

            <td>{{ $item->propertyAttribute->availability }}</td>
            <td>{{ $item->propertyAttribute->guest }}</td>
            <td>{{ $item->propertyAttribute->room }}</td>
            <td>{{ $item->propertyAttribute->bathroom }}</td>
            <td>{{ $item->propertyAttribute->area }}</td>
            <td>{{ $item->propertyAttribute->park }}</td>
            <td>{{ $item->propertyAttribute->rental }}</td>
            <td>{{ $item->propertyAttribute->furnished }}</td>
            <td>{{ $item->propertyAttribute->type }}</td>
            <td>{{ $item->propertyAttribute->lng }}</td>
            <td>{{ $item->propertyAttribute->lat }}</td>

            <td>{{ $item->priority }}</td>
            <td>{{ $item->status }}</td>
            <td>{{ $item->views }}</td>
            <td>{{ $item->created_at }}</td>
            <td>{{ $item->updated_at }}</td>
        </tr>
        @endforeach
    </tbody>
</table>

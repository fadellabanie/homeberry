<table>
    <thead>
        <tr>
            <th>#</th>
            <th>name</th>
            <th>email</th>
            <th>email_verified_at</th>
            <th>country_code</th>
            <th>mobile</th>
            <th>type</th>
            <th>created_at</th>
            <th>updated_at</th>
        </tr>
    </thead>
    <tbody>
        @foreach($users as $item)
        <tr>
            <td>{{ $item->id }}</td>
            <td>{{ $item->name }}</td>
            <td>{{ $item->email }}</td>
            <td>{{ $item->email_verified_at }}</td>
            <td>{{ $item->country_code }}</td>
            <td>{{ $item->mobile }}</td>
            <td>{{ $item->type }}</td>

            <td>{{ $item->created_at }}</td>
            <td>{{ $item->updated_at }}</td>
        </tr>
        @endforeach
    </tbody>
</table>

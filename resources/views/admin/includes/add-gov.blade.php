<!-- Add -->
<div class="modal fade" id="addnew">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><b>Add New Record</b></h4>
            </div>
            <div class="modal-body">
                <!--begin::Form-->
                <form class="form-horizontal" method="POST" action="{{ route('governorates.store') }}">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label>governorate Ar Name
                                <span class="text-danger">*</span></label>
                            <input type="text" name="name_ar" class="form-control" placeholder="Enter governorate Ar Name" required />
                        </div>
                        <div class="form-group">
                            <label>governorate En Name
                                <span class="text-danger">*</span></label>
                            <input type="text" name="name_en" class="form-control" placeholder="Enter governorate En Name" required />
                        </div>
                        <div class="form-group">
                            <label class="col-3 col-form-label">Status</label>
                            <div class="col-3">
                                <span class="switch switch-outline switch-icon switch-success">
                                    <label>
                                        <input type="checkbox" checked="checked" name="status" value="1">
                                        <span></span>
                                    </label>
                                </span>
                            </div>
                      
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary btn-flat"><i class="fa fa-save"></i> Save</button>
                        <button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
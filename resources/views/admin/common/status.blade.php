	@isset($status)

		@if($status == 1)
		<span class="label label-lg font-weight-bold label-light-info label-inline">{{__("Active")}}</span>
	
		@else
		<span class="label label-lg font-weight-bold label-light-danger label-inline">{{__("in Active")}}</span>
		
		@endif

	@endisset

	@isset($priority)

		@if($priority == 'on')
			<span class="kt-badge  kt-badge--success kt-badge--inline kt-badge--pill">{{__("Paid")}}</span>
		@else
			<span class="kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill">{{__("UnPaid")}}</span>
		@endif

	@endisset

	@isset($sort)

	@switch($sort)
		@case(1)
			{{$sort}}
			@break
		@case(2)
		{{$sort}}
			
			@break
		@case(3)
		{{$sort}}
		
			@break

		@case(4)
		{{$sort}}
		
			@break
		@case(5)
		{{$sort}}
		
			@break
		@case(6)
		{{$sort}}
		
			@break
		@case(7)
		{{$sort}}
		
			@break
		@default
		{{$sort}}
			
	@endswitch

@endisset
<div id="map"></div>

@section('scripts')
<script>
    // Initialize and add the map
    function initMap() {
      // The location of Uluru
      var uluru = {lat: -25.344, lng: 131.036};
      // The map, centered at Uluru
      var map = new google.maps.Map(
          document.getElementById('map'), {zoom: 4, center: uluru});
      // The marker, positioned at Uluru
      var marker = new google.maps.Marker({position: uluru, map: map});
    }
        </script>
        <script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD7P2KwVoARB7nvRMHQX8oPgUg0yA2is7Q&callback=initMap">
    </script>
@endsection

@if(Session::has('flash_message'))
	<div class="alert alert-{{ Session::get('flash_message_level') }} text-left">
		{{ Session::get('flash_message') }}

		@if(Session::get('flash_message_dismissible'))
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		@endif
	</div>
@endif

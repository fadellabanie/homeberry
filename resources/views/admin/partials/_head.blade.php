<head>
<meta charset="utf-8" />
<title>Home Berry</title>
<meta name="description" content="Updates and statistics" />
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
<link rel="canonical" href="https://keenthemes.com/metronic" />
<!--begin::Fonts-->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
<!--end::Fonts-->
<!--begin::Page Vendors Styles(used by this page)-->
<link href="{{asset('admin/assets/plugins/custom/fullcalendar/fullcalendar.bundle.css')}}" data-turbolinks-track="reload"  rel="stylesheet" type="text/css" />
<!--end::Page Vendors Styles-->
<!--begin::Global Theme Styles(used by all pages)-->
<link href="{{asset('admin/assets/plugins/global/plugins.bundle.css')}}" data-turbolinks-track="reload"  rel="stylesheet" type="text/css" />
<link href="{{asset('admin/assets/plugins/custom/prismjs/prismjs.bundle.css')}}"data-turbolinks-track="reload"  rel="stylesheet" type="text/css" />
<link href="{{asset('admin/assets/css/style.bundle.css')}}" data-turbolinks-track="reload" rel="stylesheet" type="text/css" />
<!--end::Global Theme Styles-->
<!--begin::Layout Themes(used by all pages)-->
{{-- <link href="{{asset('admin/assets/plugins/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" /> --}}
<script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

<meta name="csrf-token" content="{{ csrf_token() }}" />

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js" data-turbolinks-track="reload" ></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

<!--end::Layout Themes-->
<link rel="shortcut icon" href="{{asset('admin/assets/logo.ico')}}" />

<script defer src="{{ mix('js/app.js') }}"></script>
<link href="{{ asset('css/main.css')}}" rel="stylesheet" type="text/css" />

@yield('head')

</head>
@livewireScripts
<script src="https://cdn.jsdelivr.net/gh/livewire/turbolinks@v0.1.x/dist/livewire-turbolinks.js" data-turbolinks-eval="false"></script>

<!-- Styles -->
@livewireStyles


<style>
    #GoogleMapModalCenter .modal-content{
        background: transparent;
    }
    #GoogleMapModalCenter .modal-dialog-centered{
        width: 100%;
        max-width: initial;
    
    }
</style>
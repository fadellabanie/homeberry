@extends('admin.layouts.app')
@section('head')
<script async defer
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDhz6eMEig_Qf7MQ9A8T_tkjDSPVASBCyk&v=3&callback=initialize">
 </script>
 <script>  
    function initialize() {
     var myLatLng = new google.maps.LatLng(30.023994517161533, 31.21414849267577);
    var mapOptions = {
        zoom: 12,
        center: myLatLng,
        mapTypeId: google.maps.MapTypeId.READMAP
    };
    
    var map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
    map.addListener("click", function (e) {
        placeMarker(e.latLng);
        var lat = e.latLng.lat().toString();
        var lng =  e.latLng.lng().toString();
        
       document.getElementById('lat').value = lat;
       document.getElementById('lng').value = lng;
     
    });
    
    var marker='';
    
    function placeMarker(location) {
        if (marker == ''){
            marker = new google.maps.Marker({
                position: location,
                map: map, 
                animation: google.maps.Animation.DROP,
                title: 'Your location'
            });
        }
        else{
    
            marker.setPosition(location);
        }
        map.setCenter(location);
    }
    }
 </script>
@endsection
@section('content')
<div>
<div class="d-flex flex-column-fluid">
<!--begin::Container-->
<div class="container">
<div class="subheader py-3 py-lg-8 subheader-transparent" id="kt_subheader">
   <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
      <!--begin::Info-->
      <div class="d-flex align-items-center mr-1">
         <!--begin::Page Heading-->
         <div class="d-flex align-items-baseline flex-wrap mr-5">
            <!--begin::Page Title-->
            <h2 class="d-flex align-items-center text-dark font-weight-bold my-1 mr-3">projects</h2>
            <!--end::Page Title-->
            <!--begin::Breadcrumb-->
            <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold my-2 p-0">
               <li class="breadcrumb-item">
                  <a href="/homeberry-portal/projects" class="text-muted">projects List</a>
               </li>
               <li class="breadcrumb-item">
                  <a href="" class="text-muted">Add Location project</a>
               </li>
            </ul>
            <!--end::Breadcrumb-->
         </div>
         <!--end::Page Heading-->
      </div>
   </div>
</div>
<!--begin::Notice-->
<div class="alert alert-custom alert-white alert-shadow gutter-b" role="alert">
   <div class="alert-icon">
      <span class="svg-icon svg-icon-primary svg-icon-xl">
         <!--begin::Svg Icon | path:assets/media/svg/icons/Tools/Compass.svg-->
         <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
               <rect x="0" y="0" width="24" height="24"></rect>
               <path d="M7.07744993,12.3040451 C7.72444571,13.0716094 8.54044565,13.6920474 9.46808594,14.1079953 L5,23 L4.5,18 L7.07744993,12.3040451 Z M14.5865511,14.2597864 C15.5319561,13.9019016 16.375416,13.3366121 17.0614026,12.6194459 L19.5,18 L19,23 L14.5865511,14.2597864 Z M12,3.55271368e-14 C12.8284271,3.53749572e-14 13.5,0.671572875 13.5,1.5 L13.5,4 L10.5,4 L10.5,1.5 C10.5,0.671572875 11.1715729,3.56793164e-14 12,3.55271368e-14 Z" fill="#000000" opacity="0.3"></path>
               <path d="M12,10 C13.1045695,10 14,9.1045695 14,8 C14,6.8954305 13.1045695,6 12,6 C10.8954305,6 10,6.8954305 10,8 C10,9.1045695 10.8954305,10 12,10 Z M12,13 C9.23857625,13 7,10.7614237 7,8 C7,5.23857625 9.23857625,3 12,3 C14.7614237,3 17,5.23857625 17,8 C17,10.7614237 14.7614237,13 12,13 Z" fill="#000000" fill-rule="nonzero"></path>
            </g>
         </svg>
         <!--end::Svg Icon-->
      </span>
   </div>
   <div class="alert-text">
      <b class="mr-2">Projects</b>Add Locaction Project.
   </div>
</div>
<div class="container">
    <div class="row">
    <div class="col-lg-12">
       
      
        <div class="card card-custom gutter-b example example-compact">
            <!-- <form wire:submit.prevent="submitForm" class="grid grid-cols-1 row-gap-6"> -->
            <form method="POST" action="{{route('add-loaction-project')}}" class="grid grid-cols-1 row-gap-6">
                @csrf
               

                <div class="card-body">
                      <!--begin::Card-->
                      <div class="form-group">
                        <div class="alert alert-custom alert-default" role="alert">
                            <div class="alert-icon">
                                <span class="svg-icon svg-icon-primary svg-icon-xl">
                                    <!--begin::Svg Icon | path:assets/media/svg/icons/Tools/Compass.svg-->
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <rect x="0" y="0" width="24" height="24"></rect>
                                            <path d="M7.07744993,12.3040451 C7.72444571,13.0716094 8.54044565,13.6920474 9.46808594,14.1079953 L5,23 L4.5,18 L7.07744993,12.3040451 Z M14.5865511,14.2597864 C15.5319561,13.9019016 16.375416,13.3366121 17.0614026,12.6194459 L19.5,18 L19,23 L14.5865511,14.2597864 Z M12,3.55271368e-14 C12.8284271,3.53749572e-14 13.5,0.671572875 13.5,1.5 L13.5,4 L10.5,4 L10.5,1.5 C10.5,0.671572875 11.1715729,3.56793164e-14 12,3.55271368e-14 Z" fill="#000000" opacity="0.3"></path>
                                            <path d="M12,10 C13.1045695,10 14,9.1045695 14,8 C14,6.8954305 13.1045695,6 12,6 C10.8954305,6 10,6.8954305 10,8 C10,9.1045695 10.8954305,10 12,10 Z M12,13 C9.23857625,13 7,10.7614237 7,8 C7,5.23857625 9.23857625,3 12,3 C14.7614237,3 17,5.23857625 17,8 C17,10.7614237 14.7614237,13 12,13 Z" fill="#000000" fill-rule="nonzero"></path>
                                        </g>
                                    </svg>
                                    <!--end::Svg Icon-->
                                </span>
                            </div>
                            <div class="alert-text">Refresh Page If Map Does Not Display Use : Ctrl + F5
                               
                           
                        </div>
                    </div>
                <label for="example-number-input" class="col-2 col-form-label text-lg-right">Address</label>
                
                <div class="form-group row">
                    
                    <label class="col-lg-2 col-form-label text-lg-right"> Map:</label>
                    <div class="col-lg-3">
                        <div class = "user-location">
                            <div class = "location-types">
                                <label class="filter-list__item ">
                                <span class="filter-list__input input-radio">
                                </span>
                                </span>
                                <span class="filter-list__title">
                                <a href="javascript();" data-toggle="modal" data-target="#GoogleMapModalCenter"> Put Pin location via Google Map</a>
                                </span>
                                </label>
                            </div>
                        </div></div>
                    <label class="col-lg-2 col-form-label text-lg-right">Projects:</label>
                    <div class="col-lg-3">
                        <select class="form-control" required  id="exampleSelect2" data-size="5" required data-live-search="true" name="id">
                            <option selected>Select Item</option>
                            @foreach ($projects as $item)
                            <option value="{{$item->id}}">{{$item->name_en}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-2 col-form-label text-lg-right"> Latitude:</label>
                    <div class="col-lg-3">
                        <input type="text" required  name="lat" id = "lat" class="form-control @error('lat') is-invalid @enderror" placeholder="lat">
                        <span class="form-text text-muted">Latitude</span>
                    </div>
                    <label class="col-lg-2 col-form-label text-lg-right">Longitude:</label>
                    <div class="col-lg-3">
                        <input type="text" required name="lng" id = "lng" class="form-control @error('lng') is-invalid @enderror" placeholder="lng">
                        <span class="form-text text-muted">Longitude</span>
                    </div>
                </div>
                <!-- Modal -->
                <!-- Modal -->
                <div class="modal fade" id="GoogleMapModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Select your location!</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div id="map_canvas" style="height : 500px; margin:auto; width:97%;" class = "col-md-12"></div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
                </div>
                   <div class="card-footer">
                                    <div class="row">
                                        <div class="col-lg-2"></div>
                                        <div class="col-lg-10">
                                            <button class="btn btn-primary btn-flat" type="submit"><i class="fa fa-save"></i>Submit</button>
                                            {{-- <button  type="button" id="submit" class="btn btn-primary btn-flat "><i class="fa fa-save"></i> Save</button> --}}
                                        </div>
                                    </div>
                                </div>
            </form>
            </div>
        </div>
    </div>
    </div>
    </div>
    </div>
    </div>
</div>
@stop
@section('scripts')

@endsection
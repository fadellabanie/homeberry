@if ($paginator->hasPages())
    <div class="flex justify-between flex-1 sm:hidden">
        @if ($paginator->onFirstPage())
        <span class="btn btn-icon btn-sm btn-light-primary mr-2 my-1">
            {!! __('pagination.previous') !!}
        </span>
        @else
        <button wire:click="previousPage" class="btn btn-icon btn-sm btn-light-primary mr-2 my-1" style="width: 60px; height: 60px;">
        
       
            {!! __('pagination.previous') !!}
        </button>
        @endif

        @if ($paginator->hasMorePages())
        <button wire:click="nextPage" class="btn btn-icon btn-sm btn-light-primary mr-2 my-1" style="width: 60px; height: 60px;">
            {!! __('pagination.next') !!}
        </button>
        @else
        <span class="btn btn-icon btn-sm btn-light-primary mr-2 my-1">
            {!! __('pagination.next') !!}
        </span>
        @endif
    </div>

    <div class="hidden sm:flex-1 sm:flex sm:items-center sm:justify-between">
        <div style="margin-right: 15px;">
        
    
            <p class="text-sm text-gray-700 leading-5">
                {!! __('Showing') !!}
                <span class="font-medium">{{ $paginator->firstItem() }}</span>
                {!! __('to') !!}
                <span class="font-medium">{{ $paginator->lastItem() }}</span>
                {!! __('of') !!}
                <span class="font-medium">{{ $paginator->total() }}</span>
                {!! __('results') !!}
            </p>
        </div>

        <div>
            <span class="d-flex flex-wrap py-2 mr-3">
                {{-- Previous Page Link --}}
                @if ($paginator->onFirstPage())
                <span aria-disabled="true" aria-label="{{ __('pagination.previous') }}">
                    <span class="btn btn-icon btn-sm btn-light-primary mr-2 my-1" aria-hidden="true">
                        <svg class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20">
                            <path fill-rule="evenodd" d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z" clip-rule="evenodd" />
                        </svg>
                    </span>
                </span>
                @else
                <button wire:click="previousPage" rel="prev" class="btn btn-icon btn-sm btn-light-primary mr-2 my-1" aria-label="{{ __('pagination.previous') }}">
                    <svg class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20">
                        <path fill-rule="evenodd" d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z" clip-rule="evenodd" />
                    </svg>
                </button>
                @endif

                {{-- Pagination Elements --}}
                @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                <span aria-disabled="true">
                    <span class="btn btn-icon btn-sm border-0 btn-light mr-2 my-1">{{ $element }}</span>
                </span>
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                @foreach ($element as $page => $url)
                @if ($page == $paginator->currentPage())
                <span aria-current="page">
                    <span class="btn btn-icon btn-sm border-0 btn-light btn-hover-primary active mr-2 my-1">{{ $page }}</span>
                </span>
                @else
                <button wire:click="gotoPage({{ $page }})" class="btn btn-icon btn-sm border-0 btn-light mr-2 my-1" aria-label="{{ __('Go to page :page', ['page' => $page]) }}">
                    {{ $page }}
                </button>
                @endif
                @endforeach
                @endif
                @endforeach

              
            

                {{-- Next Page Link --}}
                @if ($paginator->hasMorePages())
                <button wire:click="nextPage" rel="next" class="btn btn-icon btn-sm btn-light-primary mr-2 my-1" aria-label="{{ __('pagination.next') }}">
                    <svg class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20">
                        <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd" />
                    </svg>
                </button>
                @else
                <span aria-disabled="true" aria-label="{{ __('pagination.next') }}">
                    <span class="btn btn-icon btn-sm btn-light-primary mr-2 my-1" aria-hidden="true">
                        <svg class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20">
                            <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd" />
                        </svg>
                    </span>
                </span>
                @endif
            </span>
        </div>
    </div>
@endif
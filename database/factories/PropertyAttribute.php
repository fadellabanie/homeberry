<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */


use App\Models\PropertyAttribute;
use Faker\Generator as Faker;

$factory->define(PropertyAttribute::class, function (Faker $faker) {
    return [
        'property_id' => factory(\App\Models\Property::class)->create()->id,
        'availability' => $faker->date,
        'guest' => rand(1, 10),
        'room' => rand(1, 10),
        'bathroom' => rand(1, 10),
        'area' => rand(111, 999),
        'park' =>  rand(1, 5),
        'location_name' =>  $faker->address,
        'rental' => $faker->randomElement(['daily', 'weekly', 'monthly', 'yearly']),
        'furnished' => $faker->randomElement(['furnished', 'semi furnished', 'unfurnished']),
        'type' => $faker->randomElement(['buy', 'rent']),
        'lat' => $faker->randomFloat($nbMaxDecimals = NULL, $min = 0, $max = 100),
        'lng' => $faker->randomFloat($nbMaxDecimals = NULL, $min = 0, $max = 100),
        'youtube' => $faker->url,
        'url' => $faker->url,
      


    ];
});

<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Models\Package;
use Faker\Generator as Faker;

$factory->define(Package::class, function (Faker $faker) {
    $arraytype = ['photo-sessions','subscription'];
    return [
        'type' => $arraytype[rand(0,1)],
        'name_ar' => $faker->name,
        'name_en' => $faker->name,
        'description_ar' => $faker->paragraph,
        'description_en' => $faker->paragraph,
        'price' => rand(1, 999),
        'count' => rand(1, 365),
    ];
});

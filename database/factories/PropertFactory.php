<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Property;
use Faker\Generator as Faker;

$factory->define(Property::class, function (Faker $faker) {
 
    $arraytype = ['day','month','year'];

    return [
        'name_ar' => $faker->name,
        'name_en' => $faker->name,
        'description_ar' => $faker->paragraph,
        'description_en' => $faker->paragraph,
        'price' => rand(1111,9999),
        'views' => rand(1,9999),
        'city_id' =>  rand(1,3),
        'user_id' =>  factory(\App\User::class)->create()->id,
        'payment_id' => rand(1, 2),
        'activity_type_id' => rand(1, 8),
        'rent_type' => $arraytype[rand(0,2)],
        'status' => 1,
        'priority' => 1,
    ];
});

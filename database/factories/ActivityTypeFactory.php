<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */


use App\Models\ActivityType;

use Faker\Generator as Faker;

$factory->define(ActivityType::class, function (Faker $faker) {
    return [
        'name_ar' => $faker->name,
        'name_en' => $faker->name,
        'icon' => $faker->imageUrl,
        'activity_id' =>1,
        'status' => 1,
    ];
});

<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Media;
use Faker\Generator as Faker;

$factory->define(Media::class, function (Faker $faker) {
    $mediable_type = ['App\Models\Project','App\Models\Property'];
    return [
       
        'file' => $faker->imageUrl,
        'mediable_id' => rand(1,40),
        'mediable_type'=> $mediable_type[rand(0,1)],
    ];
});

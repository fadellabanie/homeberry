<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Models\Feature;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\DB;

$factory->define(Feature::class, function (Faker $faker) {
    $arraytype = ['nearby services', 'amenities'];

    // DB::table('feature_property')->insert([
    //     'feature_id' => rand(1,10),
    //     'property_id' => rand(1, 10),
    // ]);

    return [
        'name_ar' => $faker->city,
        'name_en' => $faker->city,
        'type' => $arraytype[rand(0,1)],
        'icon' => $faker->imageUrl,
    ];


});

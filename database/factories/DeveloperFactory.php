<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Models\Devolper;
use Faker\Generator as Faker;

$factory->define(Devolper::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'description' => $faker->paragraph,
        'logo'=> $faker->imageUrl,
        'mobile'=> $faker->phoneNumber,
        'email'=> $faker->email,
        'whatsapp_mobile'=> $faker->phoneNumber,
    ];
});

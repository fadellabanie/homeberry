<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */


use App\Models\Project;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\DB;

$factory->define(Project::class, function (Faker $faker) {


//  DB::table('activitytype_project')->insert([
//         'activity_type_id' => rand(1,10),
//         'project_id' => rand(1, 10),
//     ]);


    $finished = ['fully', 'semi'];
    $delivery = ['ready to move', '1 years', '2 years', '3 years'];
    return [
        'city_id' =>  rand(1, 5),
        'devolper_id' => factory(\App\Models\Devolper::class)->create()->id,
        'payment_id' => rand(1, 2),
        'name_ar' => $faker->name,
        'name_en' => $faker->name,
        'description_ar' => $faker->paragraph,
        'description_en' => $faker->paragraph,
        
        'views' => rand(1,9999),
        'status' => 1,
        'priority' => 1,
        'finished' => $finished[rand(0,1)],
        'delivery' => $delivery[rand(0, 3)],
        'link' => $faker->url,
        'youtube' => $faker->url,
        'virtual_tour_url' => $faker->url,
        'location' => $faker->country,
        'lat' => $faker->randomFloat($nbMaxDecimals = NULL, $min = 0, $max = 100),
        'lng' => $faker->randomFloat($nbMaxDecimals = NULL, $min = 0, $max = 100),

    ];
});

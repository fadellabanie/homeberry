<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Models\PhotoSession;
use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(PhotoSession::class, function (Faker $faker) {
    return [
        'user_id' => rand(1, 10),
        'package_id' => factory(\App\Models\Package::class)->create()->id,
        'location_name' => $faker->name,
        'date' => Carbon::now()->addDays(rand(1, 10)),
        'time' => mt_rand(0, 23) . ":" . str_pad(mt_rand(0, 59), 2, "0", STR_PAD_LEFT),
        'lat' => $faker->randomFloat($nbMaxDecimals = NULL, $min = 0, $max = 100),
        'lng' => $faker->randomFloat($nbMaxDecimals = NULL, $min = 0, $max = 100),
    ];
});

<?php

use App\Models\ActivityType;
use App\Models\City;
use App\Models\Feature;
use App\Models\Media;
use App\Models\Package;
use App\Models\Project;
use App\Models\PropertyAttribute;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // DB::beginTransaction();
    //factory(Feature::class, 20)->create();
   // factory(Package::class, 20)->create();
    factory(PropertyAttribute::class, 20)->create();
    factory(Project::class, 20)->create();
       /*
       factory(ActivityType::class, 20)->create();
       factory(City::class, 20)->create();
       

         factory(Media::class, 40)->create();
*/
    }
}

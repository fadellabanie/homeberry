<?php

use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddDataToPermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      
        DB::table('roles')->insert([
            'name' => 'superuser',
            'guard_name' => 'web',
            'created_at' => Carbon::now(),
        ]);
          DB::table('roles')->insert([
            'name' => 'manager',
            'guard_name' => 'web',
            'created_at' => Carbon::now(),
        ]);
          DB::table('roles')->insert([
            'name' => 'operator',
            'guard_name' => 'web',
            'created_at' => Carbon::now(),
        ]);
        ######################################################################################
        ######################################################################################
        ######################################################################################
        DB::table('permissions')->insert([
            'name' => 'add-user',
            'guard_name' => 'web',
            'created_at' => Carbon::now(),
        ]);
        DB::table('permissions')->insert([
            'name' => 'edit-user',
            'guard_name' => 'web',
            'created_at' => Carbon::now(),

        ]);
        ######################################################################################

        #######################################
        ### Only Developers show this route ###
        #######################################
        DB::table('permissions')->insert([
            'name' => 'show-permission',
            'guard_name' => 'web',
            'created_at' => Carbon::now(),

        ]);
        DB::table('permissions')->insert([
            'name' => 'all-permissions',
            'guard_name' => 'web',
            'created_at' => Carbon::now(),

        ]);
        DB::table('permissions')->insert([
            'name' => 'add-permission',
            'guard_name' => 'web',
            'created_at' => Carbon::now(),

        ]);
        DB::table('permissions')->insert([
            'name' => 'edit-permission',
            'guard_name' => 'web',
            'created_at' => Carbon::now(),

        ]);
        DB::table('permissions')->insert([
            'name' => 'delete-permission',
            'guard_name' => 'web',
            'created_at' => Carbon::now(),

        ]);
        ######################################################################################
        ######################################################################################
        ######################################################################################
        DB::table('permissions')->insert([
            'name' => 'add-group',
            'guard_name' => 'web',
            'created_at' => Carbon::now(),

        ]);
        DB::table('permissions')->insert([
            'name' => 'show-group',
            'guard_name' => 'web',
            'created_at' => Carbon::now(),

        ]);
        DB::table('permissions')->insert([
            'name' => 'all-groups',
            'guard_name' => 'web',
            'created_at' => Carbon::now(),

        ]);
        DB::table('permissions')->insert([
            'name' => 'edit-group',
            'guard_name' => 'web',
            'created_at' => Carbon::now(),

        ]);
        DB::table('permissions')->insert([
            'name' => 'delete-group',
            'guard_name' => 'web',
            'created_at' => Carbon::now(),

        ]);
        DB::table('permissions')->insert([
            'name' => 'give-permissions-to-group',
            'guard_name' => 'web',
            'created_at' => Carbon::now(),
        ]);
        ######################################################################################
            ######################################################################################
            ######################################################################################
            DB::table('permissions')->insert([
                'name' => 'add-properties',
                'guard_name' => 'web',
                'created_at' => Carbon::now(),
            ]); 
             DB::table('permissions')->insert([
                'name' => 'show-properties',
                'guard_name' => 'web',
                'created_at' => Carbon::now(),
            ]); 
            DB::table('permissions')->insert([
                'name' => 'edit-properties',
                'guard_name' => 'web',
                'created_at' => Carbon::now(),
            ]);
             DB::table('permissions')->insert([
                'name' => 'delete-properties',
                'guard_name' => 'web',
                'created_at' => Carbon::now(),
            ]);
            // DB::table('permissions')->insert([
            //     'name' => 'add-location-properties',
            //     'guard_name' => 'web',
            //     'created_at' => Carbon::now(),
            // ]);
             ######################################################################################
            ######################################################################################
            ######################################################################################
            DB::table('permissions')->insert([
                'name' => 'add-projects',
                'guard_name' => 'web',
                'created_at' => Carbon::now(),
            ]); 
             DB::table('permissions')->insert([
                'name' => 'show-projects',
                'guard_name' => 'web',
                'created_at' => Carbon::now(),
            ]); 
            DB::table('permissions')->insert([
                'name' => 'edit-projects',
                'guard_name' => 'web',
                'created_at' => Carbon::now(),
            ]);
             DB::table('permissions')->insert([
                'name' => 'delete-projects',
                'guard_name' => 'web',
                'created_at' => Carbon::now(),
            ]); 
            //  DB::table('permissions')->insert([
            //     'name' => 'add-location-projects',
            //     'guard_name' => 'web',
            //     'created_at' => Carbon::now(),
            // ]);
            ######################################################################################
            ######################################################################################
            ######################################################################################
            DB::table('permissions')->insert([
                'name' => 'add-developers',
                'guard_name' => 'web',
                'created_at' => Carbon::now(),
            ]); 
            DB::table('permissions')->insert([
                'name' => 'edit-developers',
                'guard_name' => 'web',
                'created_at' => Carbon::now(),
            ]);
             DB::table('permissions')->insert([
                'name' => 'delete-developers',
                'guard_name' => 'web',
                'created_at' => Carbon::now(),
            ]); 
             ######################################################################################
            ######################################################################################
            ######################################################################################
            DB::table('permissions')->insert([
                'name' => 'add-packages',
                'guard_name' => 'web',
                'created_at' => Carbon::now(),
            ]); 
            DB::table('permissions')->insert([
                'name' => 'edit-packages',
                'guard_name' => 'web',
                'created_at' => Carbon::now(),
            ]);
             DB::table('permissions')->insert([
                'name' => 'delete-packages',
                'guard_name' => 'web',
                'created_at' => Carbon::now(),
            ]); 
             ######################################################################################
            ######################################################################################
            ######################################################################################
            DB::table('permissions')->insert([
                'name' => 'add-packages-photo-sessions',
                'guard_name' => 'web',
                'created_at' => Carbon::now(),
            ]); 
            DB::table('permissions')->insert([
                'name' => 'edit-packages-photo-sessions',
                'guard_name' => 'web',
                'created_at' => Carbon::now(),
            ]);
             DB::table('permissions')->insert([
                'name' => 'delete-packages-photo-sessions',
                'guard_name' => 'web',
                'created_at' => Carbon::now(),
            ]); 
            ######################################################################################
            ######################################################################################
            ######################################################################################
            DB::table('permissions')->insert([
                'name' => 'show-schedules',
                'guard_name' => 'web',
                'created_at' => Carbon::now(),
            ]); 
            DB::table('permissions')->insert([
                'name' => 'show-photo_sessions',
                'guard_name' => 'web',
                'created_at' => Carbon::now(),
            ]);
             DB::table('permissions')->insert([
                'name' => 'show-booking',
                'guard_name' => 'web',
                'created_at' => Carbon::now(),
            ]); 
            ######################################################################################
            ######################################################################################
            ######################################################################################
            DB::table('permissions')->insert([
                'name' => 'add-cities',
                'guard_name' => 'web',
                'created_at' => Carbon::now(),
            ]); 
            DB::table('permissions')->insert([
                'name' => 'edit-cities',
                'guard_name' => 'web',
                'created_at' => Carbon::now(),
            ]);
          
            ######################################################################################
            ######################################################################################
            ######################################################################################
            DB::table('permissions')->insert([
                'name' => 'add-governorates',
                'guard_name' => 'web',
                'created_at' => Carbon::now(),
            ]); 
            DB::table('permissions')->insert([
                'name' => 'edit-governorates',
                'guard_name' => 'web',
                'created_at' => Carbon::now(),
            ]);
            
            
            DB::table('model_has_roles')->insert([
                'role_id' => '1',
                'model_type' => 'App\User',
                'model_id' => '1',
            ]); 
            DB::table('role_has_permissions')->insert([
                'role_id' => '1',
                'permission_id' => '8',
                
            ]); DB::table('role_has_permissions')->insert([
                'role_id' => '1',
                'permission_id' => '9',
                
            ]); DB::table('role_has_permissions')->insert([
                'role_id' => '1',
                'permission_id' => '10',
                
            ]); DB::table('role_has_permissions')->insert([
                'role_id' => '1',
                'permission_id' => '11',
                
            ]); DB::table('role_has_permissions')->insert([
                'role_id' => '1',
                'permission_id' => '12',
                
            ]); DB::table('role_has_permissions')->insert([
                'role_id' => '1',
                'permission_id' => '13',
                
            ]); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('permissions', function (Blueprint $table) {
            //
        });
    }
}

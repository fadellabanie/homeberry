<?php

use App\Http\Controllers\GeneralController;
use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('email')->unique()->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('country_code')->nullable();
            $table->string('mobile')->nullable();
            $table->string('whatsapp_mobile')->nullable();
            $table->string('password')->nullable();
            $table->enum('type',['user','admin','guest'])->default('user');
            $table->unsignedInteger('property_count')->nullable();
            $table->text('remember_token')->nullable();
            
            $table->timestamps();
        });
        DB::table('users')->insert([

            'name' => 'admin',
            'last_name' => 'admin',
            'email' => 'admin@admin.com',
            'password' => Hash::make(12345678),
            'country_code' => 'EG',
            'mobile' => '0113110850',
            'whatsapp_mobile' => '0113110850',
            'type' => 'admin',
            'property_count' => 1000000,
            'email_verified_at' => Carbon::now(),

        ]);

        DB::table('users')->insert([

            'name' => 'user',
            'last_name' => 'user',
            'email' => 'user@user.com',
            'password' => Hash::make(12345678),
            'country_code' => 'EG',
            'mobile' => '0113110851',
            'whatsapp_mobile' => '0113110851',
            'type' => 'user',
            'property_count' => 5,
            'email_verified_at' => Carbon::now(),
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

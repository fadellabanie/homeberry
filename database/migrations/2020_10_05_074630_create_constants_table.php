<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateConstantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('constants', function (Blueprint $table) {
            $table->id();
            $table->string('key');
            $table->string('value');
        });

        DB::table('constants')->insert([
            'key'=>'cleaning fee',
            'value'=>'16',
        ]);
        DB::table('constants')->insert([
            'key'=>'service fee',
            'value'=>'59',
        ]);
         DB::table('constants')->insert([
            'key'=>'property count',
            'value'=>'5',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('constants');
    }
}

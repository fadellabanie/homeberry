<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropertyAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        ###########################
        ## Appartmant  attributes ##
        ###########################
        Schema::create('property_attributes', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('property_id');

            $table->date('availability');
            $table->tinyInteger('guest')->nullable();
            $table->tinyInteger('room')->default(1)->comment('office or bedroom');
            $table->tinyInteger('bathroom')->default(1);
            $table->integer('area');
            $table->tinyInteger('park')->default(0);
            $table->string('location_name')->nullable();
            $table->string('url')->nullable();
            
            $table->string('youtube')->nullable();

            $table->enum('rental',['daily','weekly','monthly','yearly']);
            $table->enum('furnished',['furnished','semi furnished','unfurnished']);
            $table->enum('type',['buy','rent']);
            $table->decimal('lng', 10, 7)->nullable();
            $table->decimal('lat', 10, 7)->nullable();

            $table->foreign('property_id')->references('id')->on('properties')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_attributes');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedules', function (Blueprint $table) {
            // $table->unsignedBigInteger("user_id");
            // $table->unsignedBigInteger("property_id");
            // $table->date("date");

            // $table->unique(['user_id','property_id']);
            // $table->foreign('user_id')->references('id')->on('users')
            // ->onDelete('cascade')
            // ->onUpdate('cascade');

            // $table->foreign('property_id')->references('id')->on('properties')
            // ->onDelete('cascade')
            // ->onUpdate('cascade');
            $table->bigIncrements('id');
            $table->text("name");
            $table->text("email");
            $table->text("mobile");
            $table->date("date");
            $table->integer("schedulable_id");
            $table->string("schedulable_type");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedules');
    }
}

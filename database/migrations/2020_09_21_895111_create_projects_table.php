<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('devolper_id');
            $table->unsignedBigInteger('city_id');
            $table->unsignedBigInteger('payment_id');

            $table->string('name_ar')->nullable();
            $table->string('name_en')->nullable();
            $table->text('description_ar')->nullable();
            $table->text('description_en')->nullable();
           
            $table->enum('finished',['fully','semi']);
            $table->enum('delivery',['ready to move',
            '1 years',
            '2 years',
            '3 years',
            '4 years',
            '5 years',
            '6 years',
            '7 years',
            '8 years',
            '9 years',
            '10 years',
            ]);
            $table->string('virtual_tour_url')->nullable();
            $table->string('youtube')->nullable();
            $table->string('link')->nullable();
            $table->string('location')->nullable();
            $table->boolean('is_constructed')->default(1);



            $table->integer('views')->default(0);
            $table->integer('clicked')->default(0);

            $table->decimal('lng', 10, 7)->nullable();
            $table->decimal('lat', 10, 7)->nullable();
            $table->boolean('priority')->default(false);
            $table->boolean('status')->default(false);
            $table->timestamps();

            $table->foreign('payment_id')->references('id')->on('payment_methods');

            $table->foreign('city_id')->references('id')->on('cities')
            ->onDelete('cascade')
            ->onUpdate('cascade');

            $table->foreign('devolper_id')->references('id')->on('devolpers')
            ->onDelete('cascade')
            ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}

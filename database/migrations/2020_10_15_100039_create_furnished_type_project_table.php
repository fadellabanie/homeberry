<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFurnishedTypeProjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('furnishedtype_project', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('furnished_type_id');
            $table->unsignedBigInteger('project_id');


            //$table->foreign('furnished_type_id')->references('id')->on('furnished_types');

            $table->foreign('project_id')->references('id')->on('projects')
                ->onDelete('cascade')
                ->onUpdate('cascade');
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('furnishedtype_project');
    }
}

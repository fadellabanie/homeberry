<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateFurnishedTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('furnished_types', function (Blueprint $table) {
            $table->id();
            $table->string('name_ar');
            $table->string('name_en');
         });
        DB::table('furnished_types')->insert([
            'name_ar' => 'مجهز',
            'name_en' => 'furnished',

        ]);
        DB::table('furnished_types')->insert([
            'name_ar' => ' نصف مجهز',
            'name_en' => 'semi furnished',

        ]);
        DB::table('furnished_types')->insert([
            'name_ar' => 'غير مجهز',
            'name_en' => 'unfurnished',

        ]);

    }
     /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('furnished_types');
    }
}

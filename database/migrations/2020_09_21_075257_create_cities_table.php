<?php

use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cities', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('governorate_id');
            $table->string('name_ar');
            $table->string('name_en');
            $table->string('image')->nullable();
            $table->boolean('priority')->default(false);
            $table->boolean('status')->default(false);
            $table->timestamps();

            $table->foreign('governorate_id')->references('id')->on('governorates')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        DB::table('cities')->insert([
            'governorate_id' => 1,
            'name_ar' => 'مدينتي',
            'name_en' => 'Madinaty',
            'priority' => true,
            'status' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('cities')->insert([
            'governorate_id' => 1,
            'name_ar' => 'الرحاب',
            'name_en' => 'Al Rehab',
            'priority' => true,
            'status' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('cities')->insert([
            'governorate_id' => 1,
            'name_ar' => 'التجمع الخامس',
            'name_en' => 'The 5th Settlement',
            'priority' => true,
            'status' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('cities')->insert([
            'governorate_id' => 2,
            'name_ar' => 'المهندسين',
            'name_en' => 'Mohandessin',
            'priority' => true,
            'status' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('cities')->insert([
            'governorate_id' => 2,
            'name_ar' => 'الزمالك',
            'name_en' => 'Zamalek',
            'priority' => true,
            'status' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cities');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreatePaymentMethodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_methods', function (Blueprint $table) {
            $table->id();
            $table->string('name_ar');
            $table->string('name_en');
            $table->boolean('status')->default(false);
          
        });
        DB::table('payment_methods')->insert([
            'name_ar' => 'كاش',
            'name_en' => 'Cash',
            'status' => true,
           
        ]);
        DB::table('payment_methods')->insert([
            'name_ar' => 'تقسيط',
            'name_en' => 'Installment ',
            'status' => true,
           
        ]);
        DB::table('payment_methods')->insert([
            'name_ar' => 'كاش و تقسيط',
            'name_en' => 'cash & Installment',
            'status' => true,
           
        ]);
        DB::table('payment_methods')->insert([
            'name_ar' => 'فيزا',
            'name_en' => 'Visa',
            'status' => true,
           
        ]);
      
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_methods');
    }
}

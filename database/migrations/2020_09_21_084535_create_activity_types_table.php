<?php

use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateActivityTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activity_types', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('activity_id');
            $table->string('icon')->nullable();
            $table->string('name_ar');
            $table->string('name_en');
            $table->string('logo');
            $table->boolean('status')->default(false);
            $table->timestamps();

            $table->foreign('activity_id')->references('id')->on('activities')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
        DB::table('activity_types')->insert([
            'activity_id' => 2,
            'name_ar' => 'فيلا',
            'name_en' => 'villa',
            'logo' => 'storage/app/uploads/activityTypes/villa.png',

            'status' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('activity_types')->insert([
            'activity_id' => 2,
            'name_ar' => 'تاون هاوس',
            'name_en' => 'Townhouse',
            'logo' => 'storage/app/uploads/activityTypes/Townhouse.png',

            'status' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('activity_types')->insert([
            'activity_id' => 2,
            'name_ar' => 'توين هاوس',
            'name_en' => 'Twinhouse',
            'logo' => 'storage/app/uploads/activityTypes/Twinhouse.png',
            'status' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('activity_types')->insert([
            'activity_id' => 2,
            'name_ar' => 'دوبلكس',
            'name_en' => 'Duplex',
            'logo' => 'storage/app/uploads/activityTypes/duplex.png',
            'status' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('activity_types')->insert([
            'activity_id' => 2,
            'name_ar' => 'شاليه',
            'name_en' => 'Chalet',
            'logo' => 'storage/app/uploads/activityTypes/chalet.png',
            'status' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('activity_types')->insert([
            'activity_id' => 2,
            'name_ar' => 'استوديو',
            'name_en' => 'Studio',
            'logo' => 'storage/app/uploads/activityTypes/Studio.png',
            'status' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('activity_types')->insert([
            'activity_id' => 2,
            'name_ar' => 'مبنى سكني',
            'name_en' => 'Residential Building',
            'logo' => 'storage/app/uploads/activityTypes/residential-Building.png',
            'status' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('activity_types')->insert([
            'activity_id' => 2,
            'name_ar' => 'ارض سكنيه',
            'name_en' => 'Residential Land',
            'logo' => 'storage/app/uploads/activityTypes/residential-Land.png',
            'status' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('activity_types')->insert([
            'activity_id' => 2,
            'name_ar' => 'بنتهاوس',
            'name_en' => 'Penthouse',
            'logo' => 'storage/app/uploads/activityTypes/penthouse.png',
            'status' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('activity_types')->insert([
            'activity_id' => 2,
            'name_ar' => 'شقة متشطبة',
            'name_en' => 'Finished apartment',
            'logo' => 'storage/app/uploads/activityTypes/Finished-apartment.png',
            'status' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('activity_types')->insert([
            'activity_id' => 2,
            'name_ar' => 'شقة',
            'name_en' => 'Apartment',
            'logo' => 'storage/app/uploads/activityTypes/Apartment.png',
            'status' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        #############################################################
        #############################################################
        #############################################################
        DB::table('activity_types')->insert([
            'activity_id' => 1,
            'name_ar' => 'مساحة عمل',
            'name_en' => 'office space',
            'logo' => 'storage/app/uploads/activityTypes/office-space.png',
            'status' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('activity_types')->insert([
            'activity_id' => 1,
            'name_ar' => 'وحدة طبيه',
            'name_en' => 'medical facility ',
            'logo' => 'storage/app/uploads/activityTypes/medical-facility.png',
            'status' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('activity_types')->insert([
            'activity_id' => 1,
            'name_ar' => 'تجاري',
            'name_en' => 'retail',
            'logo' => 'storage/app/uploads/activityTypes/retail.png',
            'status' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('activity_types')->insert([
            'activity_id' => 1,
            'name_ar' => 'مخزن',
            'name_en' => 'warehouse',
            'logo' => 'storage/app/uploads/activityTypes/warehouse.png',
            'status' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('activity_types')->insert([
            'activity_id' => 1,
            'name_ar' => 'محل',
            'name_en' => 'shop',
            'logo' => 'storage/app/uploads/activityTypes/shop.png',
            'status' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('activity_types')->insert([
            'activity_id' => 1,
            'name_ar' => 'معرض',
            'name_en' => 'showroom',
            'logo' => 'storage/app/uploads/activityTypes/showroom.png',
            'status' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('activity_types')->insert([
            'activity_id' => 1,
            'name_ar' => 'دور كامل',
            'name_en' => 'full floor',
            'logo' => 'storage/app/uploads/activityTypes/full-floor.png',
            'status' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('activity_types')->insert([
            'activity_id' => 1,
            'name_ar' => 'نص دور',
            'name_en' => 'half floor ',
            'logo' => 'storage/app/uploads/activityTypes/half-floor.png',
            'status' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('activity_types')->insert([
            'activity_id' => 1,
            'name_ar' => 'مبنى تجاري',
            'name_en' => 'commercial building',
            'logo' => 'storage/app/uploads/activityTypes/commercial-building.png',
            'status' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('activity_types')->insert([
            'activity_id' => 1,
            'name_ar' => 'ارض تجاريه',
            'name_en' => 'commercial land',
            'logo' => 'storage/app/uploads/activityTypes/commercial-land.png',
            'status' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activity_types');
    }
}

<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


Route::group(['prefix' => 'v1'], function () {
    Route::group(['namespace' => 'API\V1\User'], function () {

        #################################################
        ##############     Authentication   #############
        #################################################

        Route::post('register', 'AuthController@register');
        Route::post('login', 'AuthController@login');
        Route::post('verify-code/{type}/{mobile}', 'AuthController@verifyCode');
        Route::post('forget-password', 'AuthController@forgetPassword');
        Route::post('reset-password/{mobile}', 'AuthController@resetPassword');
        Route::post('resend/{type}/{mobile}', 'AuthController@resend');

        Route::post('verify', 'AuthController@verify');
        Route::get('logout', 'AuthController@logout')->middleware('auth:api');
        Route::post('user-tokens', 'UserTokenController@store')->middleware('auth:api');

        #################################################
        #######  Authentication With Phone    ########
        #################################################
        Route::post('phone-register', 'PhoneAuthController@register');
        Route::post('phone-update-name/{mobile}', 'PhoneAuthController@updateName')->middleware('auth:api');

        Route::post('phone-login', 'PhoneAuthController@login');

        Route::post('check-user', 'UserController@checkUser');
        Route::post('check-verify', 'UserController@checkVerify');

       



        Route::post('register-token', 'UserTokenController@store');
    });

    Route::group(['namespace' => 'API\V1\General'], function () {

        Route::GET('home', 'HomeController@home');
        Route::GET('general-const', 'HomeController@general');

        ##########################
        #######  Search   ########
        ##########################
        Route::POST('get-progerties-by-type', 'HomeController@getProgertiesByType');
        Route::POST('search', 'SearchController@search');
        Route::post('properties-search', 'SearchController@filterProperties');
        Route::post('projects-search', 'SearchController@filterProjects');
        Route::post('developers-search', 'SearchController@searchDevelopers');
        ##############################
        #######  Properties   #########
        ###############################

        Route::apiResource('properties', 'PropertyController');
        Route::GET('click-property/{id}', 'PropertyController@clickedProperty')->middleware(['auth:api','IsActiveToken']);
        Route::POST('compares', 'PropertyController@compares');
        Route::GET('user-properties', 'PropertyController@listUserProperties');

        ##############################
        #######  Projects   #########
        ###############################

        Route::apiResource('projects', 'ProjectController');
        Route::GET('developer-projects/{id}', 'ProjectController@showDeveloper');
        Route::GET('click-project/{id}', 'ProjectController@clickedProject')->middleware(['auth:api','IsActiveToken']);

        ##############################
        #######  cities   #########
        ###############################
        Route::GET('cities', 'CityController@index');
        Route::GET('cities/{id}', 'CityController@show');


        #########################################
        #######                   ################
        ######        General  API    ###############
        #######                   ###############
        #########################################
        Route::apiResource('notes', 'NoteController');
        Route::apiResource('favourites', 'FavouriteController')->middleware(['auth:api','IsActiveToken']);

        Route::GET('packages-photo-sessions', 'PhotoSessionController@index');
        Route::POST('reserve-photo-sessions', 'PhotoSessionController@reserve')->middleware(['auth:api','IsActiveToken']);

        #################################################
        #######  Notifications   ######################
        #################################################
       // Route::post('test-notification', 'NotificationController@testNotification');
        //Route::post('send-notification', 'NotificationController@send');

        Route::post('property-schedules-view', 'ScheduleViewController@propertyScheduleView');
        Route::post('project-schedules-view', 'ScheduleViewController@projectScheduleView');
        
        Route::post('property-view-contact', 'ViewContactController@propertyViewContact');
        Route::post('project-view-contact', 'ViewContactController@projectViewContact');

        Route::post('calaculate', 'BookingController@calaculate')->middleware(['auth:api','IsActiveToken']);
        Route::post('booking', 'BookingController@reserve')->middleware(['auth:api','IsActiveToken']);

        Route::GET('packages', 'PackageController@index');
        Route::POST('subscribe', 'PackageController@subscribe');

        Route::POST('payments', 'PaymnetController@paymentsToken');
    });
});

<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' =>'auth'], function () {

Route::get('/home', Home::class)->name('admin.home');

Route::get('/cities', Cities::class)->name('cities');
Route::get('/add-city', CitiesAdd::class)->name('add-cities')->middleware('permission:add-cities');
Route::get('/edit-city/{city}', CitiesEdit::class)->name('edit-cities')->middleware('permission:edit-cities');

Route::get('/users', Users::class)->name('users');
Route::get('show-user/{user}', UserShow::class)->name('show-user');
Route::get('add-users', UsersAdd::class)->name('add-users')->middleware('permission:add-user');
Route::get('edit-users/{user}', UsersEdit::class)->name('edit-users')->middleware('permission:edit-user');
Route::get('export-user', 'ExcelController@UserExport')->name('export-user');


######################   Hide from Dashboard Make it Statuc It was agreed ################

Route::get('activity-types', ActivityTypes::class)->name('activity-types');
Route::get('add-activity-type', ActivityTypesAdd::class)->name('add-activity-type');
Route::get('edit-activity-type/{activityType}',ActivityTypesEdit::class)->name('edit-activity-type');


Route::get('governorates', Governorates::class)->name('governorates');
Route::get('add-governorates', GovernoratesAdd::class)->name('add-governorates')->middleware('permission:add-governorates');
Route::get('edit-governorates/{governorate}',GovernoratesEdit::class)->name('edit-governorates')->middleware('permission:edit-governorates');

Route::get('properties', Properties::class)->name('properties');
Route::get('add-property', PropertiesAdd::class)->name('add-property')->middleware('permission:add-properties');
Route::get('edit-property/{property}', PropertiesEdit::class)->name('edit-property')->middleware('permission:edit-properties');
Route::get('show-property/{property}', PropertyShow::class)->name('show-property')->middleware('permission:show-properties');
Route::get('export-property', 'ExcelController@PropertyExport')->name('export-property');
Route::GET('add-loaction-property', 'AddLocationController@indexProperty')->middleware('permission:add-properties');
Route::POST('add-loaction-property', 'AddLocationController@addLocationToProperty')->name('add-loaction-property')->middleware('permission:add-location-properties');


Route::get('projects', Projects::class)->name('projects');
Route::get('add-project', ProjectsAdd::class)->name('add-project')->middleware('permission:add-projects');
Route::get('edit-project/{project}', ProjectsEdit::class)->name('edit-project')->middleware('permission:edit-projects');
Route::get('show-project/{project}', ProjectShow::class)->name('show-project')->middleware('permission:show-projects');
Route::get('export-project', 'ExcelController@ProjectExport')->name('export-project');

Route::GET('add-loaction-project', 'AddLocationController@index')->name('add-loaction-project')->middleware('permission:add-projects');
Route::POST('add-loaction-project', 'AddLocationController@addLocationToProject')->name('add-loaction-project')->middleware('permission:add-location-projects');


Route::get('developers', Developers::class)->name('developers');
Route::get('add-developer', DevelopersAdd::class)->name('add-developers')->middleware('permission:add-developers');
Route::get('edit-developer/{developer}', DevelopersEdit::class)->name('edit-developers')->middleware('permission:edit-developers');
Route::get('export-developer', 'ExcelController@DeveloperExport')->name('export-developer');



Route::get('packages', Packages::class)->name('packages');
Route::get('add-packages', PackagesAdd::class)->name('add-packages')->middleware('permission:add-packages');
Route::get('edit-packages/{package}', PackagesEdit::class)->name('edit-packages')->middleware('permission:edit-packages');

Route::get('packages-photo-sessions', PackagePhotoSessions::class)->name('packages-photo-sessions');

Route::get('schedules', Schedules::class)->name('schedules')->middleware('permission:show-schedules');
Route::get('photo_sessions', PhotoSessions::class)->name('photo_sessions')->middleware('permission:show-photo_sessions');

    //  Route::resource('governorates', 'GovernorateController');

/*
Route::get('notifications', Notifications::class)->name('notifications');
Route::get('add-notification', NotificationsAdd::class)->name('add-notification');
Route::get('send-notification/{notification}', NotificationsSend::class)->name('send-notification');
*/

######################   Hide from Dashboard Make it Statuc It was agreed ################
Route::get('features', Features::class)->name('features');
Route::get('add-feature', FeaturesAdd::class)->name('add-features');
Route::get('edit-feature/{feature}', FeaturesEdit::class)->name('edit-features');


Route::get('booking', Booking::class)->name('booking')->middleware('permission:show-booking');

######################   Roles & Permissions ################

/**
 * Roles
*/
Route::get('groups', Groups::class)->name('groups')->middleware('permission:show-group');
Route::get('add-groups', GroupsAdd::class)->name('add-groups')->middleware('permission:add-group');
Route::get('edit-groups/{role}', GroupsEdit::class)->name('edit-groups')->middleware('permission:edit-group');
Route::get('give-permission-to-group', GivePermissionToGroup::class)->name('give-permission-to-group')->middleware('permission:give-permissions-to-group');

/**
 * Permissions
*/
        #######################################
        ### Only Developers show this route ###
        #######################################
Route::get('permissions', Permissions::class)->name('permissions')->middleware('permission:show-properties');
Route::get('add-permissions', PermissionsAdd::class)->name('add-permissions');
Route::get('edit-permissions/{permission}', PermissionsEdit::class)->name('edit-permissions');

});

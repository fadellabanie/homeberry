# Home Berry

Home Berry is a cloud-enabled, mobile-ready, offline-storage, php powered HTML5 Markdown editor.

  - Type some Markdown on the left
  - See HTML in the right
  - Magic

### Installation

Install the dependencies and devDependencies and start the server.

```sh
$ git clone ......
$ composer install 
$ php artisan key:generate
$ php artisan migrate
```

For production environments...

```sh
$ npm install 
```

### Plugins

1.laravel
2,livewire
3.passport
4.npm
4.


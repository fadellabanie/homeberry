<?php

namespace App\Services;

use App\Http\Controllers\GeneralController;
use App\Traits\ApiResponder;

use App\User;
use Carbon\Carbon;
use Exception;

class WeAccept
{
   use ApiResponder;
  

    public function paymentWeAccept($request)
    {
       
        try{
                $url = "https://accept.paymobsolutions.com/api/auth/tokens";

                $header = array('Content-Type: application/json');
                
                $fields = array('api_key' => GeneralController::PAYMENT_API_KAY);

                $fields = json_encode($fields);

                $data = $this->apiParser($url, $fields, $header);

                $data = json_decode($data);

                
                $auth_token = $data->token;
             
                //$merchant_id = $data->profile->user->id;
                
                // get order key
                $url = "https://accept.paymobsolutions.com/api/ecommerce/orders";

                $user = User::find($request->user_id);
              
                $fields = array(
                    "auth_token" => $auth_token,
                    "merchant_id" => GeneralController::PAYMENT_MERCHANT_ID,
                    "merchant_order_id" => strtotime(Carbon::now()) + $user->id + rand(1000, 9999),
                    "amount_cents" => $request->price ,
                    "expiration" => 3600,
                    "order_id" => mt_rand(1000, 10000000000),
                    "billing_data" => [
                        "apartment" => "NA",
                        "email" => $user->email,
                        "floor" => "NA",
                        "first_name" => $user->name ?? "N/A",
                        "street" => "NA",
                        "building" => "NA",
                        "phone_number" => $user->mobile,
                        "shipping_method" => "PKG",
                        "postal_code" => "NA",
                        "city" => "NA",
                        "country" => "NA",
                        "last_name" => $name ?? "N/A",
                        "state" => "NA",
                    ],
                    "currency" => "USD",
                    "integration_id" => GeneralController::PAYMENT_INTEGRATION_CARD_ID,
                    "lock_order_when_paid" => "false"
                );
              
                $fields = json_encode($fields);

                $data = json_decode($this->apiParser($url, $fields, $header));     

                $url = "https://accept.paymobsolutions.com/api/acceptance/payment_keys";

                $fields = array(
                    "auth_token" => $auth_token,
                    "amount_cents" =>  $request->price * 100,
                    "expiration" => 36000,
                    "order_id" => $data->id,
                    "billing_data" => [
                        /*
                        "apartment" => "803",
                        "email" => $user->email,
                        "floor" => "3",
                        "first_name" => $name[0] ?? "N/A",
                        "street" => $street,
                        "building" => $building,
                        "phone_number" => $user->phone,
                        "shipping_method" => "PKG",
                        "postal_code" => $postal_code,
                        "city" => $city,
                        "country" => $country,
                        "last_name" => $name[1] ?? "N/A",
                        "state" => $government,
                        */
                        "apartment" => "NA",
                        "email" => $user->email,
                        "floor" => "NA",
                        "first_name" => $user->name ?? "N/A",
                        "street" => "NA",
                        "building" => "NA",
                        "phone_number" => $user->mobile,
                        "shipping_method" => "PKG",
                        "postal_code" => "NA",
                        "city" => "NA",
                        "country" => "NA",
                        "last_name" => $user->name ?? "N/A",
                        "state" => "NA",
                    ],
                    "currency" => "EGP",
                    "integration_id" => GeneralController::PAYMENT_INTEGRATION_CARD_ID,
                    "lock_order_when_paid" => "false"
                );
               
                $fields = json_encode($fields);

                $data = json_decode($this->apiParser($url, $fields, $header));

                $response = [
                    'status' => 1,
                    'payment_url' => GeneralController::PAYMENT_BASE_URL.GeneralController::PAYMENT_IFRAME_KAY."?payment_token=".$data->token,
                ];

                return $response;

            } catch (\Exception $exception) {
                $response = [
                    'status' => 0,
                    'message' => $exception->getMessage(),
                ];
              
                return $response;
            }
        
    }

    public function apiParser($url,$fields, $header = array()){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
    }

}
<?php

namespace App\Services;


use App\Models\Property;
use App\Models\PropertyAttribute;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AdminPropertyService
{
	/**
	 * Create new row.
	 * @param  array $data
	 * @return mixed
	 */
	public static function create($data)
	{
		$user = User::find(Auth::id());
		
		// dd($data);

		DB::beginTransaction();

		$response['success'] = true;
		try {

			$property = Property::create(array_merge(['user_id' => $user->id], $data));

			$property_id = $property->id;

			PropertyAttribute::create(array_merge(['property_id' => $property_id], $data));

			//$property->features()->attach(array_merge($data['amenities'], $data['services']));
			if ($data['amenities'] != []) {
				$property->amenities()->attach($data['amenities']);
			}

			if ($data['services'] != []) {
				$property->nearbyServices()->attach($data['services']);
			}
			$user->decrement('property_count', 1);

			DB::commit();
			$response['property_id'] = $property_id;

			return $response;
		} catch (\Exception $exception) {
			DB::rollback();

			$response['success'] = false;
			$response['message'] = $exception->getMessage();
			return $response;
		}
	}

	/**
	 * Updat existing row
	 * @param  array $data
	 * @param  Model $model
	 * @return mixed
	 */
	public static function update($data, Property $property)
	{
		$user = User::find(Auth::id());

		DB::beginTransaction();

		$response['success'] = true;

		try {

			$property_id = $property->id;

			// insert in propertiess table
			$property->update(array_merge(['user_id' => $user->id], $data));

			$property->propertyAttribute->update(array_merge(['property_id' => $property_id], $data));

			if (!empty($data['lastamenities'])) {
				$property->amenities()->detach($data['lastamenities']);
				$property->amenities()->attach($data['amenities']);
			}
			if (!empty($data['lastservices'])) {
				$property->nearbyServices()->detach($data['lastservices']);
				$property->nearbyServices()->attach($data['services']);
			}

			DB::commit();
			$response['property_id'] = $property_id;

			return $response;
		} catch (\Exception $exception) {
			DB::rollback();

			$response['success'] = false;
			$response['message'] = $exception->getMessage();
			return $response;
		}
	}
}

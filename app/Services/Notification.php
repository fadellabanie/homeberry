<?php

namespace App\Services;

use GuzzleHttp\Client;

class Notification
{
    /**
     * sending push message to single user by firebase reg id
     *
     * @param  $to
     * @param  $title
     * @param  $message
     * @param  $data
     * @return mixed
     */
    public function send($to, $title, $message)
    {
        $fields = array(
            'to' => $to,
            'notification' => array(
                'body'   => $message,
                'title'     => $title,
                'subtitle'  => '',
                'tickerText'    => '',
                'vibrate'   => 1,
                'sound'     => 1,
                'largeIcon' => 'large_icon',
                'smallIcon' => 'small_icon',
            ),
            'data' => array(
                'title'  => $title,
                'body' => $message
            )
        );

        return $this->sendPushNotification($fields);
    }
  /**
     * sending push message to Android or Ios users by firebase reg id
     *
     * @param  $to 
     * @param  $title
     * @param  $message
     * @param  $data
     * @return mixed
     */
    public function sendMultipleUsers($to, $title, $message)
    {
        $fields = array(
            'to' => '/topics/AllUser'.$to,
            'notification' => array(
                'body'   => $message,
                'title'     => $title,
                'subtitle'  => '',
                'tickerText'    => '',
                'vibrate'   => 1,
                'sound'     => 1,
                'largeIcon' => 'large_icon',
                'smallIcon' => 'small_icon',
            ),
            'data' => array(
                'title'  => $title,
                'body' => $message
            )
        );

        return $this->sendPushNotification($fields);
    }
    /**
     * Sending message to a topic by topic name
     *
     * @param  $to
     * @param  $title
     * @param  $message
     * @param  $data
     * @return mixed
     */
    public function sendToTopic($to, $title, $message)
    {
        $fields = array(
            'to'   => '/topics/' . $to,
            'notification' => array(
                'body'   => $message,
                'title'     => $title,
                'subtitle'  => '',
                'tickerText'    => '',
                'vibrate'   => 1,
                'sound'     => 1,
                'largeIcon' => 'large_icon',
                'smallIcon' => 'small_icon',
            ),
            'data' => array(
                'title'  => $title,
                'body' => $message
            )
        );

        return $this->sendPushNotification($fields);
    }

    /**
     * Sending push message to multiple users by firebase registration ids
     *
     * @param  $registration_ids
     * @param  $title
     * @param  $message
     * @param  $data
     * @return mixed
     */
    public function sendMultiple($registrationIds, $title, $message)
    {
        $fields = array(
            'registration_ids'   => $registrationIds,
            'notification' => array(
                'title' => $title,
                'body' => $message,
                'subtitle' => '',
                'tickerText' => '',
                'vibrate' => 1,
                'sound' => 1,
                'largeIcon' => 'large_icon',
                'smallIcon' => 'small_icon',
            ),
            'data' => array(
                'title'  => $title,
                'body' => $message
            )
        );
        //dd($fields);
        return $this->sendPushNotification($fields);
    }

    /**
     * POST request to firebase servers
     *
     * @param  $fields
     * @return mixed
     */
    private function sendPushNotification($fields)
    {
        /*
        $url = 'https://fcm.googleapis.com/fcm/send';

        $client = new Client();

        $result = $client->post($url, [
            'json'    =>
            $fields,
            'headers' => [
                'Authorization' => 'key=AAAAuKvZxrM:APA91bF-rPCX1p0918j3HPIjb8E066b5cg4kXjk8lawp1UuTEKLgHnfP9UHsfsNhpul4ZqAKKhQyeVpiYemkjPwvJOr86KMXNhIoLEXNLrtm-gDtxRQjGkwePg5xY1BSQ5U1MoQAt9cP',
                'Content-Type'  => 'application/json',
            ],
        ]);

        return json_decode($result->getBody(), true);
        */

        $data = json_encode($fields);

        $headers = array(
            'Authorization: key=AAAApk49wiM:APA91bF0sQA8HazxF1cuyLxgkR-O5bEfXSstDox4fEJEcsXPICbR6zBBfc3nM2brYrZ9m6St2aFCIAy3eIRe37uGlu0d_S2CtbJO--MEeONUbG8LYnyCRstIx-tEqIrgskj07QgWDI-0',
            'Content-Type: application/json');
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $result = curl_exec($ch);
        curl_close($ch);

        //echo $result;
    }
}


<?php

namespace App\Services;

use App\Models\Media;
use App\Models\Property;
use App\Models\PropertyAttribute;
use App\Traits\ApiResponder;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class PropertyService
{
	use ApiResponder;
	/**
	 * Create new row.
	 * @param  array $data
	 * @return mixed
	 */
	public static function create($data)
	{

		DB::beginTransaction();
		$response['success'] = true;
		try {
			
			$user = User::find(Auth::id());
			
			if($data->has('whatsapp_mobile')){
				$user->update(['whatsapp_mobile' => $data->whatsapp_mobile]);
			}

			$property = Property::create([
				'user_id' => $user->id,
				'city_id' => $data->city_id,
				'payment_id' => $data->payment_id,
				'activity_type_id' => $data->activity_type_id,
				'price' => $data->price,
				'rent_type' => $data->rent_type,
			]);

			if (isRtl($data->name)) {
				$property->name_ar = $data->name;
				$property->description_ar = $data->description;
			} else {
				$property->name_en = $data->name;
				$property->description_en = $data->description;
			}
			$property->save();

			$property_id = $property->id;


			PropertyAttribute::create([
				'property_id' => $property_id,
				'availability' => $data->availability,
				'guest' => $data->guest,
				'room' => $data->room,
				'bathroom' => $data->bathroom,
				'area' => $data->area,
				'park' => $data->park,
				'rental' => $data->rental,
				'location_name' => $data->location_name,
				'furnished' => $data->furnished,
				'type' => $data->type,
				'url' => $data->url,
				'lat' => $data->lat,
				'lng' => $data->lng,
			]);

			if ($data->images == null) {
				$response['success'] = false;
				$response['message'] = 'Should upload images';
				return $response;
			}
/*
			//dispatch(new UploadImages($data->images,$property_id));
			
*/	
// foreach ($data->images as $image) {

// 		// your base64 encoded
// 			$image = str_replace('data:image/png;base64,', '', $image);
// 			$image = str_replace(' ', '+', $image);
// 			$imageName = Str::random(10).'.'.'png';
			
		
// 			$success = \File::put(storage_path(). '/' . $imageName, base64_decode($image));
		

// 			dd (base64_encode(file_get_contents($imageName)));
		
// }	 
			
			foreach ($data->images as $value) {
				storeMedia($value, 'properties', $property_id, 'App\Models\Property');
			}
			

			if ($data['amenities'] != []) {
				$property->amenities()->attach($data->amenities);
			}

			if ($data['services'] != []) {
				$property->nearbyServices()->attach($data->services);
			}
			## Decrement Count 
			$user->decrement('property_count', 1);

			DB::commit();

			return $response;
		} catch (\Exception $exception) {
			DB::rollback();
			$response['success'] = false;
			$response['message'] = $exception->getMessage();
			return $response;
		}
	}

	/**
	 * Updat existing row
	 * @param  array $data
	 * @param  Model $model
	 * @return mixed
	 */
	public static function update($data, Property $property)
	{

		$user = User::find(Auth::id());
		DB::beginTransaction();

		$response['success'] = true;

		try {

			if($data->has('whatsapp_mobile')){
				$user->update(['whatsapp_mobile' => $data->whatsapp_mobile]);
			}

			$property->update([
				'user_id' => $user->id,
				'city_id' => $data->city_id,
				'payment_id' => $data->payment_id,
				'activity_type_id' => $data->activity_type_id,
				'price' => $data->price,
			]);
			if (isRtl($data->name)) {
				$property->name_ar = $data->name;
				$property->description_ar = $data->description;
			} else {
				$property->name_en = $data->name;
				$property->description_en = $data->description;
			}
			$property->save();


			PropertyAttribute::where('property_id', $property->id)
				->update([
					'property_id' => $property->id,
					'availability' => $data->availability,
					'guest' => $data->guest,
					'room' => $data->room,
					'bathroom' => $data->bathroom,
					'area' => $data->area,
					'park' => $data->park,
					'rental' => $data->rental,
					'location_name' => $data->location_name,
					'furnished' => $data->furnished,
					'type' => $data->type,
					'lat' => $data->lat,
					'lng' => $data->lng,
				]);

				if ($data['amenities'] != []) {
					$property->amenities()->attach($data->amenities);
				}
	
				if ($data['services'] != []) {
					$property->nearbyServices()->attach($data->services);
				}

			// find media where id == product edit
			$medias = Media::where([
				['mediable_id', $property->id],
				['mediable_type', 'App\Models\Property']
			])->get();

			// if request have images now image delete all old image in folder and db
			if ($data->hasFile('images')) {
				// delete for folder
				foreach ($medias as $media) {
					\File::delete($media->file);
				}
				// delete from db
				DB::table('medias')->where([
					['mediable_id', $property->id],
					['mediable_type', 'App\Models\Property']
				])->delete();

				// upload and insert
				foreach ($data->images as $value) {
					storeMedia($value, 'properties', $property->id, 'App\Models\Property');
				}
			}


			DB::commit();

			return $response;
		} catch (\Exception $exception) {
			DB::rollback();

			$response['success'] = false;
			$response['message'] = $exception->getMessage();
			return $response;
		}
	}


}

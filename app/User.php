<?php

namespace App;

use App\models\Contact;
use App\Models\Favourite;
use App\Models\Notification;
use App\Models\Property;
use App\Models\VerifyUser;
use App\Models\Schedule;
use App\Models\UserToken;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable, HasApiTokens,HasRoles;



    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'last_name', 'email', 'password','mobile','whatsapp_mobile','type','email_verified_at','remember_token','country_code','property_count'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    protected $with = [
        'verifyUser','favourites','schedules'
    ];

    public function scopeActive($query)
    {
        return $query->where('email_verified_at','!=', Null);
    }
    public function scopeNotActive($query)
    {
        return $query->where('email_verified_at', Null);
    }
    
  


    public function verifyUser()
    {
        return $this->hasOne(VerifyUser::class);
    }

    public function userToken()
    {
        return $this->hasOne(UserToken::class);
    }

     /**
    * Verify user mobile number
    * @return true
    */
    public function verify()
    {
        $this->verified = true;

        $this->save();

        $this->verifyUser()->delete();
    }

    /**
     * Get all favourites of properties.
     */
    public function favourites()
    {
        return $this->belongsToMany(Property::class,'favourites')->withPivot('user_id', 'property_id');
    }
    //  /**
    //  * Get all schedules of properties.
    //  */
    // public function schedules()
    // {
    //     return $this->belongsToMany(Property::class,'schedules')->withPivot('user_id', 'property_id');
    // }
    public function schedules()
    {
        return $this->morphMany(Schedule::class, 'schedulable');
    }
    
    public function viewContact()
    {
        return $this->morphMany(Contact::class, 'contacts');
    }
    /**
     * Get all reservess of properties.
     */
    public function property()
    {
        return $this->hasMany(Property::class);
    }
    /**
     * Get all reservess of properties.
     */
    public function reserves()
    {
        return $this->hasMany(Reserve::class,'reserves');
    }

    public function notifications()
    {
        return $this->belongsToMany(Notification::class)->withPivot('read_at');
    }

}

<?php

use App\Http\Controllers\GeneralController;
use App\Models\ActivityType;
use App\Models\Constant;
use App\Models\Conversation;
use App\Models\Property;
use App\Models\Setting;
use App\Models\User;
use App\Repositories\NotificationRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

/**
 * Get list of languages
 */

if (! function_exists('languages')) {
	function languages()
	{
		$languages = App\Models\Language::all();
		return $languages;
	}
}

/**
 * Get list of languages
 */

function notifyByFirebase($title, $body, $tokens, $data = [])
{

    $registrationIDs = $tokens;
    $fcmMsg = array(
        'body' => $body,
        'title' => $title,
        'sound' => "default",
        'color' => "#203E78"
    );

    $fcmFields = array(
        'registration_ids' => $registrationIDs,
        'priority' => 'high',
        'notification' => $fcmMsg,
        'data' => $data
    );
    //dd(env('FIREBASE_API_ACCESS_KEY'));
    // $key = env('FIREBASE_API_ACCESS_KEY');
    $key = 'AAAAsksUj8A:APA91bEzPj7nSkwuHN7Pj2RtV9W2RyC6R65F0rAEAeqfA3XM8Z4pE35JeFuW3sHMD12xzgPMieH2tropQunwEd_MAfGmHEH0Fqn-vpCpLIim4Venl6f6lqCdngEFHJth83dA7kU-6E9-';

    $headers = array(
        'Authorization: key=' . $key ,
        'Content-Type: application/json'
    );



    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmFields));
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
}


 /**
 * related Property
 */
if (! function_exists('relatedProperty')) {
    function relatedProperty($id,$city_id, $activity_type_id)
    {
        return Property::where('id','!=',$id)
        ->whereCity_id($city_id)
        ->where('activity_type_id', $activity_type_id)
        ->take(GeneralController::NUMBER_OF_TAKE)
        ->get();
    }
}
if (! function_exists('upload64')) {
    function upload64($file, $path)
    {
       $baseDir = 'uploads/'.$path;

       $name = sha1(time() . $file->getClientOriginalName());
       $extension = $file->getClientOriginalExtension();
       $fileName = "{$name}.{$extension}";

       $file->move(public_path().'/'.$baseDir, $fileName);

       return "{$baseDir}/{$fileName}";
    }
}
/**
 * Upload
 */
 if (! function_exists('upload')) {
 	function upload($file, $path)
 	{
		$baseDir = 'uploads/'.$path;

		$name = sha1(time() . $file->getClientOriginalName());
		$extension = $file->getClientOriginalExtension();
		$fileName = "{$name}.{$extension}";

		$file->move(public_path().'/'.$baseDir, $fileName);

		return "{$baseDir}/{$fileName}";
 	}
 }
 if (! function_exists('get_property_count')) {
    function get_property_count()
    {
         ## property_count ====== 3 
      $count = Constant::whereId(3)->select('value')->first();
     
        return intval($count->value);
        return intval(2);
    }
} 
/**
 * differenceDays
 */
 if (! function_exists('getDescriptionByLanguages')) {
 	function getDescriptionByLanguages($description)
 	{
       
        if($description == Null){
            return 'description_en';
        }
 	}
 }

 /**
 * differenceDays
 */
 if (! function_exists('differenceDays')) {
 	function differenceDays($from, $to)
 	{
        $to = Carbon::createFromFormat('Y-m-d', $to);
        $from = Carbon::createFromFormat('Y-m-d', $from);
        
        return  $diff_days = $to->diffInDays($from);

 	}
 }
 if (! function_exists('activity_type')) {
    function activity_type($name)
    {
        $activity_type = ActivityType::where('name_en',$name)->first();
        return $activity_type;
    }
} 
 if (! function_exists('verification_type')) {
    function verification_type($type)
    {
      
      if($type == 1){
       return 'register';
      }elseif($type == 2){
       return 'forget';
      }else{
          return 'login';
      }
    }
} 
 #####################################################################
 #####################################################################
 #####################################################################
 /**
 * SEARCH 
 */
 if (! function_exists('furnished_status')) {
 	function furnished_status($type)
 	{
       if($type == 1){
        return 'furnished';
       }elseif($type == 2){
        return 'unfurnished';
       }else{
        return 'semi furnished';
       }
 	}
 }
 if (! function_exists('furnished_status_const')) {
    function furnished_status_const($type)
    {
      if($type == 'furnished'){
       return 1;
      }elseif($type == 'unfurnished'){
       return 2;
    }elseif($type == 'semi furnished'){
       return 3;
      }
    }
}
  if (! function_exists('furnished_status_project')) {
 	function furnished_status_project($type)
 	{
       if($type == 1){
        return 'fully';
       }elseif($type == 2){
        return 'semi';
       }
 	}
 } 
   if (! function_exists('rent_type')) {
 	function rent_type($type)
 	{
       if($type == 1){
        return 'day';
       }elseif($type == 2){
        return 'month';
       }elseif($type == 3){
        return 'year';
       }
 	}
 }
   if (! function_exists('rent_type_const')) {
 	function rent_type_const($type)
 	{
       if($type == 'day'){
        return 1;
       }elseif($type == 'month'){
        return 2;
       }elseif($type == 'year'){
        return 3;
       }
 	}
 } 
 if (! function_exists('rental_frequency')) {
 	function rental_frequency($type)
 	{
       
       if($type == 1){
        return 'daily';
       }elseif($type == 2){
        return 'weekly';
       }elseif($type == 3){
        return 'monthly';
         }elseif($type == 4){
        return 'year';
       }
 	}
 }
 if (! function_exists('rental_frequency_const')) {
    function rental_frequency_const($type)
    {
      
      if($type =='daily'){
       return 1;
      }elseif($type == 'weekly'){
       return 2;
      }elseif($type == 'monthly'){
       return 3;
        }elseif($type == 'year'){
       return 4;
      }
    }
}
  if (! function_exists('delivery')) {
 	function delivery($type)
 	{
       if($type == 1){
        return 'ready to move';
       }elseif($type == 2){
        return '1 years';
       }elseif($type == 3){
        return '2 years';
       }elseif($type == 4){
        return '3 years';
       }elseif($type == 5){
        return '4 years';
       }elseif($type == 6){
        return '5 years';
       }elseif($type == 7){
        return '6 years';
       }elseif($type == 8){
        return '7 years';
       }elseif($type == 9){
        return '8 years';
       }elseif($type == 10){
        return '9 years';
       }else{
        return '10 years';
       }
       
 	}
 }

 if (! function_exists('delivery_count')) {
    function delivery_count($type)
    {
      if($type == 'ready to move'){
       return 1;
      }elseif($type == '1 years'){
       return 2;
      }elseif($type == '2 years'){
       return 3;
      }elseif($type == '3 years'){
       return 4;
      }elseif($type == '4 years'){
       return 5;
      }elseif($type == '5 years'){
       return 6;
      }elseif($type == '6 years'){
       return 7;
      }elseif($type == '7 years'){
       return 8;
      }elseif($type == '8 years'){
       return 9;
      }elseif($type == '9 years'){
       return 10;
    }elseif($type == '10 years'){
        return 11;
      }
      
    }
}

 if (! function_exists('delivery_percentage')) {
    function delivery_percentage($type)
    {
      if($type == 'ready to move'){
       return 100;
      }elseif($type == '1 years'){
       return 95;
      }elseif($type == '2 years'){
       return 90;
      }elseif($type == '3 years'){
       return 80;
      }elseif($type == '4 years'){
       return 70;
      }elseif($type == '5 years'){
       return 60;
      }elseif($type == '6 years'){
       return 50;
      }elseif($type == '7 years'){
       return 40;
      }elseif($type == '8 years'){
       return 30;
      }elseif($type == '9 years'){
       return 20;
      }else{
       return 10;
      }
      
    }
}
##########################################################################
##########################################################################
##########################################################################
##########################################################################
/**
 * Store Data in Medias Table
 */
if (!function_exists('getPropertyCount')) {
    function getPropertyCount()
    {
     $data =  DB::table('constants')->whereKey('property count')->first();
        return $data->value; 
    }
}

/**
 * Store Data in Medias Table
 */
if (!function_exists('storeMedia')) {
    function storeMedia($path,$file, $id ,$model)
    {
        DB::table('medias')->insert(
            [
                'file' => upload($path, $file),
                'mediable_id' => $id,
                'mediable_type' => $model
            ]
        );
    }
}

/**
 * Store Data in Medias Table
 */
if (!function_exists('storeMediaPath')) {
    function storeMediaPath($path, $id, $model)
    {
        DB::table('medias')->insert(
            [
                'file'=> $path,
                'mediable_id' => $id,
                'mediable_type' => $model
            ]
        );
    }
}

if (!function_exists('isRtl')) {
	function isRtl($value) {
		$rtlChar = '/[\x{0590}-\x{083F}]|[\x{08A0}-\x{08FF}]|[\x{FB1D}-\x{FDFF}]|[\x{FE70}-\x{FEFF}]/u';

		$isRtl = preg_match($rtlChar, $value) != 0;

		if($isRtl == true){
			return true;
		}
		return false;

	}
}

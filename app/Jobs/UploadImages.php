<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class UploadImages implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $data;
    public $property_id;
    public function __construct($data)
    {
        $this->data = $data;
        $this->property_id = $property_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    { 
     
        logger($this->data);
        foreach ($this->data as $image) {
            DB::table('medias')->insert(
                [
                    'file' => upload('Properties', $image),
                    'mediable_id' => $this->property_id,
                    'mediable_type' => 'App\Models\Property'
                ]
            );
            //storeMedia($image, 'properties', $this->property_id, 'App\Models\Property');
        }
    }
}

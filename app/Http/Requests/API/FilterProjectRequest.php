<?php

namespace App\Http\Requests\API;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class FilterProjectRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $delivery = ['ready to move','1 years','2 years','3 years','4 years','5 years','6 years','7 years','8 years','9 years','10 years'];
        $finished = ['fully','semi'];
       

        return [
          
            'city_id.*' => 'exists:cities,id',
            'activity_type_id.*' => 'numeric',
            'devolper_id' => 'exists:devolpers,id',
            'price_range_min' => 'numeric|gt:0',
            'price_range_max' => 'numeric|gt:0',
            'delivery.*' => [ Rule::in($delivery)],
            'finished.*' => [Rule::in($finished)],
        ];

    }
}

<?php

namespace App\Http\Requests\API;


use App\Http\Requests\API\FormRequest;
use Illuminate\Validation\Rule;


class StorePropertyRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rental = ['daily', 'weekly', 'monthly', 'yearly'];
        $furnished = ['furnished', 'semi furnished', 'unfurnished'];
        $type = ['buy', 'rent'];

        return [
            'city_id' => 'required|exists:cities,id',
            'payment_id' => 'required|exists:payment_methods,id',
            'activity_type_id' => 'required|exists:activity_types,id',
            'name' => 'required|min:10',
            'description' => 'required|min:10',
            'price' => 'required|gt:0',

            ## Attributes ##
            'availability' => 'date_format:Y-m-d',
            'guest' => 'required|gt:0',
            'room' => 'required|gt:0',
            'bathroom' => 'required|gt:0',
            'area' => 'required|gt:0',
            'park' => 'required|gt:0',

            'rental' => [Rule::in($rental)],
            'furnished' => ['required', Rule::in($furnished)],
            'type' => ['required', Rule::in($type)],

            //'lat' => ['regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
            //'lng' => ['regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/'],
           // 'images' => 'array',
            'images.*' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            
        ];
    }
}

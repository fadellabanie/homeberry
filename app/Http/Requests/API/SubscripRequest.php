<?php

namespace App\Http\Requests\API;

use App\Models\User;
use App\Rules\NoSpaceContaine;
use Illuminate\Foundation\Http\FormRequest;

class SubscripRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        $this->sanitize();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' =>'required|exists:users,id',
            'street' =>'required|min:5|max:200',
            'building' =>'required|min:5',
            'postal_code' =>'required||min:5',
            'city' =>'required|min:4',
            'country' =>'required|min:4',
            'government' =>'required|min:5',
             ];
    }

  
}

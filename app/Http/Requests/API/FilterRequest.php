<?php

namespace App\Http\Requests\API;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class FilterRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         $rental = ['daily', 'weekly', 'monthly', 'yearly'];
         $furnished = ['furnished', 'semi furnished', 'unfurnished'];
         $type = ['buy', 'rent'];

        return [
            'property_type' => ['required', Rule::in($type)],
            'city_id' => 'required|exists:cities,id',

            'price_range_min' => 'numeric|gt:0',
            'price_range_max' => 'numeric|gt:0',
            'area_start' => 'numeric|gt:0',
            'area_end' => 'numeric|gt:0',
           // 'Property_Shape_id' => 'exists:activity_types,id',
          
            'room_counter.*' => 'numeric|gt:0',
            'bathroom_counter.*' => 'numeric|gt:0',

            'guest_number.*' => 'array',
            'guest_number.*' => 'numeric|gt:0',
            'availabilit_start' =>'date_format:Y-m-d',
            'availabilit_end' =>'date_format:Y-m-d',
            'rental_frequency.*' => [ Rule::in($rental)],
            'furnished_status.*' => [Rule::in($furnished)],
        ];

    }
}

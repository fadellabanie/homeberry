<?php

namespace App\Http\Requests\API;

use App\Models\User;
use App\Rules\NoSpaceContaine;
use Illuminate\Foundation\Http\FormRequest;

class PhoneRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'mobile' => ['required', 'numeric', 'min: 7'],
        ];
    }

}

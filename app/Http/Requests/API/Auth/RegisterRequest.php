<?php

namespace App\Http\Requests\API\Auth;

use App\Http\Requests\API\APIRequest;


class RegisterRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3|max:50',
            'last_name' => 'required|min:3|max:50',
            'email' => 'required|email|unique:users',
            'mobile' => ['required','unique:users'],
            'password' => 'required|min:8',

        ];
    }
}

<?php

namespace App\Http\Requests\API\Auth;

use App\Http\Requests\API\APIRequest;
use App\Rules\Phone;
use App\Http\Requests\API\FormRequest;

class ResetPasswordRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password' => 'required|min:8|max:16|confirmed', //|confirmed
        ];
    }
}

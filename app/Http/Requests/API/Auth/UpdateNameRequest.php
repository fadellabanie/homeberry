<?php

namespace App\Http\Requests\API\Auth;

use App\Http\Requests\API\APIRequest;
use App\Http\Requests\API\FormRequest;
use Illuminate\Support\Facades\Auth;

class UpdateNameRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = Auth::user();
        return [
            'name' => 'required|min:3|max:50',
            'last_name' => 'required|min:3|max:50',
            'email' => 'required|email|unique:users,email,' . $user->id,
        ];
    }
}

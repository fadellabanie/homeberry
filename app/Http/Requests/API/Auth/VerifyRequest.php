<?php

namespace App\Http\Requests\API\Auth;

use App\Http\Requests\API\APIRequest;
use App\Http\Requests\API\FormRequest;

class VerifyRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'verification_code' => 'required'
        ];
    }
}

<?php

namespace App\Http\Requests\API;

use Illuminate\Foundation\Http\FormRequest;

class ScheduleViewRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //'property_id' => 'required|exists:properties,id',
            'date' => 'required|date_format:Y-m-d|after:yesterday',
            'name' => 'required|min:3|max:50',
            'email' => 'required|email',
            'mobile' => ['required'],
         
            
        ];
    }
}

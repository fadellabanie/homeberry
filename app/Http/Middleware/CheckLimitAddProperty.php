<?php

namespace App\Http\Middleware;

use App\Traits\ApiResponder;
use Closure;
use Illuminate\Support\Facades\Auth;

class CheckLimitAddProperty  
{
    use ApiResponder;
     /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
      if($user->property_count == 0){
          return $this->errorStatus('You have exceeded the maximum allowed limit');
      }
      return $next($request);
   }
}

<?php

namespace App\Http\Controllers\API\V1\General;

use App\Http\Controllers\Controller;
use App\Models\Project;
use App\Models\Property;
use App\Models\Schedule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ScheduleViewController extends Controller
{

    public function propertyScheduleView(Request $request)
    {
        
        if (auth('api')->check()) {
            $user = auth('api')->user();
           
            Schedule::create([
                'name' => $user->name ?? "",
                'email' => $user->email ?? "",
                'mobile' => $user->mobile,
                'date' => $request->date,
                'schedulable_id' => $request->property_id,
                'schedulable_type' => 'App\Models\Property',
            ]);
      
        }else{
            $validatedData = Validator::make($request->all(),[
                'date' => 'required|date_format:Y-m-d|after:yesterday',
                'name' => 'required|min:3|max:50',
                'email' => 'required|email',
                'mobile' => 'required',
            ]);
            if($validatedData->fails()){
               return $this->errorStatus($validatedData->errors()->first());
            }
           Schedule::create([
                'name' => $request->name,
                'email' => $request->email,
                'mobile' => $request->mobile,
                'date' => $request->date,
                'schedulable_id' => $request->property_id,
                'schedulable_type' => 'App\Models\Property',
            ]);
        }
/*
        $schedules = Schedule::whereUser_id($user->id)
            ->whereSchedulable_id($request->property_id)
            ->whereSchedulable_type('App\Models\Property')
            ->first();

        if (!empty($schedules)) {
            return $this->errorStatus('property is all Ready added');
        } else {
            $schedules = Schedule::create([
                'user_id' => $user->id,
                'date' => $request->date,
               
                'schedulable_id' => $request->property_id,
                'schedulable_type' => 'App\Models\Property',
            ]);
        }
        */
        $property = Property::find($request->property_id);
        if($property){
            $property->increment('clicked', 1);
        }
      
        return $this->successStatus();
    }

    public function projectScheduleView(Request $request)
    {

        if (auth('api')->check()) {
            $user = auth('api')->user();
            Schedule::create([
                'name' => $user->name ?? "",
                'email' => $user->email ?? "",
                'mobile' => $user->mobile,
                'date' => $request->date,
                'schedulable_id' => $request->project_id,
                'schedulable_type' => 'App\Models\Project',
            ]);
      
        }else{
            $validatedData = Validator::make($request->all(),[
                'date' => 'required|date_format:Y-m-d|after:yesterday',
                'name' => 'required|min:3|max:50',
                'email' => 'required|email',
                'mobile' => 'required',
            ]);
            if($validatedData->fails()){
               return $this->errorStatus($validatedData->errors()->first());
            }
           Schedule::create([
                'name' => $request->name,
                'email' => $request->email,
                'mobile' => $request->mobile,
                'date' => $request->date,
                'schedulable_id' => $request->project_id,
                'schedulable_type' => 'App\Models\Project',
            ]);
        }
        $project = Project::find($request->project_id);

        $project->increment('clicked', 1);

        return $this->successStatus();
    }
}

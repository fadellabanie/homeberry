<?php

namespace App\Http\Controllers\API\V1\General;

use App\Http\Controllers\Controller;
use App\Http\Controllers\GeneralController;
use App\Http\Requests\API\StorePropertyRequest;
use App\Http\Resources\Properties\PropertyCollection;
use App\Http\Resources\Properties\PropertyLargeResource;
use App\Models\Property;
use App\Services\PropertyService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PropertyController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth:api','IsActiveToken'])->only(['store', 'clickedProperty', 'update', 'destroy', 'listUserProperties']);
        $this->middleware('CheckLimitAddProperty')->only(['store']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $properties = Property::with('medias', 'propertyAttribute', 'note')->Active()
            ->PrioritySorted()
            ->paginate(GeneralController::PAGINATE_NUMBER_LARGE_PAGE);

        //return $properties;
        return new PropertyCollection($properties);

        // return new PropertyCollection($properties);
        //return $this->respondWithCollection($properties);

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function listUserProperties()
    {

        $properties = Property::with('medias', 'propertyAttribute')
            ->where('user_id', Auth::id())->Active()
            //->PrioritySorted()
            ->orderby('id','desc')
            ->paginate(GeneralController::PAGINATE_NUMBER_LARGE_PAGE);


        return new PropertyCollection($properties);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePropertyRequest $request)
    {
       
        $response = PropertyService::create($request);

        if (!$response['success']) {
            return $this->errorStatus($response['message']);
        }

        $property = Property::latest()->first();

        return $this->respondWithItem(new PropertyLargeResource($property));
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $property = Property::find($id);

        if (!$property) {
            return $this->errorStatus('property not found');
        }

        $property->increment('views', 1);

        return $this->respondWithItem(new PropertyLargeResource($property));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StorePropertyRequest $request, $id)
    {
       

        $property =  Property::whereId($id)->where('user_id', Auth::id())->first();
        if (!$property) {
            return $this->errorStatus('You Not Own Property');
        }
      
        $response = PropertyService::update($request, $property);

        if (!$response['success']) {
            return $this->errorStatus($response);
        }

        $property = Property::latest()->first();

        //return $this->respondWithItem(new PropertyLargeResource($property));
        return $this->successStatus();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $property =  Property::whereId($id)->where('user_id', Auth::id())->first();

        if (!$property) {
            return $this->errorStatus();
        }
        $property->delete();


        return $this->successStatus('Delete property Success');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function clickedProperty($id)
    {

        $property = Property::find($id);
        if (!$property) {
            return $this->errorStatus('property not found');
        }
        $property->increment('clicked', 1);

        return $this->respondWithMessage('clicked success');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function compares(Request $request)
    {

        $properties = Property::Active()
            ->PrioritySorted()
            ->WhereIn('id', $request->property_id)
            ->get();

        if (!$properties) {
            return $this->errorStatus('property not found');
        }

        return new PropertyCollection($properties);
    }
}

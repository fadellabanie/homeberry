<?php

namespace App\Http\Controllers\API\V1\General;

use App\Http\Controllers\Controller;
use App\Http\Controllers\GeneralController;
use App\Http\Resources\Cities\CityCollection;
use App\Http\Resources\Properties\PropertyCollection;
use App\Models\City;
use App\Models\Property;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cities = City::Active()
            ->PrioritySorted()
            ->paginate(GeneralController::PAGINATE_NUMBER_LARGE_PAGE);

        return new CityCollection($cities);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        ## GET ALL PROPERTY BY CITY ID
        $city = City::find($id);
        if (!$city) {
            return $this->errorStatus();
        }

        $properties = Property::with('medias', 'propertyAttribute')
            ->where('city_id', $city->id)
            ->Active()
            ->PrioritySorted()
            ->paginate(GeneralController::PAGINATE_NUMBER_LARGE_PAGE);


        return new PropertyCollection($properties);
        //return $this->respondWithMessage(new CityTinyResource($city));

    }
}

<?php

namespace App\Http\Controllers\API\V1\General;

use App\Http\Controllers\Controller;
use App\Http\Controllers\GeneralController;
use App\Http\Resources\Devolpers\DevolperLargeResource;
use App\Http\Resources\Projects\ProjectCollection;
use App\Http\Resources\Projects\ProjectLargeResource;
use App\Models\Devolper;
use App\Models\Project;


class ProjectController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Project::with('medias')->Active()
            ->PrioritySorted()
            ->paginate(GeneralController::PAGINATE_NUMBER_LARGE_PAGE);

        return new ProjectCollection($projects);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        $project->increment('views', 1);

        if (!$project) {
            return $this->errorNotFound();
        }
        return $this->respondWithItem(new ProjectLargeResource($project));
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showDeveloper($id)
    {

        $devolper = Devolper::find($id);


        if (!$devolper) {
            return $this->errorNotFound();
        }

        return $this->respondWithItem(new DevolperLargeResource($devolper));
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function clickedProject($id)
    {
        $project = Project::find($id);
        if (!$project) {
            return $this->errorStatus('project not found');
        }
        $project->increment('clicked', 1);

        return $this->respondWithMessage('clicked success');
    }
}

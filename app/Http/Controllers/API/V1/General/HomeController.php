<?php

namespace App\Http\Controllers\API\V1\General;

use App\Http\Controllers\Controller;
use App\Http\Controllers\GeneralController;
use App\Http\Resources\Cities\CityHomeCollection;
use App\Http\Resources\ActivityResource;
use App\Http\Resources\Features\AmenitiesResource;
use App\Http\Resources\Features\NearbyResource;
use App\Http\Resources\PaymentMethodResource;
use App\Http\Resources\Projects\ProjectTinyResource;
use App\Http\Resources\Properties\PropertyCollection;
use App\Http\Resources\Properties\PropertyTinyResource;
use App\Models\ActivityType;
use App\Models\City;
use App\Models\Favourite;
use App\Models\Feature;
use App\Models\FurnishedType;
use App\Models\PaymentMethod;
use App\Models\Project;
use App\Models\Property;
use App\Models\PropertyAttribute;
use App\Traits\ApiResponder;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    

    /**
     * get Data of Splash Screen (Home Page).
     *
     * 
     * @return \Illuminate\Http\Response
     */
    public function home()
    {
       
        //------------------------------------------------------------ projects 
        $projects = Project::Active()
            ->Priority()
            ->PrioritySorted()
            ->take(GeneralController::NUMBER_OF_TAKE)
            ->get();
        $data['projects'] = ProjectTinyResource::collection($projects);


        //--------------------------------------------------------------------------- cities
        $cities = City::Priority()->Active()->PrioritySorted()->get();
        $data['cities'] = new CityHomeCollection($cities);
        // --------------------------------------------------------------------------- properties

        $properties = Property::with('medias', 'propertyAttribute')
            ->with(['favourites' => function ($hasMany) {
                $hasMany->where('user_id', 24);
            }])
            ->Active()
            ->Priority()
            ->PrioritySorted()
            ->take(GeneralController::NUMBER_OF_TAKE)
            ->get();

        $data['properties'] = PropertyTinyResource::collection($properties);

        // --------------------------------------------------------------------------- Search attributes

        if (auth('api')->check()) {
            $user = auth('api')->user();
            $data['favourites'] =   Favourite::whereUser_id($user->id)->paginate(GeneralController::PAGINATE_NUMBER_LARGE_PAGE);
        }

        $data['properties_min_price'] = Property::min('price');
        $data['properties_max_price'] = Property::max('price');
        $data['properties_min_area'] = PropertyAttribute::min('area');
        $data['properties_max_area'] = PropertyAttribute::max('area');

       
        return $this->respondWithCollection($data);
    }

    /**
     * get  general Data .
     *
     * 
     * @return \Illuminate\Http\Response
     */
    public function general()
    {
        $data['activities'] = ActivityResource::collection(ActivityType::get());
        $data['amenities'] = AmenitiesResource::collection(Feature::whereType('amenities')->get());
        $data['nearby_services'] = NearbyResource::collection(Feature::whereType('nearby services')->get());

        $data['delivery'] = GeneralController::DELIVERY;
        $data['rental'] = GeneralController::RENTAL;
        $data['furnished_properties'] = FurnishedType::get();
        $data['furnished_project'] = GeneralController::FURNISHED_PROJECT;
        $data['payment_method'] = PaymentMethodResource::collection(PaymentMethod::get());

        $data['properties_min_price'] = Property::min('price');
        $data['properties_max_price'] = Property::max('price');
        $data['properties_min_area'] = PropertyAttribute::min('area');
        $data['properties_max_area'] = PropertyAttribute::max('area');

        return $this->respondWithCollection($data);
    }

    /**
     * get Data of Progerties Quick Search (Home Page).
     *
     * 
     * @return \Illuminate\Http\Response
     */
    public function getProgertiesByType(Request $request)
    {

        $properties = Property::with('medias', 'propertyAttribute')
            ->whereHas('propertyAttribute', function ($query) use ($request) {
                $query->where('type', $request->type);
            })
            ->PrioritySorted()->Active()->paginate(GeneralController::NUMBER_OF_TAKE);
        return new PropertyCollection($properties);
        //return $this->respondWithCollection($properties);

    }
}

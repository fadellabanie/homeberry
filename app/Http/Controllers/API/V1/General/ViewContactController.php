<?php

namespace App\Http\Controllers\API\V1\General;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\ViewContactRequest;
use App\Models\Contact;
use App\Models\Project;
use App\Models\Property;


class ViewContactController extends Controller
{

    #########################################
    ##  ViewContact   OR  ScheduleView
    ##########################################
    public function propertyViewContact(ViewContactRequest $request)
    {
        
        if (auth('api')->check()) {
            $user = auth('api')->user();

        Contact::create([
            'name' => $user->name ?? '',
            'email' => $user->email ?? '',
            'mobile' => $user->mobile,
            'contactable_id' => $request->property_id,
            'contactable_type' => 'App\Models\Property',
        ]);
        }else{
            Contact::create([
                'name' => $request->name,
                'email' => $request->email,
                'mobile' => $request->mobile,
                'contactable_id' => $request->property_id,
                'contactable_type' => 'App\Models\Property',
            ]);
        }
        $property = Property::find($request->property_id);
        if($property){
            $property->increment('clicked', 1);
        }
      
        return $this->successStatus();
    }

    public function projectViewContact(ViewContactRequest $request)
    {
        if (auth('api')->check()) {
            $user = auth('api')->user();
         
            Contact::create([
                'name' => $user->name,
                'email' => $user->email,
                'mobile' => $user->mobile,
                'contactable_id' => $request->project_id,
                'contactable_type' => 'App\Models\Project',
            ]);
     
        }else{
            Contact::create([
                'name' => $request->name,
                'email' => $request->email,
                'mobile' => $request->mobile,
                'contactable_id' => $request->project_id,
                'contactable_type' => 'App\Models\Project',
            ]);
        }
       
        $project = Project::find($request->project_id);

        $project->increment('clicked', 1);

        return $this->successStatus();
    }
}

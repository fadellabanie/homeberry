<?php

namespace App\Http\Controllers\API\V1\General;

use App\Http\Controllers\Controller;
use App\Models\Property;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    public function send(Request $request)
    {
        $title = $request->title;
        $message = $request->message;
        $user_id = $request->user_id;
        $model = $request->model;
        //$type = $this->notificationTypeKey->getTopicType() . 'useridAndroid' . $user_id; ///socket
        $type = 'AllUserAndroid';
        $dataPush = [];
        $device = "";

        $result = $this->send_notification($message, $title, $type, $dataPush, $device);
        $response = [
            'status' => 1,
            'result' => $result
        ];
        return response()->json($response, 200);
    }

 
    public function send_notification($message, $title, $topic, $notification_type, $dataPush = array(), $device = "")
    {


        $url = 'https://fcm.googleapis.com/fcm/send';

        $fields = array(
            "to" => "/topics/$topic",  //userId5     //AllUserAndroid
            "collapse_key" => "type_a",
            "priority" => "high",
            'data' => array(
                "title" => $title,
                "body" => $message,
                "type" => $notification_type,
                "data_order" => $dataPush
            )
        );
        // overwrite on fields
        if ($device == 'ios') {
            $fields = array(
                "to" => "/topics/" . $topic,
                "collapse_key" => "type_a",
                "priority" => "high",
                'notification' => array(
                    "title" => $title,
                    "body" => $message,
                    "type" => $notification_type,
                    "data_order" => $dataPush

                )
            );
        }

        $fields = json_encode($fields);
        $header = array(
            'Authorization: key=AAAApk49wiM:APA91bF0sQA8HazxF1cuyLxgkR-O5bEfXSstDox4fEJEcsXPICbR6zBBfc3nM2brYrZ9m6St2aFCIAy3eIRe37uGlu0d_S2CtbJO--MEeONUbG8LYnyCRstIx-tEqIrgskj07QgWDI-0',
            'Content-Type: application/json'
        );




        return $this->api_parser($url, $fields, $header);
    }

    public function api_parser($url, $fields, $header = array())
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
    }


    public function testNotification(Request $request)
    {
        /*
        firebase
        */

        $ids = $request->ids;
        $title = $request->title;
        $body = $request->body;
        $data = Property::first();
        $tokens = ["d_ja-_ZdTo6TpQSH4a59I2:APA91bH1L_BebbkXq5T6gSfZKuzeLUKFxqyvvUyTY2bi929T4D7NHZvkLxZck8WBcxn9dGJkHwNvk6EL_bQxX4iccG4yez9FSv4HdNu4AbAoXFJdgi9ULxYKuOO9XymM2a7VEGIBugMs"];
        // $tokens = [2];
        $send = notifyByFirebase($title, $body, $tokens, $data);

        // $send = $this-> sendAndroidNotification($device_token, "message from controller", $title, $body);
        info("firebase result: " . $send);

        return response()->json([
            'status' => 1,
            'msg' => 'تم الارسال بنجاح',
            'send' => json_decode($send)
        ]);


        // $user = User::where('id', 2 )->first();

        // $notification_id = $user->notification_id;

        // $title = "Greeting Notification";
        // $message = "Have good day!";
        // $id = $user->id;
        // $type = "basic";

        // $send = send_notification_FCM($notification_id, $title, $message, $id, $type);

        // return response()->json([
        //     'status' => 1,
        //     'msg' => 'تم الارسال بنجاح',
        //     'send' => json_decode($send)
        // ]);


    }


    public function sendAndroidNotification($device_token, $message = null, $title, $body)
    {

        if (!empty($device_token) && $device_token != 'NULL') {

            $device_token = json_decode(json_encode($device_token));

            $FIREBASE_API_KEY = 'AAAA2BLwrBg:APA91bEtLx8BWRH8TWGb6ceIjBIoSDZ74sYhDQ9mf4lefkovwuTb19XFCVJcD84DUeGem4iPXNt4g7oTPWExpWDGXUr6VapNcy5t7Tox6b6nKlaOK6gBYGR-sheST0M-yZfNkPtdVcyG';

            $url = 'https://fcm.googleapis.com/fcm/send';

            $notification = array(
                'title' => $title,
                'body' => $body,
                'sound' => 'default',
                'badge' => '1'
            );

            $fields = array(
                'registration_ids' => $device_token,
                'notification' => $notification,
                'priority' => 'high',
                'data' => $message
            );

            $headers = array(
                'Authorization: key=' . $FIREBASE_API_KEY,
                'Content-Type: application/json'
            );

            // Open connection
            $ch = curl_init();

            // Set the url, number of POST vars, POST data
            curl_setopt($ch, CURLOPT_URL, $url);

            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

            // Execute post
            $result = curl_exec($ch);

            // Close connection
            curl_close($ch);

            return  $result;
        }
    }
}

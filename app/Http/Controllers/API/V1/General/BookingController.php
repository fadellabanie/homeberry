<?php

namespace App\Http\Controllers\API\V1\General;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\StoreReserveRequest;
use App\Http\Resources\Booking\BookingResource;
use App\Http\Resources\Booking\CalaculateResource;
use App\Models\Constant;
use App\Models\Property;
use App\Models\Reserve;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class BookingController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:api','IsActiveToken']);
    }

    public function calaculate(StoreReserveRequest $request)
    {
        $user = Auth::user();

        ## Get (1)cleaning fee and (2)service fee
        $constants = Constant::find([1, 2]);
        $cleaningFee = intval($constants->first()->value);
        $serviceFee = intval($constants->last()->value);
      
        ## 'daily','weekly','monthly','yearly'
        $property = Property::find($request->property_id);

        $diff_days = differenceDays($request->to, $request->from);

        //return $this->calaDay($diff_days);

        $price =  $this->calaReserve($property->price,$property->propertyAttribute->rental,$diff_days);
      
       // return $price;

        $accommodationCost = $price * $diff_days;

        $total =  $accommodationCost + $cleaningFee  + $serviceFee;
      
        $reserve =
            [
                'user_id' => $user->id,
                'property_id' => $request->property_id,
                'from' => $request->from,
                'to' => $request->to,
                'diff_days' => $diff_days,
                'total' => $total,
                'is_work_trip' => $request->is_work_trip,
            ];
          
        return $this->respondCreated(
            new CalaculateResource($reserve, $property,$cleaningFee, $serviceFee, $accommodationCost,$total)
        );
    }

    public function reserve(StoreReserveRequest $request)
    {
        $user = Auth::user();

        ## Get (1)cleaning fee and (2)service fee
        $constants = Constant::find([1, 2]);
        $cleaningFee = intval($constants->first()->value);
        $serviceFee = intval($constants->last()->value);

        $property = Property::find($request->property_id);

        $diff_days = differenceDays($request->to, $request->from);

        $price =  $this->calaReserve($property->price,$property->propertyAttribute->rental,$diff_days);


        $accommodationCost = $price * $diff_days;
        $total =  $accommodationCost + $cleaningFee  + $serviceFee;

        $reserve = Reserve::create([
            'user_id' => $user->id,
            'property_id' => $request->property_id,
            'from' => $request->from,
            'to' => $request->to,
            'diff_days' => $diff_days,
            'total' => $total,
            'is_work_trip' => $request->is_work_trip,
            'status' => true,
        ]);

        $property->increment('clicked', 1);
       

        return $this->respondCreated(new BookingResource($reserve, $cleaningFee, $serviceFee, $accommodationCost));
    }

    public function payment_for_reserve()
    {
        ## Payment 
    }
    
    public function calaReserve($price,$rental,$diff_days)
    {

      ## 'daily','weekly','monthly','yearly'

      switch ($rental) {
        case 'daily':
            return $price;
        break;

        case 'weekly':
            return $price / 7;
        break;

        case 'monthly':
            return $price / 30;
        break;

          case 'yearly':
            return $price / 365;
        break;
      }
    }
}

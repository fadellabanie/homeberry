<?php

namespace App\Http\Controllers\API\V1\General;

use App\Http\Controllers\Controller;
use App\Http\Controllers\GeneralController;
use App\Http\Resources\Packages\PackageCollection;
use App\Models\Package;
use App\Services\Notification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class PackageController extends Controller
{

    protected $notification;

    public function __construct(Notification $notification)
    {
        $this->middleware(['auth:api','IsActiveToken'])->only(['subscrip']);
        $this->notification = $notification;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $packages = Package::whereType('subscription')
            ->take(GeneralController::NUMBER_OF_TAKE_SMALL)
            ->get();
      
        return new PackageCollection($packages);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function subscribe(Request $request)
    {
        // $data = WeAccept::paymentWeAccept($request->all());
        if (!auth('api')->check()) {
            return $this->errorStatus('unauthorization');
        }
        $user = auth('api')->user();
 
        //$user = User::find($user->id);
        $package = Package::find($request->package_id);

        if(!$package){
            return $this->errorStatus('Package Not Found');
        }

        $user->increment('property_count', $package->count);

         /*
        $title = __("Subscribe");
        $msg = __("Now you are subscribe");
        $this->notification->sendMultipleUsers(GeneralController::SEND_NOTIFICATION_FOR_ALL_ANDROID_USERS, $title, $msg);
        */

        return $this->respondWithItem($user);
    }
}

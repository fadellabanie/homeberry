<?php

namespace App\Http\Controllers\API\V1\General;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\NoteRequest;
use App\Http\Resources\NoteResource;
use App\Models\Note;
use App\Models\Property;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NoteController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:api','IsActiveToken'])->only(['store', 'update', 'destroy']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        $property = Property::find($request->property_id);

        if (!$property) {
            return $this->errorStatus('property not found');
        }
        $note = Note::create(array_merge($request->all(), ['user_id' => Auth::id()]));

        return $this->respondCreated(new NoteResource($note));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Note $note)
    {
        if (!$note) {
            return $this->errorStatus();
        }
        return $this->respondWithItem(new NoteResource($note));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(NoteRequest $request, $id)
    {
        $property = Property::find($request->property_id);

        if (!$property) {
            return $this->errorStatus('property not found');
        }
        $note =  Note::whereId($id)->where('user_id', Auth::id())->first();

        if (!$note) {
            return $this->errorStatus();
        }

        $note->update($request->all());

        return $this->successStatus();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $note =  Note::whereId($id)->where('user_id', Auth::id())->first();

        if (!$note) {
            return $this->errorStatus();
        }

        $note->delete();
        return $this->respondWithMessage('Note Has Been Deleted Successfully');
    }
}

<?php

namespace App\Http\Controllers\API\V1\General;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\PaymnetRequest;
use App\Services\WeAccept;

class PaymnetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function paymentsToken(PaymnetRequest $request)
    {

        $paymnet = new WeAccept();

        $data = $paymnet->paymentWeAccept($request);

        return response()->json($data, 200);
    }
}

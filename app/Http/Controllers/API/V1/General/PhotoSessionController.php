<?php

namespace App\Http\Controllers\API\V1\General;

use App\Http\Controllers\Controller;
use App\Http\Controllers\GeneralController;
use App\Http\Requests\API\PhotoSessionRequest;
use App\Http\Resources\Packages\PackageCollection;
use App\Http\Resources\PhotoSessions\PhotoSessionResource;
use App\Models\Package;
use App\Models\PhotoSession;

use Illuminate\Support\Facades\Auth;

class PhotoSessionController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:api','IsActiveToken'])->only(['reserve']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $packages = Package::whereType('photo-sessions')
            ->take(GeneralController::NUMBER_OF_TAKE_SMALL)->get();

        return  new PackageCollection($packages);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function reserve(PhotoSessionRequest $request)
    {
        ## ##
        ## ## Payment 
        ## ##
        $user = Auth::user();
        $photoSession = PhotoSession::create(array_merge($request->all(), ['user_id' => $user->id]));

        return $this->respondCreated(new PhotoSessionResource($photoSession));
    }
}

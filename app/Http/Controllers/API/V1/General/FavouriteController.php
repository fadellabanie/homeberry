<?php

namespace App\Http\Controllers\API\V1\General;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\FavouriteRequest;
use App\Http\Resources\Favourites\FavouriteCollection;
use App\Http\Resources\Favourites\FavouriteResource;
use App\Http\Resources\Properties\PropertyCollection;
use App\Models\Favourite;
use App\Models\Property;
use App\Models\PropertyAttribute;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class FavouriteController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:api','IsActiveToken']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new PropertyCollection(
            Auth::user()->favourites
        );
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FavouriteRequest $request)
    {
        $user = Auth::user();

        $favourites = $user->favourites()->pluck('property_id')->toArray();

        if (in_array($request->property_id, $favourites)) {
            return $this->errorStatus('property is all Ready added');
           
        } else {
            $favourite = Favourite::create(array_merge($request->all(), ['user_id' => $user->id,]));
        }
        return $this->respondWithItem(new FavouriteResource($favourite));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($property_id)
    {

        $favourite = Favourite::where('user_id', Auth::user()->id)->where('property_id', $property_id)->first();

        if (!$favourite) {
            return $this->errorStatus('No Item Favourite');
        }

        return $this->respondWithItem(new FavouriteResource($favourite));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $Favourite
     * @return \Illuminate\Http\Response
     */
    public function destroy($property_id)
    {
        $user = User::find(Auth::id());

        $property = Property::find($property_id);
        if(!$property){
             return $this->respondWithMessage("Property ID Not Found");
        }

       // $user->favourites()->wherePivot('property_id', $property_id)->delete();
        DB::table('favourites')
        ->where(['property_id'=> $property->id,'user_id'=>$user->id])
        ->delete();

        return $this->respondWithMessage("Favourite Item Deleted");
    }
}

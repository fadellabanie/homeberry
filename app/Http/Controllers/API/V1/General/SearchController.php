<?php

namespace App\Http\Controllers\API\V1\General;

use App\Http\Controllers\Controller;
use App\Http\Controllers\GeneralController;
use App\Http\Requests\API\FilterProjectRequest;
use App\Http\Requests\API\FilterRequest;
use App\Http\Resources\Devolpers\DevolperCollection;
use App\Http\Resources\Projects\ProjectCollection;
use App\Http\Resources\Properties\PropertyCollection;
use App\Models\Devolper;
use App\Models\Project;
use App\Models\Property;
use App\Models\Reserve;
use Illuminate\Http\Request;

class SearchController extends Controller
{

    // Edit input varb ,change area start ,end (min,max) and sorted by priority

    public function filterProperties(FilterRequest $request)
    {
        $properties = Property::Active()->PrioritySorted();
        
    

        $reserves = Reserve::select('property_id')->get();

        foreach ($reserves as $tt) {
            $property_id = strval($tt->property_id);

            $properties->whereNotIn('id', [$property_id])->get();
        }


        if ($request->has('property_type')) {
            $properties->whereHas('propertyAttribute', function ($query) use ($request) {
                $query->where('type', $request['property_type']);
            });
        }
        if ($request->has('city_id')) {
            $properties->where('city_id', $request['city_id']);
        }

        //--------------------------
        ##---------- optional value
        //--------------------------

        if ($request->has('price_range_min') && $request->has('price_range_max')) {
            $properties->whereBetween('price', [$request['price_range_min'], $request['price_range_max']]);
        }
        if ($request->has('area_start') && $request->has('area_end')) {
            $properties->whereHas('propertyAttribute', function ($query) use ($request) {
                $query->whereBetween('area', [$request['area_start'], $request['area_end']]);
            });
        }
        if ($request->has('activity_type_id')) {
            $properties->whereIn('activity_type_id', $request['activity_type_id']);
        }

        //--------------------------
        ##---------- optional in Property type  "Apartment buy" or "Apartment Rent"
        //--------------------------
        //if (In_array($request->property_type, ["rent", "buy"])) {

        if ($request->has('room_counter')) {
            $properties->whereHas('propertyAttribute', function ($query) use ($request) {
                $query->whereIn('room', $request['room_counter']);
            });
        }

        if ($request->has('bathroom_counter')) {
            $properties->whereHas('propertyAttribute', function ($query) use ($request) {
                $query->whereIn('bathroom', $request['bathroom_counter']);
            });
        }
        // }
        //--------------------------
        ##---------- optional in "Apartment Rent" only
        //--------------------------
        if ($request->property_type == "rent") {
            if ($request->has('guest_number')) {
                $properties->whereHas('propertyAttribute', function ($query) use ($request) {
                    $query->whereIn('guest', $request['guest_number']);
                });
            }
            if ($request->has('availabilit_start') && $request->has('availabilit_end')) {
                $properties->whereHas('propertyAttribute', function ($query) use ($request) {
                    $start = date($request['availabilit_start']);
                    $end = date($request['availabilit_end']);
                    $query->whereBetween('availability', [$start, $end]);
                });
            }
            if ($request->has('rental_frequency')) {


                $properties->whereHas('propertyAttribute', function ($query) use ($request) {
                    $query->whereIn('rental', $request->rental_frequency);
                });
            }
            if ($request->has('furnished_status')) {

                //$furnished_status = furnished_status($request->furnished_status);

                $properties->whereHas('propertyAttribute', function ($query) use ($request) {
                    $query->whereIn('furnished', $request->furnished_status);
                });
            }
        }


        return  new PropertyCollection(
            $properties->paginate(GeneralController::PAGINATE_NUMBER_LARGE_PAGE)
        );
    }


    public function filterProjects(FilterProjectRequest $request)
    {
        // $price_range_min = intval($request->price_range_min);
        // $price_range_max = intval($request->price_range_max);

        $projects = Project::active()->PrioritySorted();

        if ($request->has('devolper_id')) {
            $projects->where('devolper_id', $request->devolper_id);
        }
        if ($request->has('activity_type_id')) {
            $projects->whereHas('activitytypes', function ($query) use ($request) {
                $query->whereIn('activity_type_id', $request->activity_type_id);
            });
        }
        if ($request->has('city_id')) {
            $projects->whereIn('city_id', $request->city_id);
        }
        if ($request->has('finished')) {
            $projects->whereIn('finished', $request->finished);
        }
        if ($request->has('delivery')) {
            $projects->whereIn('delivery', $request->delivery);
        }
        // if ($request->has('price_range_min') && $request->has('price_range_max')) {
        //     $projects->whereBetween('price', [$price_range_min, $price_range_max]);
        // }

        return  new ProjectCollection(
            $projects->paginate(GeneralController::PAGINATE_NUMBER_LARGE_PAGE)
        );
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function searchDevelopers(Request $request)
    {
        $devolpers = Devolper::with('projects')
            ->where('name', 'like', $request->name . '%')
            ->orderby('name', 'DESC')
            ->paginate(GeneralController::PAGINATE_NUMBER_DEVELOPER_PAGE);

        return  new DevolperCollection($devolpers);
    }
}

<?php

namespace App\Http\Controllers\API\V1\User;

use App\Models\UserToken;

use App\Http\Requests\API\UserTokens\StoreUserTokenRequest;
use App\Http\Controllers\Controller;
use App\Traits\ApiResponder;
use Illuminate\Support\Facades\Auth;

class UserTokenController extends Controller
{
 

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\API\UserTokens\StoreUserTokenRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserTokenRequest $request)
    {
        UserToken::updateOrCreate([
            'device_id' => $request->device_id
        ],
        [
            'user_id' => Auth::id(),
            'token' => $request->token,
            'device_id' => $request->device_id,
            'device_type' => $request->device_type
        ]);

        return $this->respondWithMessage('Token created successfully!');
    }
}

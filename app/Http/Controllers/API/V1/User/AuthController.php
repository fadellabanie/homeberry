<?php

namespace App\Http\Controllers\API\V1\User;

use Carbon\Carbon;

use App\Http\Resources\UserResource;
use App\Services\Notification;
use App\Http\Controllers\Controller;
use App\Http\Controllers\GeneralController;
use App\Http\Requests\API\Auth\RegisterRequest;
use App\Http\Requests\API\Auth\LoginRequest;
use App\Http\Requests\API\Auth\VerifyRequest;
use App\Http\Requests\API\Auth\ForgetPasswordRequest;
use App\Http\Requests\API\Auth\ResetPasswordRequest;
use App\Models\UserToken;
use App\Models\VerifyUser;
use App\Traits\ApiResponder;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AuthController extends Controller
{

    protected $notification;

    /**
     * Create new controller instance
     *
     * @param Notification $notification
     */
    public function __construct(Notification $notification)
    {
        $this->notification = $notification;
    }

    /**
     * Register new user
     * @param  RegisterRequest $request
     * @return mixed
     */
    public function register(RegisterRequest $request)
    {
        //add lastname
        $user = User::create([
            'name' => $request->name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'mobile' => $request->mobile,
            'password' => bcrypt($request->password),
            'property_count' =>  get_property_count(),
            'country_code' => $request->country_code,

        ]);

        //$verificationCode = mt_rand(1000, 9999);

        VerifyUser::create([
            'user_id' => $user->id,
            //'verification_code' => $verificationCode,
            'verification_code' => 44444,
            'verification_expiry_days' => 1,
            'type' => 1,
            'verification_updated_at' => Carbon::now(),
        ]);


        //update user token
        if ($request->has('device_id') and $request->device_id != "") {
            $userToken = UserToken::where('device_id', $request->device_id)->first();

            if (!empty($userToken)) {
                $userToken->update([
                    'user_id' => $user->id
                ]);

                //send notification
                /*
                    $title = "كود التفعيل";
                    $msg = "كود التفعيل الخاص بك هو {$verificationCode}";
                    $this->notification->send($userToken->token, $title, $msg);
                */
            }
        }

        return $this->successStatus('Verify your request');
    }

    /**
     * Login
     * @param  LoginRequest $request
     * @return mixed
     */
    public function login(LoginRequest $request)
    {

        $message = 'Your account not verified';

        if (!Auth::attempt($request->only('email', 'password'))) {

            return $this->errorStatus(__('Unauthorized'));
        }

        $user = Auth::user();

        if ($user->type == 'admin') {
            return $this->errorStatus('Your account is admin oop...');
        }

        if (!$user->email_verified_at) {

            return $this->respond([
                'status' => 0,
                'verified' => false,
                'message' =>  $message,
            ]);
            return $this->errorStatus('Your account not verified');
        }
        $token = $user->createToken('Token-Login')->accessToken;

        $user->update(['remember_token' =>$token]);

        return $this->respond([
            'status' => 1,
            'message' => 'Success Request',
            'is_user_found' => true,
            'data' => new UserResource($user),
        ]);
        // return $this->respondWithItem(new UserResource($user));
    }

    /**
     * Verify user mobile number
     * @param  VerifyRequest $request
     * @return mixed
     */
    public function verify(VerifyRequest $request)
    {

        $user = $request->user();

        if ($user->verified) {
            return $this->errorStatus('Your account already verified');
        }

        //check if user has verification code
        $verification_code = $user->verifyUser()->where('type', 1)->first();

        if (empty($verification_code)) {
            return $this->errorStatus('Verification code is wrong');
        }

        if ($user->verifyUser->verification_code != $request->verification_code and $user->verifyUser->type == 1) {
            return $this->errorStatus('Verification code is wrong');
        }

        $verification_updated_at = Carbon::parse($user->verifyUser->verification_updated_at)->addDays($user->verifyUser->verification_expiry_days);

        if ($verification_updated_at->lessThan(Carbon::now())) {
            return $this->errorStatus('Verification code is expired');
        }

        $user->verify();
        $token = $user->createToken('Token-Login')->accessToken;

         $user->update(['remember_token' =>$token]);

        return $this->respondWithItem(new UserResource($user));
    }

    /**
     * Verify user mobile number
     * @param  VerifyRequest $request
     * @return mixed
     */
    public function verifyCode(VerifyRequest $request, $type, $mobile)
    {

        $user = User::where('mobile', $mobile)->first();

        if (empty($user)) {
            return $this->errorStatus('No user registered with this mobile number');
        }
        $typeString = verification_type($type);

        //check if user has verification code
        $verification_code = $user->verifyUser()->where('type', $typeString)->first();

        if (empty($verification_code)) {
            return $this->errorStatus('Verification code is wrong');
        }

        if ($user->verifyUser->verification_code != $request->verification_code) {
            return $this->errorStatus('Verification code is wrong');
        }

        $verification_updated_at = Carbon::parse($user->verifyUser->verification_updated_at)->addDays($user->verifyUser->verification_expiry_days);

        if ($verification_updated_at->lessThan(Carbon::now())) {
            return $this->errorStatus('Verification code is expired');
        }

        $user->verifyUser()->delete();

        $token = $user->createToken('Token-Login')->accessToken;
        $user->update(['remember_token' =>$token]);
         //DB::table('users')->whereId($user->id)->update(['remember_token' =>$token]);
        
        if ($type == 1) {
            $user->update(['email_verified_at' => Carbon::now()]);
        }
        if ($type == 3) {
            Auth::login($user);
            $user->update(['email_verified_at' => Carbon::now()]);
            // return $this->respondWithItem(new UserResource($user));

        }
       

        return $this->respond([
            'status' => 1,
            'message' => 'Success Request',
            'is_user_found' => true,
            'data' => new UserResource($user),
        ]);
        // return $this->respondUser(new UserResource($user));
    }

    /**
     * Resend verification code
     * @param  Request $request
     * @return mixed
     */
    public function resend(Request $request, $type, $mobile)
    {

        //send verification code sms
        $user = User::where('mobile', $mobile)->first();

        if (empty($user)) {
            return $this->errorStatus('No user registered with this mobile number');
        }

        $this->sendVerificationCode($user, $type);

        return $this->successStatus('Verification code sent successfully');
    }

    /**
     *   Forget password
     * @param  ForgetPasswordRequest $request
     * @return mixed
     */
    public function forgetPassword(ForgetPasswordRequest $request)
    {
        $user = User::where('mobile', $request->mobile)->first();

        if (empty($user)) {
            return $this->errorStatus('No user registered with this mobile number');
        }
        $this->sendVerificationCode($user, GeneralController::TYPE_VERFITY_FORGET);

        return $this->successStatus('Verification code sent to your mobile');
    }

    /**
     *   Reset password
     * @param  ResetPasswordRequest $request
     * @param  $mobile
     * @return mixed
     */
    public function resetPassword(ResetPasswordRequest $request, $mobile)
    {
        $user = User::where('mobile', $mobile)->first();

        if (empty($user)) {
            return $this->errorStatus('No user registered with this mobile number');
        }

        $user->update([
            'password' => bcrypt($request->password)
        ]);
        $token = $user->createToken('Token-Login')->accessToken;

        $user->update(['remember_token' =>$token]);
        return $this->successStatus(new UserResource($user));

        //return $this->successStatus('Password has been changed successfully');

    }

    /**
     * Logout
     * @return mixed
     */
    public function logout()
    {
    
        //auth()->user()->last_seen = Carbon::now();
        //auth()->user()->save();
         Auth::user()->token()->revoke();

        return $this->respondWithMessage('User successfully logout');
    }

    /**
     *   Send verification code
     * @param User $user
     */
    protected function sendVerificationCode(User $user, $type)
    {
        $type = verification_type($type);

        return $user->verifyUser()->updateOrCreate(
            [
                'user_id' => $user->id,
                'type' => $type,
            ],
            [
                'verification_code' => 44444,
                'type' => $type,
                'verification_expiry_days' => 1,
                'verification_updated_at' => Carbon::now(),
            ]
        );
    }
}

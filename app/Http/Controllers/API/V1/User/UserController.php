<?php

namespace App\Http\Controllers\API\V1\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\PhoneRequest;
use App\Http\Resources\UserResource;
use App\Traits\ApiResponder;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;


class UserController extends Controller
{
 

    public function __construct()
    {
        $this->middleware('auth:api')->only(['index','update']);
    }
    /**
     * Display a User Profile.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!(Auth::user())) return $this->errorUnauthorized();

        $user = new UserResource(Auth::user());
        return $this->respondWithItem($user);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * See Another  User Profile.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        if(!$user){
            return  $this->errorNotFound();
        }
        return $this->respondWithItem(new UserResource($user));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update( $request,$id)
    {
        $user = User::find($id);

        if($user->id == null){
            $user = Auth::user();
        }

        $user->update(array_merge($request->all(),['password' => Hash::make($request->password)]));

        return $this->respondWithItem(new UserResource($user),'User Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }


    public function checkUser(PhoneRequest $request)
    {
        $is_user_found= false;
        $message= 'No user registered with this mobile number';

        $user = User::where('mobile', $request->mobile)->first();

        if (empty($user)) {

            return $this->respond([
                'status' => 0,
                 'message' =>  $message,
                'is_user_found' => $is_user_found,
               
            ]);
        }
        return $this->respond([
            'status' => 1,
            'message' =>'Success Request',
            'is_user_found' => true,
            'data' => new UserResource($user),
        ]);
    
    }


    public function checkVerify(PhoneRequest $request)
    {
        $user = User::where('mobile', $request->mobile)->first();

        if (empty($user)) {
            return $this->errorStatus('No user registered with this mobile number');
        }

        if ($user->hasVerifiedEmail() == "true") {
            return $this->successStatus('this mobile number is verified');
        }else{
            return $this->errorStatus('Not verified with this mobile number');
        }

    }

}

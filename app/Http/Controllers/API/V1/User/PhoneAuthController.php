<?php

namespace App\Http\Controllers\API\V1\User;

use App\Http\Resources\UserResource;
use App\Services\Notification;
use App\Http\Controllers\Controller;
use App\Http\Controllers\GeneralController;
use App\Http\Requests\API\Auth\RegisterRequest;
use App\Http\Requests\API\Auth\LoginRequest;
use App\Http\Requests\API\Auth\UpdateNameRequest;
use App\Http\Requests\API\PhoneRequest;
use App\Models\UserToken;
use App\Traits\ApiResponder;
use App\User;
use Carbon\Carbon;

class PhoneAuthController extends Controller
{

  
    protected $notification;

    /**
     * Create new controller instance
     *
     * @param Notification $notification
     */
    public function __construct(Notification $notification)
    {
        $this->middleware('auth:api')->only('updateName');
        $this->notification = $notification;
    }

    /**
     * Register new user
     * @param  RegisterRequest $request
     * @return mixed
     */
    public function register(PhoneRequest $request)
    {

        if (User::where('mobile', $request->mobile)->first()) {
            return $this->errorStatus('mobile exist before');
        }

        $user = User::create([
            'name' => "",
            'mobile' => $request->mobile,
            'country_code' => $request->country_code,
            'property_count' =>  get_property_count(),
        ]);


        //$verificationCode = mt_rand(1000, 9999);

        $this->sendVerificationCode($user, GeneralController::TYPE_VERIFY_LOGIN);
        //send sms

        //update user token
        if ($request->has('device_id') and $request->device_id != "") {
            $userToken = UserToken::where('device_id', $request->device_id)->first();

            if (!empty($userToken)) {
                $userToken->update([
                    'user_id' => $user->id
                ]);

                //send notification
                /*
                    $title = "كود التفعيل";
                    $msg = "كود التفعيل الخاص بك هو {$verificationCode}";
                    $this->notification->send($userToken->token, $title, $msg);
                */
            }
        }
        return $this->successStatus('Check your Phone SMS');
    }

    public function updateName(UpdateNameRequest $request, $mobile)
    {
        // $request->validate([
        //     'name' => 'required|min:2|max:50',
        // ]);

        $user = User::where('mobile', $mobile)->first();

        if (empty($user)) {
            return $this->errorStatus('No user registered with this mobile number');
        }
        if (!$user->hasVerifiedEmail()) {
            return $this->errorStatus('you must verify your Account first');
        }

        //add lastname
        $user->update([
            'name' => $request->name,
            'email' => $request->email,
            'last_name' => $request->last_name,


        ]);

        return $this->respondWithItem(new UserResource($user));
    }




    /**
     * Login
     * @param  LoginRequest $request
     * @return mixed
     */
    public function login(phoneRequest $request)
    {

        $user = User::where('mobile', $request->mobile)->first();

        if (empty($user)) {
            return $this->errorStatus('No user registered with this mobile number');
        }
        if (!$user->hasVerifiedEmail()) {
            return $this->errorStatus('you must verify your acc first');
        }

        //$verificationCode = mt_rand(1000, 9999);

        $this->sendVerificationCode($user, GeneralController::TYPE_VERIFY_LOGIN);
        //send sms

        return $this->successStatus("success send code");
    }


    /**
     *   Send verification code
     * @param User $user
     */
    protected function sendVerificationCode(User $user, $type)
    {
        return $user->verifyUser()->updateOrCreate(
            [
                'user_id' => $user->id,
                'type' => intval($type),
            ],
            [
                'verification_code' => 44444,
                'type' => intval($type),
                'verification_expiry_days' => 1,
                'verification_updated_at' => Carbon::now(),
            ]
        );
    }
}

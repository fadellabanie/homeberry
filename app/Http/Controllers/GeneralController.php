<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GeneralController extends Controller
{
    const TYPE_USER = 1;
    const TYPE_ADMIN = 2;
    const TYPE_PROVIDER = 3;
    const TYPE_VERIFY_REGISTER = 1;
    const TYPE_VERFITY_FORGET = 2;
    const TYPE_VERIFY_LOGIN = 3;
    
    const EXPIRE_AT = 60;

    const PAGINATE_NUMBER_LARGE_PAGE = 8;
    const PAGINATE_NUMBER_LARGE_CARD = 18;
   
    const PAGINATE_NUMBER_DEVELOPER_PAGE = 16;
    const PAGINATE_NUMBER_SMALL_PAGE = 6;
    const NUMBER_OF_TAKE = 8;
    const NUMBER_OF_TAKE_SMALL = 5;

    const NUMBER_OF_PARTNER = 8;
    const NUMBER_OF_OFFER_PRODUCTS = 8;


    ######################################## Data Const #####################
    
    const DELIVERY = [
        [
            'id' => 1,
            'name_ar' => 'جاهز للتسليم',
            'name_en' => 'ready to move',
        ],[
            'id' => 2,
            'name_ar' => 'سنة',
            'name_en' => '1 years',
        ],[
            'id' => 3,
            'name_ar' => 'سنتين',
            'name_en' => '2 years',
        ],[
            'id' => 4,
            'name_ar' => '3 سنوات',
            'name_en' => '3 years',
        ],[
            'id' => 5,
            'name_ar' => '4 سنوات',
            'name_en' => '4 years',
        ],[
            'id' => 6,
            'name_ar' => '5 سنوات',
            'name_en' => '5 years',
        ],[
            'id' => 7,
            'name_ar' => '6 سنوات',
            'name_en' => '6 years',
        ],[
            'id' => 8,
            'name_ar' => '7 سنوات',
            'name_en' => '7 years',
        ],[
            'id' => 9,
            'name_ar' => '8 سنوات',
            'name_en' => '8 years',
        ],[
            'id' => 10,
            'name_ar' => '9 سنوات',
            'name_en' => '9 years',
        ],[
            'id' => 11,
            'name_ar' => '10 سنوات',
            'name_en' => '10 years',
        ],
        
    ];   
    const RENTAL  = [
        [
            'id' => 1,
            'name_ar' => 'يومي',
            'name_en' => 'Daily',
        ], [
            'id' => 2,
            'name_ar' => 'اسبوعي',
            'name_en' => 'Weekly',
        ], [
            'id' => 3,
            'name_ar' => 'شهري',
            'name_en' => 'monthly',
        ], [
            'id' => 4,
            'name_ar' => 'سنوي',
            'name_en' => 'year',
        ],
       
    ];
       const FURNISHED_PROPERTIES = [
        'finished' => 1,
        'semi furnished' => 2,
        'unfurnished' => 3,
      
    ]; 
      const FURNISHED_PROJECT= [
          [
          'id' => 1,
          'name_ar' => 'مجهز',
          'name_en' => 'fully', 
        ],
        [ 
          'id' => 2,
          'name_ar' => 'نصف مجهز',
          'name_en' => 'semi',
        ],
      
    ];
  

    ######################################## Notification #####################
    const SEND_NOTIFICATION_FOR_ALL_ANDROID_USERS = "Android";
    const SEND_NOTIFICATION_FOR_ALL_IOS_USERS = "Ios";

    const SEND_NOTIFICATION_FOR_USER = "/topics/userId";
   

    ######################################## PayPal #####################

    const PAYMENT_API_KAY = 'ZXlKMGVYQWlPaUpLVjFRaUxDSmhiR2NpT2lKSVV6VXhNaUo5LmV5SnVZVzFsSWpvaWFXNXBkR2xoYkNJc0ltTnNZWE56SWpvaVRXVnlZMmhoYm5RaUxDSndjbTltYVd4bFgzQnJJam95TnpNMU5uMC5Fd2w3Uk1uV3hmaWpoRERMamkwRlFManQ3ZWpjaU1wbElsdW41X1VwR1hJMWtzWWEzNm1xUktpbE9mM2xfQUtUdWZjUjNydU84dV9HN2pLM0lNQXF5QQ==';
    const PAYMENT_BASE_URL =  "https://accept.paymobsolutions.com/api/acceptance/iframes/";
    const PAYMENT_MERCHANT_ID = 27356;
    const PAYMENT_IFRAME_KAY = 80591;
    const PAYMENT_INTEGRATION_CARD_ID = 60745;
    
}

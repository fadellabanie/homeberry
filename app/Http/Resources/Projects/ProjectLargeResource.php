<?php

namespace App\Http\Resources\Projects;

use App\Http\Controllers\GeneralController;
use App\Http\Resources\ActivityResource;
use App\Http\Resources\Medias\MediaResource;
use App\Models\Project;
use Illuminate\Http\Resources\Json\JsonResource;

class ProjectLargeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $images = [];
        foreach ($this->medias->map->file as $file) {
            $images[] = asset($file);
        }

        $related = Project::where('id','!=',$this->id)
        ->whereCity_id($this->city_id)
        ->Where('devolper_id',$this->devolper_id)
        ->where('city_id',$this->city_id)->take(GeneralController::NUMBER_OF_TAKE)->get();

        $finished = furnished_status_project($this->finished);
        $delivery = delivery_count($this->delivery);
       
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description'=> $this->description,
            'delivery'=> $delivery,
            //'finished'=> $finished,
            'link'=> $this->link,
            'lat'=> $this->lat,
            'lng'=> $this->lng,
            'youtube'=> $this->youtube,
            'virtual_tour_url'=> $this->virtual_tour_url,
            'views'=> $this->views,
            'clicked'=> $this->clicked,
            'is_constructed'=> $this->is_constructed,
            'location_name'=> $this->location,
            'city' => $this->city->name,
            'payment_mathod' => $this->paymentMathod->id,
            'activity_types' => $this->activityTypes->pluck('id')->toArray(),
            'furnished_types' => $this->furnishedtypes->pluck('id')->toArray(),
            'details' => $this->details,
            'images' =>  $images ?? "",

            'owner' => [
                'id' => $this->devolper->id,
                'name' => $this->devolper->name,
                'mobile' => $this->devolper->mobile,
                'email' => $this->devolper->email,
                'whatsapp_mobile' => $this->devolper->whatsapp_mobile,
            ],

            'similar project' =>  ProjectTinyResource::collection($related),
        ];
    }
}

<?php

namespace App\Http\Resources\Projects;

use App\Http\Resources\ActivityResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ProjectTinyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $image = "";
      
        if (count($this->medias) > 0) {
            $image = asset($this->medias->first()->file);
        }

        return [
            'id' => $this->id,
            'name' => $this->name,
            //'price' => $this->price,
            'is_featured' => $this->priority == 1 ? true : false,
            'city' => $this->city->name,
            'devolper' => $this->devolper->name,
            'activity_types' => $this->activityTypes->pluck('id')->toArray(),
            'image' => $image,
        ];
    }
}

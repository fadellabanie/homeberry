<?php

namespace App\Http\Resources\Projects;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ProjectCollection extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public $collects = ProjectTinyResource::class;

    public function toArray($request)
    {
        return [
            'status' => 1,
            'massage' => 'Success Request',
            'data' =>  parent::toArray($request),
        ];
    }
}

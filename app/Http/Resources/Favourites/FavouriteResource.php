<?php

namespace App\Http\Resources\Favourites;

use App\Http\Resources\Properties\PropertyTinyResource;
use Illuminate\Http\Resources\Json\JsonResource;

class FavouriteResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'user' => $this->user->name,
            'property' => new PropertyTinyResource($this->property),
        ];
    }
}

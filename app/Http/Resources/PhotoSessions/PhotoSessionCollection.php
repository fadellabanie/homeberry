<?php

namespace App\Http\Resources\PhotoSessions;

use Illuminate\Http\Resources\Json\ResourceCollection;

class PhotoSessionCollection extends ResourceCollection
{
    public $collects = PhotoSessionResource::class;

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}

<?php

namespace App\Http\Resources\PhotoSessions;

use Illuminate\Http\Resources\Json\JsonResource;

class PhotoSessionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user'=> $this->user->name,
            'package_id' => $this->package->id,
            'package' => $this->package->name,
            'location_name'=> $this->location_name,
            'date'=> $this->date,
            'time'=> $this->time,
            'lat'=> $this->lat,
            'lng'=> $this->lng,
        ];
    }
}

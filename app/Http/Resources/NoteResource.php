<?php

namespace App\Http\Resources;

use App\Http\Resources\Properties\PropertyTinyResource;
use Illuminate\Http\Resources\Json\JsonResource;

class NoteResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'note' => $this->note,
            'user'=> $this->user->name,
            'property'=> new PropertyTinyResource($this->property),
        ];
    }
}

<?php

namespace App\Http\Resources\Features;

use Illuminate\Http\Resources\Json\JsonResource;

class NearbyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if ($this->type == "nearby services") {
            return [
                'id' => $this->id,
                'name_ar' => $this->name_ar,
                'name_en' => $this->name_en,
            ];
        }
    }
}

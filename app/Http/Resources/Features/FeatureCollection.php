<?php

namespace App\Http\Resources\Features;

use Illuminate\Http\Resources\Json\ResourceCollection;

class FeatureCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    public $collects = FeatureResource::class;

    public function toArray($request)
    {
        return ['status' => 1,
                'massage' => 'Success Request',
                'data' =>  parent::toArray($request),
        ];
    }
};

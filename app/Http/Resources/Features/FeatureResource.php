<?php

namespace App\Http\Resources\Features;

use Illuminate\Http\Resources\Json\JsonResource;

class FeatureResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $features = [];
        $amenities = [];
        $nearby = [];
        if($this->type == "amenities"){
            $amenities[] =[
                'id' => $this->id,
                'name_ar' => $this->name_ar,
                'name_en' => $this->name_en,
                'icon' => asset($this->icon) ?? "",
            ] ;

        }else if ($this->type == "nearby services") {
            $nearby[] =[
                'id' => $this->id,
                'name_ar' => $this->name_ar,
                'name_en' => $this->name_en,
            ] ;
        }
        $features = [
            
            'amenities' => $amenities,
            'nearby services' => $nearby,
        ];

        return $features;

    }
}

<?php

namespace App\Http\Resources\Packages;

use Illuminate\Http\Resources\Json\JsonResource;

class PackageTinyResource extends JsonResource
{
     
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        
        if($this->count > 0 )
           {
            return [
                'id' => $this->id,
                'name' => $this->name,
                'description'=> $this->description,
                'price'=> $this->price,
                'count'=> $this->count,
                
            ];
           }else{
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description'=> $this->description,
            'price'=> $this->price,
        ];
     }
    }
}

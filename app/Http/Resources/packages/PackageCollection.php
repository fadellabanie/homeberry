<?php

namespace App\Http\Resources\Packages;

use Illuminate\Http\Resources\Json\ResourceCollection;

class PackageCollection extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public $collects = PackageTinyResource::class;

    public function toArray($request)
    {
        return [
            'status' => 1,
            'massage' => 'Success Request',
            'data' =>  parent::toArray($request),
        ];
    }
}

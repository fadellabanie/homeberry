<?php

namespace App\Http\Resources\Properties;

use Illuminate\Http\Resources\Json\ResourceCollection;

class PropertyLargeCollection extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public $collects = PropertyLargeResource::class;

    public function toArray($request)
    {
        return parent::toArray($request);
    }
}

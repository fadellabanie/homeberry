<?php

namespace App\Http\Resources\Properties;

use Illuminate\Http\Resources\Json\ResourceCollection;

class PropertyCollection extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public $collects = PropertyTinyResource::class;

    public function toArray($request)
    {
        return [
            'status' => 1,
            'massage' => 'Success Request',
            'data' =>  parent::toArray($request),
            
        ];
           
    }
}

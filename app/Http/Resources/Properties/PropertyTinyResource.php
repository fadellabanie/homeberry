<?php

namespace App\Http\Resources\Properties;

use App\Http\Controllers\GeneralController;
use App\Traits\General;
use Illuminate\Http\Resources\Json\JsonResource;

class PropertyTinyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $image = "";
      
        if (count($this->medias) > 0) {
            $image = asset($this->medias->first()->file);
        }
        $rental = rental_frequency_const($this->propertyAttribute->rental);
        $furnished = furnished_status_const($this->propertyAttribute->furnished);
        $rent_type = rent_type_const($this->rent_type);

        if (auth('api')->check()) {
            $isFavourites = auth('api')->user()->favourites->where('id',$this->id)->count() != 0 ? true : false;
        }else{
            $isFavourites = false;
        }
        

        return [
            'id' => $this->id,
            'name' => $this->name_ar == Null ? $this->name_en : $this->name_ar ,
            'type' => $this->propertyAttribute->type ?? "",
            'price' => $this->price,
            'city' => $this->city->name,
            'views' => $this->views,
            'clicked' => $this->clicked,
            'rent_type' =>  $rent_type,
            'activity_type' => $this->activity_type_id,
            'area' => $this->propertyAttribute->area ?? "",
            'room' => $this->propertyAttribute->room ?? "",
            'bathroom' => $this->propertyAttribute->bathroom ?? "",
            'guest' => $this->propertyAttribute->guest ?? "",
            'availability' => $this->propertyAttribute->availability ?? "",
            'is_featured' => $this->priority == 1 ? true : false,
            'rental' => $rental,
            'lat' => $this->propertyAttribute->lat ?? "",
            'lng' => $this->propertyAttribute->lng ?? "",
            'furnished' => $furnished ,
            'is_favourites' => $isFavourites,
            'note_id' => $this->note != Null ? $this->note->id : 0,
            'note' => $this->note != Null ? $this->note->note : '',
            'created_at' => date_format($this->created_at,"Y-m-d H:i:s"),
            'expire_at' => date_format($this->created_at->addDays(GeneralController::EXPIRE_AT),"Y-m-d H:i:s"),
            'image' => $image,
            
        ];
    }
}

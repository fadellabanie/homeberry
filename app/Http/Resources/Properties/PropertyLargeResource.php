<?php

namespace App\Http\Resources\Properties;

use App\Http\Controllers\GeneralController;
use App\Http\Resources\Features\AmenitiesResource;
use App\Http\Resources\Features\NearbyResource;
use App\Models\Property;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class PropertyLargeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $locale = $request->header('Accept-Language');
        
        $related = relatedProperty($this->id,$this->city_id,$this->activity_type_id);

        $rental = rental_frequency_const($this->propertyAttribute->rental);
        $rent_type = rent_type_const($this->rent_type);
        $furnished = furnished_status_const($this->propertyAttribute->furnished);
        $images = [];
        foreach ($this->medias as $image) {
            $images[] = asset($image->file);
        }

   
        if($locale == 'ar'){
            if($this->name == null){
                $name = $this->name_en;
            }else{
                $name = $this->name_ar;
            }
        }else{
            if($this->name == null){
                $name = $this->name_ar;
            }else{
                $name = $this->name_en;
            }
        }
        if($locale == 'ar'){
            if($this->description == null){
                $description = $this->description_en;
            }else{
                $description = $this->description_ar;
            }
        }else{
            if($this->description == null){
                $description = $this->description_ar;
            }else{
                $description = $this->description_en;
            }
        }
        if (auth('api')->check()) {
            $isFavourites = auth('api')->user()->favourites->where('id',$this->id)->count() != 0 ? true : false;
         }else{
            $isFavourites = false;
        }
        
        return [
            'id' => $this->id,
            'name' => $name ,
            'description' => $description,
            'price' => $this->price,
            'city' => $this->city->name,
            'views'=> $this->views,
            'clicked'=> $this->clicked,
            'rent_type'=> $rent_type,
            'payment' => $this->paymentMathod->id,
            'activity_type' => $this->activity_type_id,

            'guest' => $this->propertyAttribute->guest ?? "",
            'availability' => $this->propertyAttribute->availability ?? "",
            'room' => $this->propertyAttribute->room ?? "",
            'bathroom' => $this->propertyAttribute->bathroom,
            'area' => $this->propertyAttribute->area,
            'park' => $this->propertyAttribute->park,
            'rental' =>  $rental,
            'furnished' => $furnished,
            'type' => $this->propertyAttribute->type,
            'url' => $this->propertyAttribute->url,
            'youtube' => $this->propertyAttribute->youtube,
            'lat' =>  $this->propertyAttribute->lat,
            'lng' => $this->propertyAttribute->lng,
            'is_favourites' => $isFavourites,
         
            // 'is_favourites' => auth('api')->user()->IsFavorited->where('id',$this->id)->count() != 0 ? true : false,
            'note_id' => $this->note != Null ? $this->note->id : 0,
            'note' =>$this->note != Null ? $this->note->note : '',
            'created_at' => date_format($this->created_at,"Y-m-d H:i:s"),

            'images' => $images,

           'amanties' =>$this->amenities->pluck('id')->toArray(),
            'nearby' =>$this->nearbyServices->pluck('id')->toArray(),
          
            'owner' => [
                'id' => $this->user->id,
                'name' => $this->user->name,
                'mobile' => $this->user->mobile,
                'email' => $this->user->email,
                'whatsapp_mobile' => $this->user->whatsapp_mobile,

            ],
            'similar project' => PropertyTinyResource::collection($related),
        ];
    }
}

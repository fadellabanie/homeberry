<?php

namespace App\Http\Resources;


use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{

    
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //$tokenResult = $this->createToken('Token-Login');
       
        return [
            'id' => $this->id,
            'name' => $this->name,
            'last_name' => $this->last_name,
            'mobile' => $this->mobile,
            'email' => $this->email,
            'type' => $this->type,
            'country_code' => $this->country_code,
            'created_at' => (string) $this->created_at,
            'verified' => (boolean) $this->hasVerifiedEmail(),
            'token_type' => 'Bearer',
            'access_token' => $this->remember_token,
            'expires_at' => Carbon::now()->addDays(15)->toDateTimeString(),
            // 'access_token' => $this->email_verified_at != null ? $tokenResult->accessToken : "",
            // 'expires_at' => $this->email_verified_at != null ? Carbon::parse($tokenResult->token->expires_at)->toDateTimeString() : '',
        ];
    }
}

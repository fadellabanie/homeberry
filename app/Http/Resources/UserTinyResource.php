<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserTinyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'prefix' => $this->prefix,
            'name' => $this->name,
            'last_name' => $this->last_name,
            'email' => $this->email,
            'type' => $this->type,
            'mobile' => $this->mobile,
            'landline' => $this->landline,
        ];
    }
}

<?php

namespace App\Http\Resources\Booking;

use Illuminate\Http\Resources\Json\JsonResource;

class CalaculateResource extends JsonResource
{

    public $property;
    public $cleaningFee;
    public $serviceFee;
    public $accommodationCost;
    public $total;

    public function __construct($resource,$property,$cleaningFee,$serviceFee,$accommodationCost,$total)
    {
        parent::__construct($resource);
        $this->resource = $resource;
        $this->property = $property;
        
        $this->cleaningFee= $cleaningFee;
        $this->serviceFee= $serviceFee;
        $this->accommodationCost= $accommodationCost;
        $this->total = $total;
    }
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            
            'property' => [
                'id'=>$this->property->id,
                'name'=>$this->property->name,
                'price'=>$this->property->price,
            ],
            'accommodation_cost' => $this->accommodationCost,
            'cleaning_fee' => $this->cleaningFee,
            'service_fee' => $this->serviceFee,
            'total' => $this->total,
        ];
    }
}

<?php

namespace App\Http\Resources\Booking;

use Illuminate\Http\Resources\Json\JsonResource;

class BookingResource extends JsonResource
{

    public $cleaningFee;
    public $serviceFee;
    public $accommodationCost;

    public function __construct($resource,$cleaningFee,$serviceFee,$accommodationCost)
    {
        parent::__construct($resource);
        $this->resource = $resource;
        $this->cleaningFee = $cleaningFee;
        $this->serviceFee= $serviceFee;
        $this->accommodationCost= $accommodationCost;
    }
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user' => [
               'id'=> $this->user->id,
               'name'=> $this->user->name,
               'email'=> $this->user->email,
            ],
            'property' => [
                'id'=>$this->property->id,
                'name'=>$this->property->name,
                'price'=>$this->property->price,
            ],
            'status_of_payment' => (boolean) $this->status,
            'accommodation cost' => $this->accommodationCost,
            'cleaning Fee' => $this->cleaningFee,
            'service Fee' => $this->serviceFee,
            'total' => $this->total,
        ];
    }
}

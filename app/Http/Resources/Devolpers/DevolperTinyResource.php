<?php

namespace App\Http\Resources\Devolpers;

use App\Models\Project;
use Illuminate\Http\Resources\Json\JsonResource;

class DevolperTinyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'logo' => asset($this->logo),
            'projects_count' => $this->projects->count(),
        ];
    }
}

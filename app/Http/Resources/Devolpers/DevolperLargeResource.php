<?php

namespace App\Http\Resources\Devolpers;

use App\Http\Resources\Projects\ProjectTinyResource;
use App\Models\Project;
use Illuminate\Http\Resources\Json\JsonResource;

class DevolperLargeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $projects = Project::whereDevolper_id($this->id)->get();
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description'=> $this->description,
            'images' => asset($this->logo) ?? '' ,
            // 'mobile' => $this->mobile,
            // 'email' => $this->email,
            // 'whatsapp_mobile' => $this->whatsapp_mobile,
            'projects' => ProjectTinyResource::collection($projects), 
        ];
    }
}

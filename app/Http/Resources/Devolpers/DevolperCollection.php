<?php

namespace App\Http\Resources\Devolpers;

use Illuminate\Http\Resources\Json\ResourceCollection;

class DevolperCollection extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
   
    public $collects = DevolperTinyResource::class;

    public function toArray($request)
    {
        return [
            'status' => 1,
            'massage' => 'Success Request',
            'data' =>  parent::toArray($request),
        ];
    }
}

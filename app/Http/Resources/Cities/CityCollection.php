<?php

namespace App\Http\Resources\Cities;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CityCollection extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public $collects = CityTinyResource::class;

    public function toArray($request)
    {
        return [
            'status' => 1,
            'massage' => 'Success Request',
            'data' => parent::toArray($request),
        ];

    }
}

<?php

namespace App\Http\Resources\Cities;

use Illuminate\Http\Resources\Json\JsonResource;

class CityTinyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'is_featured' => (boolean) $this->priority,
            'image' => asset($this->image) ?? "",
            'governorate' => [
                'id' => $this->governorate->id,
                'name' => $this->governorate->name,

            ],

        ];
    }
}

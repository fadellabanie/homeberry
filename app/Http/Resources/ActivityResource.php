<?php

namespace App\Http\Resources;

use App\Http\Resources\Properties\PropertyTinyResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ActivityResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'activity' => $this->activity->name,
            'name_ar' => $this->name_ar,
            'name_en' => $this->name_en,
            'icon'=> $this->icon,
        ];
    }
}

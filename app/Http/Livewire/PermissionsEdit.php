<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Spatie\Permission\Models\Permission;

class PermissionsEdit extends Component
{
        
    public $permission;
    public $name;
   

    public $successMessage;
    protected $rules = [
        'name' => 'required|string|min:2|max:100',
       
    ];
    /**
     * mount or construct function
     */
    public function mount(Permission $permission)
    {
        $this->role = null;
        if($permission) {
            $this->permission = $permission;
            $this->name  = $this->permission->name;
        }

    }
    public function submitForm()
    {
        $validatedDate = $this->validate();
        sleep(1);

        $this->permission->update($validatedDate);

        $this->successMessage = 'successfully ';

        $this->resetForm();

        $this->dispatchBrowserEvent(
            'alert',
            [
                'type' => 'success',
                'title' => 'Updated',
                'message' => 'successfully Saved',
                'footer' => ''
            ]
        );
    }
    private function resetForm()
    {
        $this->name = '';
      
    }
    public function render()
    {
        return view('livewire.permissions-edit')->extends('admin.layouts.app')
        ->section('content');
    }
}

<?php

namespace App\Http\Livewire;

use App\Models\ActivityType;
use App\Models\City;
use App\Models\Devolper;
use App\Models\FurnishedType;
use App\Models\PaymentMethod;
use App\Models\Project;
use App\Models\ProjectDetail;
use Livewire\Component;
use Livewire\WithFileUploads;

class ProjectsAdd extends Component
{
    use WithFileUploads;

    public $name_ar;
    public $name_en;
    public $description_ar;
    public $description_en;
    public $devolper_id;
    public $city_id;
    public $finished;
    public $payment_id; //

    public $activitytypes = [];
    public $furnishedtypes = [];
   
    public $delivery;
    public $link;
    public $youtube;//
    //public $price;
    public $is_constructed; //
    public $status;
   
   
    public $priority;
    public $images = [];

    public $location;//

    public $key ,$value;
    public $updateMode = false;
    public $inputs = [0=>''];
    public $i = 1;

    public $successMessage;
    protected $rules = [
        'name_ar' => 'required|min:3|max:150',
        'name_en' => 'required|min:3|max:150',
        'status' => 'required',
        'priority' => 'required',
       
       
                
        'description_ar' => 'required|min:3',
        'description_en' => 'required|min:3',
        'devolper_id' => 'required',
        'city_id' => 'required',
        'payment_id' => 'required',
        'activitytypes' => 'required',
        'furnishedtypes' => 'required',
        //'price' => 'required|gt:0',
        'finished' => 'required',
        'delivery' => 'required',
        'location' => 'required',
        'link' => '',
        'youtube' => '',
        'images' => 'array',
        'images.*' => 'image|max:1024',

    ];

   
    public function add($i)
    {
        $i = $i + 1;
        $this->i = $i;
        array_push($this->inputs, $i);
    }

    public function remove($i)
    {
        unset($this->inputs[$i]);
    }

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function submit()
    {

        $validatedDate = $this->validate();
        sleep(1);


        $validatedDate = array_diff_key($validatedDate,['images' => 0]);

        $project = Project::create($validatedDate);

        
        $project->activityTypes()->attach($validatedDate['activitytypes']);
        $project->furnishedTypes()->attach($validatedDate['furnishedtypes']);

        if($this->key != null){
            foreach ($this->key as $key => $value) {
                ProjectDetail::create([
                   'key' => $this->key[$key],
                   'value'=> $this->value[$key],
                   'project_id' => $project->id
               ]);
            }
        }
       

        if ($this->images){
            foreach ($this->images as $image) {
                $path = $image->store('uploads/projects');
                $path= 'storage/app/' . $path;
                storeMediaPath($path,$project->id,'App\Models\Project');
            }
        }


        $this->successMessage = 'successfully ';

        $this->resetForm();
/*
        $this->dispatchBrowserEvent(
            'alert',
            [
                'type' => 'success',
                'title' => 'Create Project' ,
                'message' => 'Project successfully Saved',
                'footer' => ''
            ]
        );
*/
        // $this->emit('alert', ['type' => 'success', 'message' => 'Project successfully Created.']);
        sleep(1);

        return redirect()->to('/homeberry-portal/add-loaction-project');
    }

    private function resetForm()
    {
        $this->name_ar = '';
        $this->name_en = '';
    }

    public function render()
    {
        return view('livewire.projects-add', [
            'payments' => PaymentMethod::all(),
            'developers' => Devolper::all(),
            'cities' => City::all(),
            'activitytypeArr' => ActivityType::all(),
            'furnishedtypeArr' => FurnishedType::all(),
        ])->extends('admin.layouts.app')
            ->section('content');
    }
}

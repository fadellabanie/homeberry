<?php

namespace App\Http\Livewire;

use App\Models\Notification;
use App\Services\Notification as ServicesNotification;
use App\User;
use Livewire\Component;

class NotificationsSend extends Component
{
    public $notification;
    public $title;
    public $content;
    public $topic;
    public $successMessage;
    public $users;

    protected $rules = [
        'users' => 'array',
        'users.*' => 'exists:users,id'
    ];

    /**
     * mount or construct function
     */
    public function mount(Notification $notification)
    {
        $this->notification = null;
        if ($notification) {
            $this->notification = $notification;
        }
    }

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function submitForm()
    {
        $validatedDate = $this->validate();
        sleep(1);

        $title =  $this->notification->title;
        $message =  $this->notification->content;
        $topic =  $this->notification->topic;
         $user_id =  $this->notification->user_id;
        // $model =  $this->notification->model //device ios or android;
        //$type = $this->notificationTypeKey->getTopicType() . 'useridAndroid' . $user_id; ///socket
        $type = 'AllUserAndroid';
        $dataPush = [];
        $device = "";

        $result = ServicesNotification::send($user_id,$message, $title);
        $response = [
            'status' => 1,
            'result' => $result
        ];



        $this->notification->users()->attach($this->users);

        $this->successMessage = 'successfully '. $response;

        $this->resetForm();

        $this->dispatchBrowserEvent(
            'alert',
            [
                'type' => 'success',
                'title' => 'sended',
                'message' => 'successfully Saved',
                'footer' => ''
            ]
        );
    }

    private function resetForm()
    {
        $this->name_ar = '';
        $this->name_en = '';
    }

    public function render()
    {
        return view('livewire.notifications-send',[
            'usersdb' => User::Active()->get()
        ])
        ->extends('admin.layouts.app')
        ->section('content');
    }
}

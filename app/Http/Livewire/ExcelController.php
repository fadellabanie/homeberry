<?php

namespace App\Http\Livewire;

use App\Exports\DeveloperExport;
use App\Exports\ProjectExport;
use App\Exports\PropertyExport;
use App\Exports\UserExport;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ExcelController extends Controller
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function ProjectExport()
    {
        return Excel::download(new ProjectExport, 'Projects.xlsx');
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function PropertyExport()
    {
        return Excel::download(new PropertyExport, 'Properties.xlsx');
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function UserExport()
    {
        return Excel::download(new UserExport, 'Users.xlsx');
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function DeveloperExport()
    {
        return Excel::download(new DeveloperExport, 'Developers.xlsx');
    }
}

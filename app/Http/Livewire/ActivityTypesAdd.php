<?php

namespace App\Http\Livewire;

use App\Models\Activity;
use App\Models\ActivityType;
use Livewire\Component;
use Livewire\WithFileUploads;

class ActivityTypesAdd extends Component
{
    use WithFileUploads;

    public $name_ar;
    public $name_en;
    public $status;
    public $icon;
    public $activity_id;
    public $successMessage;
    protected $rules = [
        'name_ar' => 'required|min:2|max:50',
        'name_en' => 'required|min:2|max:50',
        'activity_id' => 'required',
        'status' => 'required',
        'icon' => 'image|max:1024',

    ];
    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function submitForm()
    {
        $validatedDate = $this->validate();
        sleep(1);

        $path = $this->icon->store('uploads/activityTypes');
        $path= 'storage/app/' . $path;


        ActivityType::create(array_merge($validatedDate,['icon'=>$path]));

        $this->successMessage = 'successfully ';
        $this->resetForm();

        $this->dispatchBrowserEvent(
            'alert',
            [
                'type' => 'success',
                'title' => 'Created',
                'message' => 'successfully Saved',
                'footer' => ''
            ]
        );


    }

    private function resetForm()
    {
        $this->name_ar = '';
        $this->name_en = '';

    }

    public function render()
    {
        return view('livewire.activity-types-add',[
            'activities' => Activity::all(),
        ])->extends('admin.layouts.app')
        ->section('content');
    }
}

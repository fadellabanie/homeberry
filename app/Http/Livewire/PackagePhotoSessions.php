<?php

namespace App\Http\Livewire;

use App\Models\Package;
use Livewire\Component;

class PackagePhotoSessions extends Component
{

    public function destroy($id)
    {
        if ($id) {
            $record = Package::whereId($id);
            $record->delete();
        }
     // $this->emit('alert',['type' => 'success','message'=>'Successfully Deleted']);
    }
    public function render()
    {
        return view('livewire.package-photo-sessions',[
            'packages' => Package::whereType('photo-sessions')->get(),
        ])
        ->extends('admin.layouts.app')
        ->section('content');
    }
}

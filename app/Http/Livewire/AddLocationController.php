<?php

namespace App\Http\Livewire;

use App\Http\Controllers\Controller;
use App\Models\Project;
use App\Models\Property;
use App\Models\PropertyAttribute;
use Illuminate\Http\Request;
use Alert;

class AddLocationController extends Controller
{

    /**
     * View For Add Location for Project 
     *
     * @return void
     */
    public function index()
    {
        $projects = Project::whereLat(Null)->orWhere('lng',Null)->get();
       
        return view('admin.addLocationProject', compact('projects'));
    }
    /**
     * addLocationToProject Find PRoject ID and update Lat ,Lng
     *
     * @param  mixed $request
     * @return void
     */
    public function addLocationToProject(Request $request)
    {

        $project = Project::find($request->id);
        $project->update(['lat' => $request->lat, 'lng' => $request->lng]);
        Alert::success('Create', 'Success Add Location');

        return redirect('homeberry-portal/projects');
    }
    /**
     *  View For Add Location for Property
     *
     * @return void
     */
    public function indexProperty()
    {

        $properties = Property::whereHas('propertyAttribute', function ($query) {
            $query->where(['lat' => null]);
        })->get();

        return view('admin.addLocationProperty', compact('properties'));
    }
    /**
     * addLocationTo Property Find PRoject ID and update Lat ,Lng
     *
     * @param  mixed $request
     * @return void
     */
    public function addLocationToProperty(Request $request)
    {

        $property = PropertyAttribute::where('property_id', $request->id)->first();
        $property->update(['lat' => $request->lat, 'lng' => $request->lng]);
        Alert::success('Create', 'Success Add Location');
      
        return redirect('homeberry-portal/properties');

    }
}

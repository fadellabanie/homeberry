<?php

namespace App\Http\Livewire;

use App\Http\Controllers\GeneralController;
use App\Models\Schedule;
use Livewire\Component;
use Livewire\WithPagination;

class Schedules extends Component
{
    use WithPagination;

    //protected $queryString = ['search'];
    public $active = true;
    public $priority = false;
    public $search;
    public $sortField;
    public $sortAsc = true;
    protected $queryString = ['search', 'active', 'priority', 'sortAsc', 'sortField'];

    public function mount(): void
    {
        $this->search = request()->query('search', $this->search);
    }

    public function paginationView()
    {
        return 'custom-pagination-links-view';
    }
    public function sortBy($field)
    {
        if ($this->sortField === $field) {
            $this->sortAsc = !$this->sortAsc;
        } else {
            $this->sortAsc = true;
        }

        $this->sortField = $field;
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        return view('livewire.schedules', [
            'schedulesProperty' => Schedule::with('user','property')->where('schedulable_type','App\Models\Property')->when($this->sortField, function ($query) {
                    $query->orderBy($this->sortField, $this->sortAsc ? 'asc' : 'desc');
                })
                ->paginate(GeneralController::PAGINATE_NUMBER_LARGE_CARD),
            'schedulesProject' => Schedule::with('user','project')->where('schedulable_type','App\Models\Project')->when($this->sortField, function ($query) {
                    $query->orderBy($this->sortField, $this->sortAsc ? 'asc' : 'desc');
                })
                ->paginate(GeneralController::PAGINATE_NUMBER_LARGE_CARD),
        ])
            ->extends('admin.layouts.app')
            ->section('content');
    }
}

<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Spatie\Permission\Models\Role;

class GroupsAdd extends Component
{
    public $name;

    public $successMessage;
    protected $rules = [
        'name' => 'required|min:2|max:100',
    ];

    public function submit()
    {
        $validatedDate = $this->validate();
        
        Role::create($validatedDate);

        $this->successMessage = 'successfully ';

        $this->resetForm();

        $this->dispatchBrowserEvent(
            'alert',
            [
                'type' => 'success',
                'title' => 'Created',
                'message' => 'successfully Saved',
                'footer' => ''
            ]
        );
        sleep(1);

    }

    private function resetForm()
    {
        $this->name = '';

    }
    public function render()
    {
        return view('livewire.groups-add')->extends('admin.layouts.app')
        ->section('content');
    }
}

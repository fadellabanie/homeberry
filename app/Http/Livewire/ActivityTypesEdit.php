<?php

namespace App\Http\Livewire;

use App\Models\Activity;
use App\Models\ActivityType;
use Livewire\Component;
use Livewire\WithFileUploads;

class ActivityTypesEdit extends Component
{
    use WithFileUploads;

    public $activityType;
    public $name_ar;
    public $name_en;
    public $status;
    public $icon;
    public $lasticon;

    public $activity_id;
    public $successMessage;
    protected $rules = [
        'name_ar' => 'required|min:2|max:50',
        'name_en' => 'required|min:2|max:50',
        'activity_id' => 'required',
        'status' => 'required',
    ];

        /**
     * mount or construct function
     */
    public function mount(ActivityType $activityType)
    {
        $this->activityType = null;
        if($activityType) {
            $this->activityType = $activityType;
            $this->activity_id   = $this->activityType->activity_id;
            $this->name_ar    = $this->activityType->name_ar;
            $this->name_en  = $this->activityType->name_en;
            $this->status  = $this->activityType->status;
            $this->lasticon = $this->activityType->icon;
        }

    }

    public function updated($propertyName)
    {
        if ($propertyName == "icon") {
            $this->lasticon  = "";
        }

        $this->validateOnly($propertyName);
    }

    public function submitForm()
    {
        $validatedDate = $this->validate();
        sleep(1);

        if ($this->icon != null) {
            $this->validate([
                'icon' => 'image|max:2024',
            ]);
            $path = $this->icon->store('uploads/activityTypes');
            $validatedDate['icon'] = 'storage/app/' . $path;
        } else {
            unset($validatedDate['icon']);
        }

// dd($validatedDate);
        $this->activityType->update($validatedDate);

        $this->successMessage = 'successfully ';

        $this->resetForm();

        $this->dispatchBrowserEvent(
            'alert',
            [
                'type' => 'success',
                'title' => 'updated',
                'message' => 'successfully Saved',
                'footer' => ''
            ]
        );
    }

    private function resetForm()
    {
        $this->name_ar = '';
        $this->name_en = '';
    }

    public function edit($id)
    {
        $record = ActivityType::findOrFail($id);
        $this->name_ar = $record->name_ar;
        $this->name_en = $record->name_en;
        $this->activity_id = $record->activity_id;
        $this->status = $record->status;
    }
    public function render()
    {

        return view('livewire.activity-types-edit',[
            'activities' => Activity::all(),
        ])->extends('admin.layouts.app')
        ->section('content');
    }
}

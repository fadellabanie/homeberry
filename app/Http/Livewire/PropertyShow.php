<?php

namespace App\Http\Livewire;

use App\Models\Property;
use Livewire\Component;

class PropertyShow extends Component
{
    public $property;

    public $name_ar;
    public $name_en;
    public $status;
    public $images = [];

    public $successMessage;
  
    public function mount(Property $property)
    {
        $this->property = null;
        if ($property) {
            $this->property = $property;
            $this->name_ar = $this->property->name_ar;
            $this->name_en = $this->property->name_en;
            $this->status  = $this->property->status;

        }
    }
    public function destroy($id)
    {
        if ($id) {
            $record = Property::whereId($id);
            $record->delete();
        }
        return redirect('homeberry-portal/properties');
     // $this->emit('alert',['type' => 'success','message'=>'Successfully Deleted']);
    }
    public function render()
    {
       // dd($this->property);
        return view('livewire.property-show', [
            'property' => $this->property
        ])->extends('admin.layouts.app')
        ->section('content');
    }
}

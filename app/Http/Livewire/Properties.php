<?php

namespace App\Http\Livewire;

use App\Http\Controllers\GeneralController;
use App\Models\Property;
use Livewire\Component;
use Livewire\WithPagination;
use App\Services\Notification;
use Alert;
class Properties extends Component
{
    use WithPagination;
    public $active = true;
    public $priority = true;
    public $search;
    protected $listeners = ['refreshComponent' => '$refresh'];
    protected $queryString = ['search','active','priority'];

    public function mount()
    {
        return view('livewire.properties', [
            'properties' => Property::paginate(12)
        ]) ->extends('admin.layouts.app')
        ->section('content');
    }
    public function paginationView()
    {
        return 'custom-pagination-links-view';
    }

    public function destroy($id)
    {
        $property = Property::findOrFail($id);
        $property->delete();
     
        $title = __("Delete");
        $msg = __("You Property Is Deleted");
        $notification = new Notification;
        $notification->send(GeneralController::SEND_NOTIFICATION_FOR_USER.$property->user_id, $title, $msg);
    } 
    
    public function approve($id)
    {
        $property = Property::findOrFail($id);
        $property->update(['status'=>true]);
        
         
        $title = __("Approve");
        $msg = __("Now You Property Is Approved");
        $notification = new Notification;
        $notification->send(GeneralController::SEND_NOTIFICATION_FOR_USER.$property->user_id, $title, $msg);
        
    // $this->emit('alert',['type' => 'success','message'=>'Successfully Approve']);

    }

    public function render()
    {

        return view('livewire.properties', [
            'properties' =>  Property::where('name_ar', 'like', '%' . $this->search . '%')
            ->orwhere('name_en', 'like', '%' . $this->search . '%')
            ->where('status', $this->active)
            ->where('priority', $this->priority)
            ->orderBy('id', 'desc')
            ->paginate(GeneralController::PAGINATE_NUMBER_LARGE_CARD)
        ])
        ->extends('admin.layouts.app')
        ->section('content');
    }
}

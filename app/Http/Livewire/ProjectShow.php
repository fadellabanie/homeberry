<?php

namespace App\Http\Livewire;

use App\Models\Media;
use App\Models\Project;
use Livewire\Component;

class ProjectShow extends Component
{
    public $project;

    public $name_ar;
    public $name_en;
    public $description_ar;
    public $description_en;
    public $devolper_id;
    public $city_id;
    public $activitytypes = [];
    public $finished;
    public $delivery;
    public $link;
    public $price;
    public $status;
    public $created_at;
    public $images = [];

    public $successMessage;

    /**
     * mount or construct function
     */
    public function mount(project $project)
    {
        $this->project = null;
        if ($project) {
            $this->project = $project;
            $this->name_ar = $this->project->name_ar;
            $this->name_en = $this->project->name_en;
            $this->status  = $this->project->status;
            $this->description_ar  = $this->project->description_ar;
            $this->description_en = $this->project->description_en;
            $this->devolper_id = $this->project->devolper_id;
            $this->city_id = $this->project->city_id;
            $this->activitytypes = $this->project->activitytypes->all();
            $this->finished = $this->project->furnishedTypes->all();
            $this->delivery = $this->project->delivery;
            $this->link = $this->project->link;
            $this->price = $this->project->price;
            $this->created_at = $this->project->created_at;

            $this->images = Media::where('mediable_type', 'App/Models/Project')->where('mediable_id', $this->project->id)->get();
        }
    }

    public function render()
    {
       
        return view('livewire.project-show', [
            'project' => $this->project
        ])->extends('admin.layouts.app')
            ->section('content');
    }

}

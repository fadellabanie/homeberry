<?php

namespace App\Http\Livewire;

use App\User;
use Livewire\Component;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;

class UsersEdit extends Component
{
          
    public $user;
    public $name;
    public $last_name;
    public $email;
    public $password;
    public $mobile;
    public $whatsapp_mobile;
    public $groups_id;
    public $type;
    public $country_code;

    public $successMessage;
    protected $rules = [
        'name' => 'required|min:2|max:100',
        'last_name' => 'required|min:2|max:100',
       
        'type' => '',
        'email' => 'required|email',
        'whatsapp_mobile' => 'required',
        'mobile' => 'required',
        'country_code' => 'required',
    ];

   
    /**
     * mount or construct function
     */
    public function mount(User $user)
    {

        $this->user = null;
        if($user) {
            $this->user = $user;
            $this->name  = $this->user->name;
            $this->last_name  = $this->user->last_name;
            $this->groups_id  = $this->user->roles->first()->id ?? '';
          
            $this->email  = $this->user->email;
            $this->mobile  = $this->user->mobile;
            $this->whatsapp_mobile  = $this->user->whatsapp_mobile;
            $this->country_code  = $this->user->country_code;
            $this->type  = $this->user->type;
        }

    }
    public function submitForm()
    {
        $data = $this->validate();
        sleep(1);
       


        if ($this->password != null) {
            $data['password'] = Hash::make($this->password);
            $this->user->update([
                'password' => $data['password'],
            ]);
        }
       

        $this->user->update([
            'name' => $data['name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'mobile' => $data['mobile'],
            'whatsapp_mobile' => $data['whatsapp_mobile'],
            'country_code' => $data['country_code'],
            'type' =>  $data['type'],
        ]);

        $this->user->syncRoles($data['groups_id']);

        $this->successMessage = 'successfully ';

        $this->resetForm();

        $this->dispatchBrowserEvent(
            'alert',
            [
                'type' => 'success',
                'title' => 'Updated',
                'message' => 'successfully Saved',
                'footer' => ''
            ]
        );
    }
    private function resetForm()
    {
        $this->name = '';
        $this->last_name= '';
        $this->mobile= '';
        $this->email= '';
        $this->password= '';
        $this->whatsapp_mobile= '';
    }
    public function render()
    {
        return view('livewire.users-edit',[
            'groups' => Role::all(),
        ])
        ->extends('admin.layouts.app')
        ->section('content');
    }
}

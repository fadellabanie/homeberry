<?php

namespace App\Http\Livewire;

use App\User;
use Carbon\Carbon;
use Livewire\Component;

class UserShow extends Component
{
    public $user;
    public $name;
    public $email;
    public $mobile;
    public $type;
    public $verify;

    public $successMessage;


    public function mount(User $user)
    {
        $this->user = null;
        if ($user) {
            $this->user = $user;
            $this->name = $this->user->name;
            $this->email = $this->user->email;
            $this->mobile  = $this->user->mobile;
            $this->type  = $this->user->type;
            $this->verify  = Carbon::parse($this->user->email_verified_at);
        }
    }

    public function render()
    {

        return view('livewire.user-show', [
            'user' => $this->user,
            'verify' => $this->verify
        ])->extends('admin.layouts.app')
            ->section('content');
    }

}

<?php

namespace App\Http\Livewire;

use App\Models\Package;
use Livewire\Component;

class PackagesAdd extends Component
{
    public $name_ar;
    public $name_en;
    public $description_ar;
    public $description_en;
    public $count;
    public $price;
    public $type;
    public $successMessage ;

    protected $rules = [
        'type' => 'required',
        'name_ar' => 'required|min:3|max:150',
        'name_en' => 'required|min:3|max:150',
        'description_ar' => 'required|min:3',
        'description_en' => 'required|min:3',
        'price' => 'required|gt:0',
        'count' => 'nullable|gt:0',

    ];

public function submit()
    {
        $validatedDate = $this->validate();
        sleep(1);
       
        Package::create($validatedDate);

        $this->successMessage = 'successfully ';
        $this->resetForm();

        $this->dispatchBrowserEvent(
            'alert',
            ['type' => 'success',
            'title' => 'Create package' ,
            'message' => 'package successfully Saved',
            'footer' => '']
        );

        sleep(2);
    }


    private function resetForm()
    {
        $this->type = true;
        $this->name_ar = '';
        $this->name_en = '';
        $this->description_ar = '';
        $this->description_en = '';
        $this->price = '';
        $this->count = '1';
    }
    public function render()
    {
        return view('livewire.packages-add')->extends('admin.layouts.app')
        ->section('content');
    }
}

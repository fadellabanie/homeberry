<?php

namespace App\Http\Livewire;

use App\Http\Controllers\GeneralController;
use App\Models\Reserve;
use Livewire\Component;
use Livewire\WithPagination;

class Booking extends Component
{
    use WithPagination;

    //protected $queryString = ['search'];
    public $active = true;
   
    public $search;
    public $sortField;
    public $sortAsc = true;
    protected $queryString = ['search', 'active', 'sortAsc', 'sortField'];

    public function mount(): void
    {
        $this->search = request()->query('search', $this->search);
    }

    public function paginationView()
    {
        return 'custom-pagination-links-view';
    }
    public function sortBy($field)
    {
        if ($this->sortField === $field) {
            $this->sortAsc = !$this->sortAsc;
        } else {
            $this->sortAsc = true;
        }

        $this->sortField = $field;
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        return view('livewire.booking',[
            'reserves' => Reserve::
          
            where('status', $this->active)
            ->orderBy('id', 'desc')
            ->paginate(GeneralController::PAGINATE_NUMBER_LARGE_PAGE),
        ])
        ->extends('admin.layouts.app')
        ->section('content');
    }
}

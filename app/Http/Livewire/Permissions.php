<?php

namespace App\Http\Livewire;

use App\Http\Controllers\GeneralController;
use Livewire\Component;
use Livewire\WithPagination;
use Spatie\Permission\Models\Permission;

class Permissions extends Component
{
    use WithPagination;
  
  public $search;
  public $sortField;
  public $sortAsc = true;
  protected $queryString = ['search', 'sortAsc', 'sortField'];

  public function mount(): void
  {
      $this->search = request()->query('search', $this->search);
  }

  public function paginationView()
  {
      return 'custom-pagination-links-view';
  }
  public function sortBy($field)
  {
      if ($this->sortField === $field) {
          $this->sortAsc = !$this->sortAsc;
      } else {
          $this->sortAsc = true;
      }

      $this->sortField = $field;
  }

  public function updatingSearch()
  {
      $this->resetPage();
  }

  public function destroy($id)
  {
      if ($id) {
          $record = Permission::whereId($id);
          $record->delete();
      }
  }

    public function render()
    {
        return view('livewire.permissions',[
            'permissions' => Permission::when($this->sortField, function ($query) {
                $query->orderBy($this->sortField, $this->sortAsc ? 'asc' : 'desc');
            })->paginate(GeneralController::PAGINATE_NUMBER_LARGE_PAGE),
            
        ])->extends('admin.layouts.app')
        ->section('content');
            
           
    }
}

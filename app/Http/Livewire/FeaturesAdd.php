<?php

namespace App\Http\Livewire;

use App\Models\Feature;
use Illuminate\Validation\Rule;
use Livewire\Component;
use Livewire\WithFileUploads;

class FeaturesAdd extends Component
{
    use WithFileUploads;

    public $name_ar;
    public $name_en;
    public $type;
    public $icon;

    public $successMessage;
    protected $rules = [
        'name_ar' => 'required|min:2',
        'name_en' => 'required|min:2',
        'type' =>  'required|in:nearby services,amenities',
 
    ];
    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function submitForm()
    {
        $validatedDate = $this->validate();
        sleep(1);

        if ($this->icon != null) {
            $this->validate([
                'icon' => 'image|max:2024',
            ]);
            $path = $this->icon->store('uploads/features');
            $validatedDate['icon'] = 'storage/app/' . $path;
        } else {
            unset($validatedDate['icon']);
        }


        Feature::create( $validatedDate);

        $this->successMessage = 'successfully ';
        $this->resetForm();

        $this->dispatchBrowserEvent(
            'alert',
            [
                'type' => 'success',
                'title' => 'Created',
                'message' => 'successfully Saved',
                'footer' => ''
            ]
        );
    }

    private function resetForm()
    {
        $this->name_ar = '';
        $this->name_en = '';
    }

    public function render()
    {
        return view('livewire.features-add')
        ->extends('admin.layouts.app')
            ->section('content');
    }


}

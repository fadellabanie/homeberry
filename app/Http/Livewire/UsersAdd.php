<?php

namespace App\Http\Livewire;

use App\User;
use Carbon\Carbon;
use Livewire\Component;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;

class UsersAdd extends Component
{
    public $name;
    public $last_name;
    public $email;
    public $password;
    public $mobile;
    public $whatsapp_mobile;
    public $groups_id;
    public $type;
    public $country_code;

    public $successMessage;
    protected $rules = [
        'name' => 'required|min:2|max:100',
        'last_name' => 'required|min:2|max:100',
        'password' => 'required',
        'type' => 'required',
        'email' => 'required|email',
        'whatsapp_mobile' => 'required',
        'mobile' => 'required',
        'country_code' => 'required',
        
    ];

    public function submit()
    {
        $data = $this->validate();
    
        $user = new User();
        $user->name =  $data['name'];
        $user->last_name =  $data['last_name'];
        $user->password =  Hash::make($data['password']);
        $user->email =  $data['email'];
        $user->mobile =  $data['mobile'];
        $user->whatsapp_mobile =  $data['whatsapp_mobile'];
        $user->country_code =  $data['country_code'];
        $user->type =  $data['type'];
        $user->email_verified_at =  Carbon::now();
        $user->property_count =  getPropertyCount();

        $user->save();

        $user->assignRole($this->groups_id);

        $this->successMessage = 'successfully ';

        $this->resetForm();

        $this->dispatchBrowserEvent(
            'alert',
            [
                'type' => 'success',
                'title' => 'Created',
                'message' => 'successfully Saved',
                'footer' => ''
            ]
        );
        sleep(1);
    }

    private function resetForm()
    {
        $this->name = '';
        $this->last_name= '';
        $this->mobile= '';
        $this->email= '';
        $this->password= '';
        $this->whatsapp_mobile= '';

    }
  
    public function render()
    {
        return view('livewire.users-add',[
            'groups' => Role::all(),
        ])
        ->extends('admin.layouts.app')
        ->section('content');
    }
}

<?php

namespace App\Http\Livewire;

use App\Models\ActivityType;
use App\Models\City;
use App\Models\Devolper;
use Livewire\Component;
use Livewire\WithFileUploads;

class DevelopersEdit extends Component
{
    use WithFileUploads;

    public $developer;

    public $name;
    public $description;
    public $logo;
    public $lastlogo;
    public $email;
    public $mobile;
    public $whatsapp_mobile;

    public $successMessage;
    protected $rules = [
        'name' => 'required|min:2|max:100',
        'description' => 'required|min:10',
        'email' => 'required|email',
        'mobile' => 'required',
        'whatsapp_mobile' => 'required',
    ];

    /**
     * mount or construct function
     */
    public function mount(Devolper $developer)
    {
        $this->developer = null;
        if ($developer) {
            $this->developer = $developer;
            $this->name = $this->developer->name;
            $this->description  = $this->developer->description;
            $this->lastlogo =  $this->developer->logo;
            $this->email =  $this->developer->email;
            $this->mobile =  $this->developer->mobile;
            $this->whatsapp_mobile =  $this->developer->whatsapp_mobile;
        }
    }

    public function updated($propertyName)
    {
        if ($propertyName == "logo") {
            $this->lastlogo  = "";
        }
        $this->validateOnly($propertyName);
    }

    public function submit()
    {
        $validatedDate = $this->validate();
        sleep(1);

        if ($this->logo != null) {
            $this->validate([
                'logo' => 'image|max:2024',
            ]);
            $path =  $this->logo->store('uploads/cities');
            $validatedDate['logo'] = 'storage/app/' . $path;
        } else {
            unset($validatedDate['logo']);
        }

        $this->developer->update($validatedDate);

        $this->successMessage = 'successfully ';

        $this->resetForm();

        $this->dispatchBrowserEvent(
            'alert',
            [
                'type' => 'success',
                'title' => 'updated Developer',
                'message' => 'Developer successfully updated',
                'footer' => ''
            ]
        );

        // return redirect()->to('/admin/Developers');
    }

    private function resetForm()
    {
        $this->name = '';
        $this->description = '';
        $this->mobile = '';
        $this->email = '';
        $this->whatsapp_mobile = '';
        $this->logo = '';
    }

    public function edit($id)
    {
        $record = Devolper::findOrFail($id);
        $this->name = $record->name;
        $this->status = $record->status;
    }
    public function render()
    {
        return view('livewire.developers-edit')
            ->extends('admin.layouts.app')
            ->section('content');
    }
}

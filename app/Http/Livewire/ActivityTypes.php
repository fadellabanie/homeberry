<?php

namespace App\Http\Livewire;

use App\Http\Controllers\GeneralController;
use App\Models\ActivityType;
use Livewire\Component;
use Livewire\WithPagination;

class ActivityTypes extends Component
{
    use WithPagination;

    //protected $queryString = ['search'];
    public $active = true;
    public $search;
    public $sortField;
    public $sortAsc = true;
    protected $queryString = ['search', 'active', 'sortAsc', 'sortField'];

    public function paginationView()
    {
        return 'custom-pagination-links-view';
    }
    public function sortBy($field)
    {
        if ($this->sortField === $field) {
            $this->sortAsc = !$this->sortAsc;
        } else {
            $this->sortAsc = true;
        }

        $this->sortField = $field;
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {

        return view('livewire.activity-types', [
            'activityTypes' => ActivityType::where(function ($query) {
                $query->where('name_ar', 'like', '%' . $this->search . '%')
                    ->orWhere('name_en', 'like', '%' . $this->search . '%');
            })->where('status', $this->active)
                ->when($this->sortField, function ($query) {
                    $query->orderBy($this->sortField, $this->sortAsc ? 'asc' : 'desc');
                })->paginate(GeneralController::PAGINATE_NUMBER_LARGE_PAGE),
        ])->extends('admin.layouts.app')
            ->section('content');
    }
}

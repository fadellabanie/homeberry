<?php

namespace App\Http\Livewire;

use App\Models\Notification;
use Livewire\Component;

class NotificationsAdd extends Component
{

    public $title;
    public $content;
    public $topic;
    public $successMessage;

    protected $rules = [
        'title' => 'required|min:2',
        'content' => 'required|min:2',
        'topic' => 'required|min:2',

    ];
    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function submitForm()
    {
        $validatedDate = $this->validate();
        sleep(1);
        Notification::create($validatedDate);

        $this->successMessage = 'successfully ';

        $this->resetForm();

        $this->dispatchBrowserEvent(
            'alert',
            [
                'type' => 'success',
                'title' => 'Created',
                'message' => 'successfully Saved',
                'footer' => ''
            ]
        );
    }

    private function resetForm()
    {
        $this->name  = '';
        $this->content = '';
        $this->topic = '';
    }


    public function render()
    {
        return view('livewire.notifications-add')
        ->extends('admin.layouts.app')
        ->section('content');
    }
}

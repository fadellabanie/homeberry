<?php

namespace App\Http\Livewire;

use App\Models\ActivityType;
use App\Models\City;
use App\Models\Devolper;
use Livewire\Component;
use Livewire\WithFileUploads;

class DevelopersAdd extends Component
{
    use WithFileUploads;

    public $name;
    public $description;
    public $logo;
    public $email;
    public $mobile;
    public $whatsapp_mobile;

    public $successMessage;
    protected $rules = [
        'name' => 'required|min:2|max:100',
        'description' => 'required|min:10',
        'email' => 'required|email',
        'mobile' => 'required',
        'whatsapp_mobile' => 'required',
        'logo' => 'image|max:1024',
    ];


    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function submit()
    {
        $validatedDate = $this->validate();
        sleep(1);

        if ($this->logo){
            $path =  $this->logo->store('uploads/developers');
            $validatedDate['logo'] = 'storage/app/'.$path;
        }

        $developer = Devolper::create($validatedDate);




        $this->successMessage = 'successfully ';

        $this->resetForm();

        $this->dispatchBrowserEvent(
            'alert',
            ['type' => 'success',
            'title' => 'Create developer' ,
            'message' => 'developer successfully Saved',
            'footer' => '']
        );

        // $this->emit('alert', ['type' => 'success', 'message' => 'developer successfully Created.']);
        sleep(2);

        // return redirect()->to('/admin/developers');
    }


    private function resetForm()
    {
        $this->name = '';
        $this->description = '';
        $this->logo = '';
    }

    public function render()
    {
        return view('livewire.developers-add')->extends('admin.layouts.app')
            ->section('content');
    }
}

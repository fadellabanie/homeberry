<?php

namespace App\Http\Livewire;

use App\Models\ActivityType;
use App\Models\City;
use App\Models\Feature;
use App\Models\PaymentMethod;
use App\Models\Property;
use App\Services\AdminPropertyService;
use Livewire\Component;
use Livewire\WithFileUploads;

class PropertiesAdd extends Component
{
    use WithFileUploads;

    public $city_id;
    public $activity_type_id;
    public $payment_id;
    public $name_ar;
    public $name_en;
    public $description_ar;
    public $description_en;
    public $price;
    public $url;
    public $status;
    public $priority;

    public $availability;
    public $guest;
    public $room;
    public $bathroom;
    public $area;
    public $park;
    public $rental;
    public $furnished;
    public $type;
    public $location_name;
    public $youtube;
   
    public $images = [];
    public $amenities;
    public $services;
    public $successMessage;
    protected $rules = [
        'name_ar' => 'required|string|min:2|max:100',
        'name_en' => 'required|string|min:2|max:100',
        'city_id' => 'required',
        'activity_type_id' => 'required',
        'description_ar' => 'required',
        'description_en' => 'required',
        'price' => 'required',
        'url' => '',
        'guest' => 'required',
       
        'room' => 'required',
        'bathroom' => 'required',
        'area' => 'required',
        'park' => 'required',
        'rental' => '',
        'availability' => '',
        'furnished' => 'required',
        'type' => 'required',
        'amenities' => 'required',
        'services' => 'required',
        'payment_id' => 'required',

        'location_name' => '',
        'youtube' => '',

        // 'lat' => ['regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
        // 'lng' => ['regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],

        'images' => 'array',
        'images.*' => 'image|max:2024',

    ];
    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function submitForm()
    {
        // dd($this);

        $validatedDate = $this->validate();
      

        // // $images = md5($this->images . microtime()).'.'.$this->images->extension();
        // unset($validatedDate['images']);
     
        $response = AdminPropertyService::create($validatedDate);

        if ($response['success'] == false){


             return $this->dispatchBrowserEvent(
                'alert',
                [
                    'type' => 'error',
                    'title' => 'Error',
                    'message' => " Process Didn't Compeleted " ,
                    'footer' => '',
                    'color' => '#f27474'
                ]
            );
        }

        if ($this->images) {
            foreach ($this->images as $image) {
                $path = $image->store('uploads/properties');
                $path = 'storage/app/' . $path;
                storeMediaPath($path, $response['property_id'], 'App\Models\Property');
            }
        }

        $this->successMessage = 'successfully ';

        $this->resetForm();

        // $this->dispatchBrowserEvent(
        //     'alert',
        //     [
        //         'type' => 'success',
        //         'title' => 'Created',
        //         'message' => 'successfully Saved',
        //         'footer' => '',
        //         'color' => "#64c82d",
        //     ]
        // );
        sleep(1);
        return redirect()->to('/homeberry-portal/add-loaction-property');
    }

    private function resetForm()
    {
        $this->name_ar = '';
        $this->name_en = '';

    }

    public function render()
    {
        return view('livewire.properties-add',[
            'activity_types' => ActivityType::all(),
            'payments' => PaymentMethod::all(),
            'cities' => City::all(),
            'amenitiesArr'=> Feature::where('type', 'amenities')->get(),
            'servicesArr' => Feature::where('type', 'nearby services')->get(),
        ])->extends('admin.layouts.app')
        ->section('content');
    }
}

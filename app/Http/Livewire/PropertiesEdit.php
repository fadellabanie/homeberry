<?php

namespace App\Http\Livewire;

use App\Models\ActivityType;
use App\Models\City;
use App\Models\Feature;
use App\Models\Media;
use App\Models\PaymentMethod;
use App\Models\Property;
use App\Services\AdminPropertyService;
use App\Services\PropertyService;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithFileUploads;

class PropertiesEdit extends Component
{
    use WithFileUploads;

    public $property;
    public $propertyAttribute;

    public $city_id;
    public $activity_type_id;
    public $payment_id;
    public $name_ar;
    public $name_en;
    public $description_ar;
    public $description_en;
    public $price;
    public $status;
    public $priority;
    public $availability;
    public $guest;
    public $room;
    public $bathroom;
    public $area;
    public $park;
    public $rental;
    public $furnished;
    public $type;
    public $lat;
    public $lng;
    public $images = [];
    public $lastimages = [];
    public $lastimages2 = [];
    public $url;

    public $amenities;
    public $services;

    public $lastamenities;
    public $lastservices;
    public $successMessage;

    protected $rules = [
        'name_ar' => 'required|string|min:2|max:100',
        'name_en' => 'required|string|min:2|max:100',
        'city_id' => 'required',
        'activity_type_id' => 'required',
        'description_ar' => 'required',
        'description_en' => 'required',
        'price' => 'required',
        //'url' => 'required',
        'guest' => 'required',
        'room' => 'required',
        'bathroom' => 'required',
        'area' => 'required',
        'park' => 'required',
        'rental' => '',
        'availability' => '',
        'furnished' => 'required',
        'type' => 'required',
        'amenities' => 'required',
        'services' => 'required',

        'lat' => ['regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
        'lng' => ['regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],

        'images' => 'array',
        'images.*' => 'image|max:2024',
    ];

    /**
     * mount or construct function
     */
    public function mount(Property $property)
    {
        $this->property = null;
        if ($property) {
            $this->property = $property;
            $this->propertyAttribute = $property->propertyAttribute;

            $this->city_id = $this->property->city_id;
            $this->activity_type_id = $this->property->activity_type_id;
            $this->payment_id = $this->property->payment_id;
            $this->name_ar = $this->property->name_ar;
            $this->name_en = $this->property->name_en;
            $this->description_ar  = $this->property->description_ar;
            $this->description_en = $this->property->description_en;
            $this->price = $this->property->price;


            $this->status  = $this->property->status;
            $this->priority  = $this->property->priority;

            $this->availability = $this->propertyAttribute->availability;
            $this->guest = $this->propertyAttribute->guest;
            $this->room = $this->propertyAttribute->room;
            $this->bathroom = $this->propertyAttribute->bathroom;
            $this->area = $this->propertyAttribute->area;
            $this->park = $this->propertyAttribute->park;
            $this->rental = $this->propertyAttribute->rental;
            $this->furnished = $this->propertyAttribute->furnished;
            $this->type = $this->propertyAttribute->type;
            $this->lat = $this->propertyAttribute->lat;
            $this->lng = $this->propertyAttribute->lng;
            $this->url = $this->propertyAttribute->url;

            $this->lastamenities = $this->property->amenities()->pluck('id')->toArray();
            $this->lastservices =  $this->property->nearbyServices()->pluck('id')->toArray();

            $this->amenities = $this->lastamenities;
            $this->services = $this->lastservices;

            $this->lastimages2 = Media::where('mediable_type', 'App\Models\property')->where('mediable_id', $this->property->id)->get();
            $this->lastimages = $this->lastimages2;
        }
    }

    public function updated($propertyName)
    {
        if ($propertyName == "images") {
            $this->lastimages  = "";
        }
        $this->validateOnly($propertyName);
    }

    public function submitForm()
    {
        // dd($this);

        $validatedDate = $this->validate();
        sleep(1);

        // // $images = md5($this->images . microtime()).'.'.$this->images->extension();
        // unset($validatedDate['images']);

        $response = AdminPropertyService::update($validatedDate , $this->property);

        if ($response['success'] == false) {
            return $this->dispatchBrowserEvent(
                'alert',
                [
                    'type' => 'error',
                    'title' => 'Error',
                    'message' => " Process Didn't Compeleted ",
                    'footer' => '',
                    'color' => '#f27474'
                ]
            );
        }


        if ($this->images) {
            foreach ($this->lastimages2 as $image) {
                $image->delete();
            }

            foreach ($this->images as $image) {
                $path = $image->store('uploads/properties');
                $path = 'storage/app/' . $path;
                storeMediaPath($path, $this->property->id, 'App\Models\Property');
            }
        }

        // City::create($validatedDate);



        $this->successMessage = 'successfully ';

        $this->resetForm();

        $this->dispatchBrowserEvent(
            'alert',
            [
                'type' => 'success',
                'title' => 'Updated',
                'message' => 'successfully Saved',
                'footer' => '',
                'color' => "#64c82d",
            ]
        );
    }

     private function resetForm()
    {
        $this->name_ar = '';
        $this->name_en = '';
    }

    public function edit($id)
    {
        $record = Property::findOrFail($id);
        $this->name_ar = $record->name_ar;
        $this->name_en = $record->name_en;
        $this->status = $record->status;
    }

    public function render()
    {
        return view('livewire.properties-edit',[
            'activity_types' => ActivityType::all(),
            'payments' => PaymentMethod::all(),
            'cities' => City::all(),
            'amenitiesArr' => Feature::where('type', 'amenities')->get(),
            'servicesArr' => Feature::where('type', 'nearby services')->get(),
        ])->extends('admin.layouts.app')
        ->section('content');
    }
}

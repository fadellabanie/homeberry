<?php

namespace App\Http\Livewire;

use App\Models\City;
use App\Models\Governorate;
use Livewire\Component;
use Livewire\WithFileUploads;

class CitiesEdit extends Component
{

    use WithFileUploads;

    public $name_ar;

    public $city;
    public $name_en;
    public $status;
    public $governorate_id;
    public $priority;
    public $image;
    public $lastimage;

    public $successMessage;
    protected $rules = [
        'name_ar' => 'required|string|min:2|max:100',
        'name_en' => 'required|string|min:2|max:100',
        'status' => 'required',
        'priority' => 'required',
        
    ];
      /**
     * mount or construct function
     */
    public function mount(City $city)
    {
        $this->city = null;
        if($city) {
            $this->city = $city;
            $this->governorate_id   = $this->city->governorate_id;
            $this->name_ar    = $this->city->name_ar;
            $this->name_en  = $this->city->name_en;
            $this->status  = $this->city->status;
            $this->priority  = $this->city->priority;
            $this->lastimage  = $this->city->image;
        }

    }


    public function updated($propertyName)
    {
        if ($propertyName == "image"){
            $this->lastimage  = "";
        }
        $this->validateOnly($propertyName);
    }

    public function submitForm()
    {
        $validatedDate = $this->validate();
        sleep(1);

        if ($this->image != null) {
            $this->validate([
                'image' => 'image|max:2024',
            ]);
            $path =  $this->image->store('uploads/cities');
            $validatedDate['image'] = 'storage/app/' . $path;
        }else{
            unset($validatedDate['image']);
        }
        // $image = md5($this->image . microtime()).'.'.$this->image->extension();

        // $path = $this->image->store('uploads/cities');
        // City::create($validatedDate);

        $this->city->update($validatedDate);

        $this->successMessage = 'successfully ';


        $this->resetForm();

        $this->dispatchBrowserEvent(
            'alert',
            [
                'type' => 'success',
                'title' => 'Updated',
                'message' => 'successfully Saved',
                'footer' => ''
            ]
        );
    }

    private function resetForm()
    {
        $this->name_ar = '';
        $this->name_en = '';

    }
    public function render()
    {
        return view('livewire.cities-edit',[
            'governorates' => Governorate::all(),
        ])->extends('admin.layouts.app')
        ->section('content');
    }
}

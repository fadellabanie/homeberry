<?php

namespace App\Http\Livewire;

use App\Models\Governorate;
use Livewire\Component;
use RealRashid\SweetAlert\Facades\Alert;

class GovernoratesEdit extends Component
{
    public $governorate;
    public $name_ar;
    public $name_en;
    public $status;
    public $successMessage;
    protected $rules = [
        'name_ar' => 'required|min:3|max:50',
        'name_en' => 'required|min:3|max:50',
        'status' => 'required',
    ];

        /**
     * mount or construct function
     */
    public function mount(Governorate $governorate)
    {
        $this->governorate = null;
        if($governorate) {
            $this->governorate = $governorate;
            $this->name_ar    = $this->governorate->name_ar;
            $this->name_en  = $this->governorate->name_en;
            $this->status  = $this->governorate->status;
        }

    }

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function submitForm()
    {
        $validatedDate = $this->validate();
        sleep(1);
        
        $this->governorate->update($validatedDate);

        $this->successMessage = 'successfully ';

        $this->resetForm();

        $this->dispatchBrowserEvent(
            'alert',
            [
                'type' => 'success',
                'title' => 'updated',
                'message' => 'successfully Saved',
                'footer' => ''
            ]
        );
        sleep(1);
        
       // Alert::success('Edit', 'Success Edit Governorates');
    }

    private function resetForm()
    {
        $this->name_ar = '';
        $this->name_en = '';

    }

    public function edit($id)
    {
        $record = Governorate::findOrFail($id);
        $this->name_ar = $record->name_ar;
        $this->name_en = $record->name_en;
        $this->status = $record->status;
    }
    public function render()
    {
        return view('livewire.governorates-edit')
        ->extends('admin.layouts.app')
        ->section('content');
    }
}

<?php

namespace App\Http\Livewire;

use App\Models\Package;
use Livewire\Component;

class PackagesEdit extends Component
{

    public $package;
    public $name_ar;
    public $name_en;
    public $description_ar;
    public $description_en;
    public $price;
    public $count;
    public $type;
    public $successMessage ;

    protected $rules = [
        'type' => 'required',
        'name_ar' => 'required|min:3|max:150',
        'name_en' => 'required|min:3|max:150',
        'description_ar' => 'required|min:3',
        'description_en' => 'required|min:3',
        'price' => 'required|gt:0',
        'count' => 'gt:0',
    ];
  /**
     * mount or construct function
     */
    public function mount(Package $package)
    {
        $this->developer = null;
        if ($package) {
            $this->package = $package;
            $this->type = $this->package->type;
            $this->name_ar = $this->package->name_ar;
            $this->name_en = $this->package->name_en;
            $this->description_ar  = $this->package->description_ar;
            $this->description_en  = $this->package->description_en;
            $this->price =  $this->package->price;
            $this->count =  $this->package->count;
        }
    }

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function submit()
    {
        $validatedDate = $this->validate();
        sleep(1);

        $this->package->update($validatedDate);

        $this->successMessage = 'successfully ';

        $this->resetForm();

        $this->dispatchBrowserEvent(
            'alert',
            [
                'type' => 'success',
                'title' => 'updated Package',
                'message' => 'Package successfully updated',
                'footer' => ''
            ]
        );
      
    }
    private function resetForm()
    {
        $this->type = '';
        $this->name_ar = '';
        $this->name_en = '';
        $this->description_ar = '';
        $this->description_en = '';
        $this->price = '';
        $this->count = '';
    }
    public function edit($id)
    {
        $record = Package::findOrFail($id);
        $this->type = $record->type;
        $this->name_ar = $record->name_ar;
        $this->name_en = $record->name_en;
        $this->description_ar  = $record->description_ar;
        $this->description_en  = $record->description_en;
        $this->price = $record->price;
        $this->count = $record->count;
    }
    public function render()
    {
        return view('livewire.packages-edit')->extends('admin.layouts.app')
        ->section('content');
    }
}

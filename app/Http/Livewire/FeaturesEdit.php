<?php

namespace App\Http\Livewire;

use App\Models\Feature;
use Livewire\Component;
use Livewire\WithFileUploads;

class FeaturesEdit extends Component
{
    use WithFileUploads;

    public $feature;
    public $name_ar;
    public $name_en;
    public $type;
    public $icon;
    public $lasticon;

    public $successMessage;
    protected $rules = [
        'name_ar' => 'required|min:2',
        'name_en' => 'required|min:2',
        'type' =>  'required|in:nearby services,amenities',
    ];

    /**
     * mount or construct function
     */
    public function mount(Feature $feature)
    {
        $this->feature = null;
        if ($feature) {
            $this->feature = $feature;
            $this->type   = $this->feature->type;
            $this->name_ar    = $this->feature->name_ar;
            $this->name_en  = $this->feature->name_en;
            $this->lasticon = $this->feature->icon;
        }
    }

    public function updated($propertyName)
    {
        if ($propertyName == "icon") {
            $this->lasticon  = "";
        }
        $this->validateOnly($propertyName);
    }

    public function submitForm()
    {
        $validatedDate = $this->validate();
        sleep(1);

        if ($this->icon != null) {
            $this->validate([
                'icon' => 'image|max:2024',
            ]);
            $path = $this->icon->store('uploads/features');
            $validatedDate['icon'] = 'storage/app/' . $path;
        } else {
            unset($validatedDate['icon']);
        }

        // dd($validatedDate);
        $this->feature->update($validatedDate);

        $this->successMessage = 'successfully ';

        $this->resetForm();

        $this->dispatchBrowserEvent(
            'alert',
            [
                'type' => 'success',
                'title' => 'updated',
                'message' => 'successfully Saved',
                'footer' => ''
            ]
        );
    }

    private function resetForm()
    {
        $this->name_ar = '';
        $this->name_en = '';
    }

    public function edit($id)
    {
        $record = Feature::findOrFail($id);
        $this->name_ar = $record->name_ar;
        $this->name_en = $record->name_en;
        $this->activity_id = $record->activity_id;
        $this->status = $record->status;
    }
    public function render()
    {

        return view('livewire.features-edit')
            ->extends('admin.layouts.app')
            ->section('content');
    }

}

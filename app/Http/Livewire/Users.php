<?php

namespace App\Http\Livewire;

use App\Http\Controllers\GeneralController;
use App\User;
use Livewire\Component;
use Livewire\WithPagination;

class Users extends Component
{   
     use WithPagination;

    //protected $queryString = ['search'];
   
    public $search;
    public $sortField;
    public $sortAsc = true;
    protected $queryString = ['search', 'sortAsc', 'sortField'];

    public function paginationView()
    {
        return 'custom-pagination-links-view';
    }
    
    public function sortBy($field)
    {
        if ($this->sortField === $field) {
            $this->sortAsc = !$this->sortAsc;
        } else {
            $this->sortAsc = true;
        }

        $this->sortField = $field;
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }
   
    public function render()
    {
        return view('livewire.users',[
            'users' => User::where(function ($query) {
                $query->where('name', 'like', '%' . $this->search . '%')
                    ->orWhere('email', 'like', '%' . $this->search . '%')
                    ->orWhere('mobile', 'like', '%' . $this->search . '%');
                    
            })
            ->orderBy('id', 'desc')
            ->paginate(GeneralController::PAGINATE_NUMBER_LARGE_CARD),
        ])->extends('admin.layouts.app')
        ->section('content');
    }
}

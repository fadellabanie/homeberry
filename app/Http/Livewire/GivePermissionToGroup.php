<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class GivePermissionToGroup extends Component
{
    
    public $groups_id;
    public $permissions;

    public $successMessage;
    protected $rules = [
        'groups_id' => 'required',
        'permissions' => 'required',
    ];

    public function submit()
    {
        $validatedDate = $this->validate();

       // dd($validatedDate);

        $role = Role::find($validatedDate['groups_id']);

        $role->syncPermissions($validatedDate['permissions']);


        $this->successMessage = 'successfully ';

        $this->resetForm();

        $this->dispatchBrowserEvent(
            'alert',
            [
                'type' => 'success',
                'title' => 'Created',
                'message' => 'successfully Saved',
                'footer' => ''
            ]
        );
        sleep(1);

    }
    private function resetForm()
    {
        $this->permissions = '';
        $this->groups_id = '';

    }
    public function render()
    {
        
        return view('livewire.give-permission-to-group',[
            'permissionsArr' => Permission::all(),
            'groups' => Role::with('permissions')->get(),
        ])
        ->extends('admin.layouts.app')
        ->section('content');
    }
}

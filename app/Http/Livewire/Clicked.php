<?php

namespace App\Http\Livewire;

use App\Http\Controllers\GeneralController;
use App\Models\Property;
use Livewire\Component;
use Livewire\WithPagination;

class Clicked extends Component
{
    use WithPagination;

    //protected $queryString = ['search'];
    public $active = false;
    public $priority = false;
    public $search;
    public $sortField;
    public $sortAsc = true;
    protected $queryString = ['search', 'active', 'priority', 'sortAsc', 'sortField'];

    public function mount(): void
    {
        $this->search = request()->query('search', $this->search);
    }

    public function paginationView()
    {
        return 'custom-pagination-links-view';
    }
    public function sortBy($field)
    {
        if ($this->sortField === $field) {
            $this->sortAsc = !$this->sortAsc;
        } else {
            $this->sortAsc = true;
        }

        $this->sortField = $field;
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        return view('livewire.clicked',[
            'properties' => Property::when($this->sortField, function ($query) {
                $query->orderBy($this->sortField, $this->sortAsc ? 'asc' : 'desc');
            })
            ->where('status', $this->active)->paginate(GeneralController::PAGINATE_NUMBER_LARGE_PAGE),
        ])
        ->extends('admin.layouts.app')
        ->section('content');
    }
}

<?php

namespace App\Http\Livewire;

use App\User;
use App\Http\Controllers\GeneralController;
use App\Models\PhotoSession;
use App\Models\Project;
use App\Models\Property;
use App\Models\Reserve;
use App\Models\Schedule;
use Livewire\Component;

class Home extends Component
{
    public function render()
    {
        return view('livewire.home',[
           
            'lastUsersRegister' => User::with('UserToken')->orderBy('id', 'desc')
            ->take(GeneralController::NUMBER_OF_TAKE)->get(),
            'countOfUserActive' =>User::Active()->count(),
            'countOfNotUserActive' =>User::NotActive()->count(),
            'totalProperties' => Property::count(),
            'schedules' => Schedule::count(),
            'bookings' => Reserve::count(),
            'photoSessions' => PhotoSession::count(),
            'lastProperties' => Property::orderBy('id', 'desc')
            ->take(GeneralController::NUMBER_OF_TAKE)->get(),
            'totalProjects' => Project::count(),
        ])
        ->extends('admin.layouts.app')
        ->section('content');
    }
}

<?php

namespace App\Http\Livewire;

use App\Models\Governorate;
use Livewire\Component;
use RealRashid\SweetAlert\Facades\Alert;

class GovernoratesAdd extends Component
{
    public $name_ar;
    public $name_en;
    public $status;
    public $successMessage;
    protected $rules = [
        'name_ar' => 'required|min:3|max:50',
        'name_en' => 'required|min:3|max:50',
        'status' => 'required',
    ];
    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function submitForm()
    {
        $validatedDate = $this->validate();
        
        Governorate::create($validatedDate);

        $this->successMessage = 'successfully ';

        $this->resetForm();

        $this->dispatchBrowserEvent(
            'alert',
            [
                'type' => 'success',
                'title' => 'Created',
                'message' => 'successfully Saved',
                'footer' => ''
            ]
        );
        sleep(1);
       // Alert::success('Create', 'Success Add Governorates');
        //return redirect()->to('/homeberry-portal/governorates');

    }

    private function resetForm()
    {
        $this->name_ar = '';
        $this->name_en = '';

    }

    public function render()
    {
        return view('livewire.governorates-add')->extends('admin.layouts.app')
        ->section('content');
    }
}

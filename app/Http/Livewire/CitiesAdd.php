<?php

namespace App\Http\Livewire;

use App\Models\City;
use App\Models\Governorate;
use Livewire\Component;
use Livewire\WithFileUploads;

class CitiesAdd extends Component
{
    use WithFileUploads;

    public $name_ar;
    public $name_en;
    public $status;
    public $governorate_id;
    public $priority;
    public $image;

    public $successMessage;
    protected $rules = [
        'name_ar' => 'required|min:2|max:100',
        'name_en' => 'required|min:2|max:100',
        'status' => 'required',
        'priority' => 'required',
        'governorate_id' => 'required',
       'image' => 'image|max:1024',
       
       
    ];
    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function submit()
    {
        $validatedDate = $this->validate();
        sleep(1);

        // $image = md5($this->image . microtime()).'.'.$this->image->extension();

        $path = $this->image->store('uploads/cities');

        // City::create($validatedDate);
         City::create([
            'governorate_id' => $this->governorate_id,
            'name_ar' => $this->name_ar,
            'name_en' => $this->name_en,
            'status' => $this->status,
            'priority' =>$this->priority,
            'image' => 'storage/app/' . $path,

         ]);

        $this->successMessage = 'successfully ';

        $this->resetForm();

        $this->dispatchBrowserEvent(
            'alert',
            [
                'type' => 'success',
                'title' => 'Created',
                'message' => 'successfully Saved',
                'footer' => ''
            ]
        );

    }

    private function resetForm()
    {
        $this->name_ar = '';
        $this->name_en = '';

    }

    public function render()
    {
        return view('livewire.cities-add',[
            'governorates' => Governorate::all(),
        ])->extends('admin.layouts.app')
        ->section('content');
    }
}

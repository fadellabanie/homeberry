<?php

namespace App\Http\Livewire;

use App\Http\Controllers\GeneralController;
use App\Models\Feature;
use Livewire\Component;
use Livewire\WithPagination;

class Features extends Component
{
    use WithPagination;

    //protected $queryString = ['search'];
    public $search;
    public $sortField;
    public $sortAsc = true;
    protected $queryString = ['search', 'sortAsc', 'sortField'];

    public function paginationView()
    {
        return 'custom-pagination-links-view';
    }
    public function sortBy($field)
    {
        if ($this->sortField === $field) {
            $this->sortAsc = !$this->sortAsc;
        } else {
            $this->sortAsc = true;
        }

        $this->sortField = $field;
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        return view('livewire.features',[
            'features' => Feature::where(function ($query) {
                $query->where('name_ar', 'like', '%' . $this->search . '%')
                    ->orWhere('name_en', 'like', '%' . $this->search . '%');

            })->orderby('id', 'desc')->paginate(GeneralController::PAGINATE_NUMBER_LARGE_PAGE),
        ])->extends('admin.layouts.app')
        ->section('content');
    }
}

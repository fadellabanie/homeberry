<?php

namespace App\Http\Livewire;

use App\Http\Controllers\GeneralController;
use App\Models\Project;
use Livewire\Component;
use Livewire\WithPagination;

class Projects extends Component
{
    use WithPagination;

    //protected $queryString = ['search'];
    public $active = true;
   
    public $priority = true;
    public $search;
    public $sortField;
    public $sortAsc = true;
    protected $queryString = ['search', 'active', 'priority', 'sortAsc', 'sortField'];

    public function mount(): void
    {
        $this->search = request()->query('search', $this->search);
    }

    public function paginationView()
    {
        return 'custom-pagination-links-view';
    }
    public function sortBy($field)
    {
        if ($this->sortField === $field) {
            $this->sortAsc = !$this->sortAsc;
        } else {
            $this->sortAsc = true;
        }

        $this->sortField = $field;
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }
    public function destroy($id)
    {
        if ($id) {
            $record = Project::whereId($id);
            $record->delete();
        }
     // $this->emit('alert',['type' => 'success','message'=>'Successfully Deleted']);
    }
    public function render()
    {
        return view('livewire.projects', [
            'projects' => Project::where(function ($query) {
                    $query->where('name_ar', 'like', '%' . $this->search . '%')
                        ->orWhere('name_en', 'like', '%' . $this->search . '%');
                    })->where('status', $this->active)
                    ->where('priority', $this->priority)
                    ->orderBy('id', 'desc')
                    ->paginate(GeneralController::PAGINATE_NUMBER_LARGE_CARD),
                   
            'rowsName'  => [
                'NAME ARABIC'=> 'name_ar',
                'NAME ENGLISH'=>'name_en',
                'PRICE' => 'price',
                'FINISHED'=>'finished',
                'DELIVERY' => 'delivery',
                'VIEWS'=>'VIEWS'],
            ]
            )
            ->extends('admin.layouts.app')
            ->section('content');

    }
}

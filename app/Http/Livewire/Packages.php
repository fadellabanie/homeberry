<?php

namespace App\Http\Livewire;

use App\Models\Package;
use Livewire\Component;

class Packages extends Component
{
    public function destroy($id)
    {
        if ($id) {
            $record = Package::whereId($id);
            $record->delete();
        }
     // $this->emitSelf('alert',['type' => 'success','message'=>'Successfully Deleted']);
    }
    public function render()
    {
        return view('livewire.packages',[
            'packages' => Package::whereType('subscription')->orderBy('id', 'desc')->get(),
        ])
        ->extends('admin.layouts.app')
        ->section('content');
    }
}

<?php

namespace App\Http\Livewire;

use App\Http\Controllers\GeneralController;

use App\Models\Devolper;
use Livewire\Component;
use Livewire\WithPagination;

class Developers extends Component
{
    use WithPagination;

    //protected $queryString = ['search'];
    public $active = true;
    public $priority = false;
    public $search;
    public $sortField;
    public $sortAsc = true;
    protected $queryString = ['search', 'active', 'priority', 'sortAsc', 'sortField'];

    public function mount(): void
    {
        $this->search = request()->query('search', $this->search);
    }

    public function paginationView()
    {
        return 'custom-pagination-links-view';
    }
    public function sortBy($field)
    {
        if ($this->sortField === $field) {
            $this->sortAsc = !$this->sortAsc;
        } else {
            $this->sortAsc = true;
        }

        $this->sortField = $field;
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }
    public function destroy($id)
    {
        if ($id) {
            $record = Devolper::whereId($id);
            $record->delete();
        }
     // $this->emit('alert',['type' => 'success','message'=>'Successfully Deleted']);
    }
    public function render()
    {
        return view('livewire.developers', [
            'developers' => Devolper::where(function ($query) {
                $query->where('name', 'like', '%' . $this->search . '%');
                })
                ->orderBy('id', 'desc')
                ->paginate(GeneralController::PAGINATE_NUMBER_LARGE_CARD),
            'rowsName'  => [
                'lOGO'=>'logo',
                'NAME'=> 'name',
                'DESCRIPTION' => 'description'
                ],
            ]
            )
            ->extends('admin.layouts.app')
            ->section('content');

    }
}

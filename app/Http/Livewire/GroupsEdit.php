<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Spatie\Permission\Models\Role;

class GroupsEdit extends Component
{
    
    public $role;
    public $name;
   

    public $successMessage;
    protected $rules = [
        'name' => 'required|string|min:2|max:100',
       
    ];
      /**
     * mount or construct function
     */
    public function mount(Role $role)
    {
        $this->role = null;
        if($role) {
            $this->role = $role;
            $this->name  = $this->role->name;
        }

    }
    public function submitForm()
    {
        $validatedDate = $this->validate();
        sleep(1);

        $this->role->update($validatedDate);

        $this->successMessage = 'successfully ';

        $this->resetForm();

        $this->dispatchBrowserEvent(
            'alert',
            [
                'type' => 'success',
                'title' => 'Updated',
                'message' => 'successfully Saved',
                'footer' => ''
            ]
        );
    }

    private function resetForm()
    {
        $this->name = '';
      
    }
    public function render()
    {
        return view('livewire.groups-edit')->extends('admin.layouts.app')
        ->section('content');
    }
}

<?php

namespace App\Http\Livewire;

use App\Models\ActivityType;
use App\Models\City;
use App\Models\Devolper;
use App\Models\FurnishedType;
use App\Models\Media;
use App\Models\PaymentMethod;
use App\Models\Project;
use App\Models\ProjectDetail;
use Livewire\Component;
use Livewire\WithFileUploads;

class ProjectsEdit extends Component
{
    use WithFileUploads;

    public $project;

    public $name_ar;
    public $name_en;
    public $description_ar;
    public $description_en;
    public $devolper_id;
    public $city_id;
    public $payment_id; //

    public $activitytypes = [];
    public $furnishedtypes = [];
    public $finished;
    public $delivery;
    public $link;
    public $youtube; //
    //public $price;
    public $is_constructed; //
    public $status;
    public $priority;
    public $images = [];

    public $location; //

    public $key, $value;
    public $updateMode = false;
    public $inputs = [0 => ''];
    public $i = 1;

    public $lastkey;
    public $lastimages = [];
    public $lastimages2 = [];

    public $lastactivitytypes  = [];
    public $lastfurnishedtypes = [];

    public $successMessage;
    protected $rules = [
        'name_ar' => 'required|min:3|max:150',
        'name_en' => 'required|min:3|max:150',
        'status' => 'required',
        'priority' => 'required',

        'description_ar' => 'required|min:3',
        'description_en' => 'required|min:3',
        'devolper_id' => 'required',
        'city_id' => 'required',

        //'price' => 'required|gt:0',
        'finished' => 'required',
        'delivery' => 'required',
        'link' => '',
        'images' => 'array',
        'images.*' => 'image|max:1024',

    ];

    /**
     * mount or construct function
     */
    public function mount(project $project)
    {
        $this->project = null;
        if ($project) {
            $this->project = $project;
            $this->name_ar = $this->project->name_ar;
            $this->name_en = $this->project->name_en;

            $this->description_ar  = $this->project->description_ar;
            $this->description_en = $this->project->description_en;
            $this->devolper_id = $this->project->devolper_id;
            $this->city_id = $this->project->city_id;
            $this->finished = $this->project->finished;
            $this->delivery = $this->project->delivery;
            $this->link = $this->project->link;
            //$this->price = $this->project->price;

            $this->status  = $this->project->status;
            $this->priority  = $this->project->priority;

            $this->lastimages2 = Media::where('mediable_type', 'App\Models\Project')->where('mediable_id', $this->project->id)->get();
            $this->lastimages = $this->lastimages2;

            $this->lastactivitytypes =  $this->project->activitytypes->pluck('id')->toArray();
            $this->activitytypes =  $this->lastactivitytypes;

            $this->lastfurnishedtypes =  $this->project->furnishedtypes->pluck('id')->toArray();
            $this->furnishedtypes =  $this->lastfurnishedtypes;

            $this->payment_id = $this->project->payment_id;
            $this->youtube = $this->project->youtube;
            $this->location = $this->project->location;
            $this->lastkey = $this->project->details()->pluck('key')->toArray();
            $this->inputs = $this->lastkey;
            $this->key = $this->lastkey;
             $this->value = $this->project->details()->pluck('value')->toArray();
            $this->is_constructed = $this->project->is_constructed;
        }
    }

    public function add($i)
    {
        $i = $i + 1;
        $this->i = $i;
        array_push($this->inputs, $i);
    }

    public function remove($i)
    {
        unset($this->inputs[$i]);
    }

    public function updated($propertyName)
    {
        if ($propertyName == "images") {
            $this->lastimages  = "";
        }
        $this->validateOnly($propertyName);
    }

    public function submit()
    {
        $validatedDate = $this->validate();
        sleep(1);

        $validatedDate = array_diff_key($validatedDate, ['images' => 0]);

        $this->project->update($validatedDate);

        if(!empty($this->lastactivitytypes )){
            $this->project->activitytypes()->detach($this->lastactivitytypes);
            $this->project->activitytypes()->attach($validatedDate['activitytypes']);
        }

        if (!empty($this->lastfurnishedtypes)) {
            $this->project->furnishedtypes()->detach($this->lastfurnishedtypes);
            $this->project->furnishedtypes()->attach($validatedDate['furnishedtypes']);
        }

        if($this->lastkey){
            $this->project->details()->delete();

              foreach ($this->key as $keyforeach => $value) {
                ProjectDetail::create([
                    'key' => $this->key[$keyforeach],
                    'value' => $this->value[$keyforeach],
                    'project_id' => $this->project->id
                ]);
            }
        }else{
            foreach ($this->key as $key => $value) {
                ProjectDetail::create([
                    'key' => $this->key[$key],
                    'value' => $this->value[$key],
                    'project_id' => $this->project->id
                ]);
            }
        }

        if ($this->images) {
            foreach ($this->lastimages2 as $image) {
                $image->delete();
            }

            foreach ($this->images as $image) {
                $path = $image->store('uploads/projects');
                $path = 'storage/app/' . $path;
                storeMediaPath($path,$this->project->id, 'App\Models\Project');
            }
        }
        $this->successMessage = 'successfully ';

        $this->resetForm();

        $this->dispatchBrowserEvent(
            'alert',
            [
                'type' => 'success',
                'title' => 'updated Project',
                'message' => 'Project successfully updated',
                'footer' => ''
            ]
        );

        // return redirect()->to('/admin/projects');
    }

    private function resetForm()
    {
        $this->name_ar = '';
        $this->name_en = '';
    }

    public function edit($id)
    {
        $record = Project::findOrFail($id);
        $this->name_ar = $record->name_ar;
        $this->name_en = $record->name_en;
        $this->status = $record->status;
    }
    public function render()
    {
        return view('livewire.projects-edit', [
            'payments' => PaymentMethod::all(),
            'developers' => Devolper::all(),
            'cities' => City::all(),
            'activitytypeArr' => ActivityType::all(),
            'furnishedtypeArr' => FurnishedType::all(),
        ])
            ->extends('admin.layouts.app')
            ->section('content');
    }
}

<?php

namespace App\Models;

use App\Support\Translatable;
use Illuminate\Database\Eloquent\Model;

class ActivityType extends Model
{
    use Translatable;

    protected $translatedAttributes = ['name'];

    protected $fillable = ['activity_id', 'name_ar', 'icon', 'name_en', 'status'];

    //protected $with = ['activity'];

    #########################
    ## Eloquent Functions ###
    #########################

    public function scopeActive($query)
    {
        return $query->where('status', true);
    }


    /**
     * Get the ActivityType that this Activity belongs to.
     */
    public function activity()
    {
        return $this->belongsTo(Activity::class);
    }

    public function projects()
    {
        return $this->belongsToMany(Project::class, 'activitytype_project');
    }
}

<?php

namespace App\Models;

use App\Support\Translatable;
use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    use Translatable;

    protected $translatedAttributes = ['name'];

    protected $fillable = ['name_ar', 'name_en', 'status'];
    #########################
    ## Eloquent Functions ###
    #########################

    public function scopeActive($query)
    {
        return $query->where('status', true);
    }
    /**
     * Get the ActivityType that this Activity belongs to.
     */
    public function type()
    {
        return $this->hasMany(ActivityType::class);
    }
}

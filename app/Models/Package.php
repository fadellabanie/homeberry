<?php

namespace App\Models;

use App\Support\Translatable;
use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    use Translatable;

    protected $translatedAttributes = ['name', 'description'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    ###################################
    ## Number ==> Days ################
    ## number of Adding Properties ####
    ###################################
    protected $fillable = [
        'type',
        'name_ar',
        'name_en',
        'description_ar',
        'description_en',
        'price',
        'count',
    ];
}

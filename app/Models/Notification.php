<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $fillable = ['title', 'content', 'topic'];

    public function users()
    {
        return $this->belongsToMany(User::class)->withPivot('read_at');
    }
}

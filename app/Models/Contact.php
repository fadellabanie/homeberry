<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $table = 'contacts';
    
    public $timestamps = false;

    protected $guarded = [];

    /**
     *
     */
    public function contactable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
    public function property()
    {
        return $this->hasOne(property::class, 'id', 'contactable_id');
    }
    public function Project()
    {
        return $this->hasOne(Project::class, 'id', 'contactable_id');
    }
}

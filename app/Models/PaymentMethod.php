<?php

namespace App\Models;

use App\Support\Translatable;
use Illuminate\Database\Eloquent\Model;

class PaymentMethod extends Model
{
    use Translatable;

    protected $translatedAttributes = ['name'];
}

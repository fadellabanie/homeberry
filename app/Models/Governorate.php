<?php

namespace App\Models;

use App\Support\Translatable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Governorate extends Model
{
    use Translatable;

    protected $translatedAttributes = ['name'];

    protected $fillable = ['name_ar', 'name_en', 'status'];

    #########################
    ## Eloquent Functions ###
    #########################
    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::addGlobalScope('id', function (Builder $builder) {
            $builder->orderBy('id', 'DESC');
        });
    }
    public function scopeActive($query)
    {
        return $query->where('status', true);
    }

    /**
     * Get the City that this governorate belongs to.
     */
    public function cities()
    {
        return $this->hasMany(City::class);
    }
}

<?php

namespace App\Models;

use App\Models\VerifyUser;
use App\Models\Schedule;
use App\Support\Translatable;
use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Property extends Model
{
    use Translatable;

    protected $translatedAttributes = ['name', 'description'];

    protected $fillable = [
        'user_id',
        'city_id',
        'activity_type_id',
        'payment_id',
        'name_ar',
        'name_en',
        'description_ar',
        'description_en',
        'price',
        //'rent_type',
        'views',
        'clicked',
        'priority',
        'status'
    ];
    protected $with = ['propertyAttribute','amenities','nearbyServices'];
           
    #########################
    ## Eloquent Functions ###
    #########################

    public function scopeActive($query)
    {
        return $query->where('status', true);
    }

    public function scopePriority($query)
    {
        return $query->where('priority', true);
    }

    public function scopePrioritySorted($query)
    {
        return $query->orderBy('priority', 'DESC');
    }
    // User model
    public function scopeAvailableProperties($query)
    {
        $reserves = Reserve::where('property_id', $this->id)->get();
        //dd($reserves);
        return $query->whereNotIn('id', $reserves->property_id);
    }
    /**
     * Get all media of product.
     */
    public function medias()
    {
        return $this->morphMany(Media::class, 'mediable');
    }

    /**
     * Get all favourites of Property.
     */
    public function favourites()
    {
        return $this->belongsToMany(User::class, 'favourites')->withPivot('user_id', 'property_id');
    }
    /**
     * Get all media of product.
     */
    public function schedules()
    {
        return $this->morphMany(Schedule::class, 'schedulable');
    }
    public function viewContact()
    {
        return $this->morphMany(Contact::class, 'contacts');
    }
    /**
     * Get the property_attributes that this property belongs to.
     */
    public function note()
    {
        return $this->hasOne(Note::class);
    }

    /**
     * Get the property_attributes that this property belongs to.
     */
    public function propertyAttribute()
    {
        return $this->hasOne(PropertyAttribute::class);
    }
    /**
     * Get the Property that this city belongs to.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    /**
     * Get the Property that this city belongs to.
     */
    public function city()
    {
        return $this->belongsTo(City::class);
    }

    /**
     * Get the Property that this activityType belongs to.
     */
    public function activityType()
    {
        return $this->belongsTo(ActivityType::class);
    }

    /**
     * Get the Property that this payment belongs to.
     */
    public function paymentMathod()
    {
        return $this->belongsTo(PaymentMethod::class, 'payment_id');
    }

   ## 'nearby services','amenities'
    /**
     * Get the Property that this Amenities belongs To Many.
     */
    public function amenities()
    {
        return $this->belongsToMany(Feature::class, 'amenity_property','property_id','amenity_id')->withPivot('property_id','amenity_id');
    }
     /**
     * Get the Property that this near by Services belongs To Many.
     */
    public function nearbyServices()
    {
        return $this->belongsToMany(Feature::class, 'service_property','property_id','service_id')->withPivot('property_id','service_id');
    }
    /**
     * Get all reserves of properties.
     */
    public function reserves()
    {
        return $this->hasMany(Reserve::class, 'reserves');
    }
}

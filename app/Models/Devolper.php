<?php

namespace App\Models;

use App\Support\Translatable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Devolper extends Model
{
    // use Translatable;

    // protected $translatedAttributes = ['name','description'];

    protected $fillable = ['name','description','logo','email','mobile','whatsapp_mobile'];


    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::addGlobalScope('id', function (Builder $builder) {
            $builder->orderBy('id', 'DESC');
        });
    }
    /**
     * Get the Projects that this Devolper belongs to.
     */
    public function projects()
    {
        return $this->hasMany(Project::class);
    }
}

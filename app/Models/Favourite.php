<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Favourite extends Model
{
    protected  $table = 'favourites';

    public $timestamps = false;

    protected $fillable = ['user_id', 'property_id'];

    // protected $with = ['property','user'];

    /**
     * Get the property that this Favourite belongs to.
     */
    
    public function property()
    {
        return $this->belongsTo(Property::class);
    }

    /**
     * Get the user that this Favourite belongs to.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}

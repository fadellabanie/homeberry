<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'property_id', 'note',
    ];


    /**
     * Get the Property that this Features Note To Many.
     */

    public function property()
    {
        return $this->hasOne(Property::class, 'id', 'property_id');
    }

    /**
     * Get the user that this Features Note To Many.
     */
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}

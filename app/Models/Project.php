<?php

namespace App\Models;

use App\Support\Translatable;
use Illuminate\Database\Eloquent\Model;
use App\Models\Schedule;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

class Project extends Model
{
    use Translatable;

    protected $translatedAttributes = ['name', 'description'];

    protected $fillable = [
        'devolper_id',
        'city_id',
        'name_ar',
        'name_en',
        'description_ar',
        'description_en',
        'finished',
        'delivery',
        'link',
        'lat',
        'lng',
        'priority',
        'views',
        'clicked',
        'status',
        'virtual_tour_url',
        'youtube',
        'payment_id',
        'is_constructed',
        'location',

    ];

    protected $with = ['devolper', 'medias'];

    #########################
    ## Eloquent Functions ###
    #########################

    public function scopeActive($query)
    {
        return $query->where('status', true);
    }

    public function scopePriority($query)
    {
        return $query->where('priority', true);
    }

    public function scopePrioritySorted($query)
    {
        return $query->orderBy('priority', 'DESC');
    }
    /**
     * Get the Property that this payment belongs to.
     */
    public function paymentMathod()
    {
        return $this->belongsTo(PaymentMethod::class, 'payment_id');
    }

    public function activityTypes()
    {
        return $this->belongsToMany(ActivityType::class, 'activitytype_project');
    }

    public function furnishedTypes()
    {
        return $this->belongsToMany(FurnishedType::class, 'furnishedtype_project');
    }

    public function details()
    {
        return $this->hasMany(ProjectDetail::class);
    }

    /**
     * Get all media of product.
     */
    public function medias()
    {
        return $this->morphMany(Media::class, 'mediable');
    }
    public function viewContact()
    {
        return $this->morphMany(Contact::class, 'contacts');
    }
    /**
     * Get the Project that this devolper belongs to.
     */
    public function devolper()
    {
        return $this->belongsTo(Devolper::class);
    }


    public function schedules()
    {
        return $this->morphMany(Schedule::class, 'schedulable');
    }
    /**
     * Get the Project that this city belongs to.
     */
    public function city()
    {
        return $this->belongsTo(City::class);
    }
    /**
     * Get the Project that this activityType belongs to.
     */
    public function activityType()
    {
        return $this->belongsTo(ActivityType::class);
    }

    // public function getCreatedAtAttribute($date)
    // {
    //     return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d');
    // }

    // public function getUpdatedAtAttribute($date)
    // {
    //     return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d');
    // }
}

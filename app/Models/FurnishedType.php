<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FurnishedType extends Model
{
    public function projects()
    {
        return $this->belongsToMany(Project::class, 'furnishedtype_project');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PropertyAttribute extends Model
{

   public $timestamps = false;

   protected $fillable = [
      'property_id',
      'availability',
      'guest',
      'room',
      'bathroom',
      'area',
      'park',
      'location_name',
      'rental',
      'furnished',
      'type',
      'youtube',
      'url',
      'lat',
      'lng',

   ];
   /**
    * Get the City that this governorate belongs to.
    */
   public function property()
   {
      return $this->hasOne(Property::class);
   }
}

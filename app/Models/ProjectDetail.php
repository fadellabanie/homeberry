<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectDetail extends Model
{
    protected $fillable = [
        'project_id',
        'key',
        'value'
    ];

    public $timestamps = false;
}

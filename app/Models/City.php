<?php

namespace App\Models;

use App\Support\Translatable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    use Translatable;
    protected $translatedAttributes = ['name'];

    protected $fillable = ['governorate_id', 'name_ar', 'name_en', 'image', 'priority', 'status'];

    protected $with = ['governorate'];



    #########################
    ## Eloquent Functions ###
    #########################
    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::addGlobalScope('id', function (Builder $builder) {
            $builder->orderBy('id', 'DESC');
        });
    }

    public function scopeActive($query)
    {
        return $query->where('status', true);
    }

    public function scopePriority($query)
    {
        return $query->where('priority', true);
    }

    public function scopePrioritySorted($query)
    {
        return $query->orderBy('priority', 'DESC');
    }



    /**
     * Get the City that this governorate belongs to.
     */
    public function governorate()
    {
        return $this->belongsTo(Governorate::class);
    }
}

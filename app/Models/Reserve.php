<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Reserve extends Model
{
    protected $fillable = [
        'user_id', 'property_id', 'from', 'to', 'is_work_trip', 'diff_days', 'total', 'status'
    ];
    public function property()
    {
        return $this->belongsTo(Property::class);
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}

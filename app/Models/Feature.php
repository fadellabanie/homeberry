<?php

namespace App\Models;

use App\Support\Translatable;
use Illuminate\Database\Eloquent\Model;

class Feature extends Model
{
    use Translatable;

    protected $translatedAttributes = ['name'];

    //protected $fillable = ['name_ar', 'name_en', 'icon', 'type', 'status'];
    protected $guarded = [];

    public $timestamps = false;

     /**
     * Get the Property that this Features belongs To Many.
     */
    public function amenityProperty()
    {
        return $this->belongsToMany(Property::class,'amenity_property')->withPivot('amenity_id','property_id');
    } 
      /**
     * Get the Property that this Features belongs To Many.
     */
    public function serviceProperty()
    {
        return $this->belongsToMany(Property::class, 'service_property')->withPivot('service_id','property_id');
    }
    
}

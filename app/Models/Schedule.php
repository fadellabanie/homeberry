<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{

    protected $table = 'schedules';
    
    public $timestamps = false;

    protected $guarded = [];

    /**
     *
     */
    public function schedulable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
    public function property()
    {
        return $this->hasOne(Property::class, 'id', 'schedulable_id');
    }
    public function Project()
    {
        return $this->hasOne(Project::class, 'id', 'schedulable_id');
    }
}

<?php

namespace App\Exports;

use App\Models\Devolper;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class DeveloperExport implements FromView, ShouldAutoSize
{
    public function view(): View
    {
        return view('excel.developers', [
            'developers' => Devolper::all()
        ]);
    }
}

<?php

namespace App\Exports;

use App\Models\Project;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ProjectExport implements FromView, WithHeadings, ShouldAutoSize
{
    // /**
    // * @return \Illuminate\Support\Collection
    // */
    // public function collection()
    // {

    //     return ModelsProject::all();
    // }

    public function view(): View
    {
        return view('excel.projects', [
            'projects' => Project::all()
        ]);
    }


    public function headings(): array
    {
        return [
            '#',
            "devolper_id",
            "city_id",
            "activity_type_id",
            "name_ar",
            "name_en",
            "description_ar",
            "description_en",
            "finished",
            "delivery",
            "price",
            "link",
            "views",
            "lat",
            "lng",
            "priority",
            "status",
        ];
    }
}

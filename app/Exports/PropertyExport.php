<?php

namespace App\Exports;

use App\Models\Property;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class PropertyExport implements FromView, WithHeadings, ShouldAutoSize
{
    public function view(): View
    {
        return view('excel.properties', [
            'properties' => Property::all()
        ]);
    }



    // /**
    // * @return \Illuminate\Support\Collection
    // */
    // public function collection()
    // {
    //     dd(ModelsProperty::first());
    //     return ModelsProperty::all();
    // }

    public function headings(): array
    {
        return [
            '#',
            "devolper_id",
            "city_id",
            "activity_type_id",
            "name_ar",
            "name_en",
            "description_ar",
            "description_en",
            "finished",
            "delivery",
            "price",
            "link",
            "views",
            "lat",
            "lng",
            "priority",
            "status",
        ];
    }
}

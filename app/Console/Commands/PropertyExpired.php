<?php

namespace App\Console\Commands;

use App\Models\Property;
use DateTime;
use Illuminate\Console\Command;

class PropertyExpired extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'property-expired';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'change status of property after 60 Days';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $date = new DateTime();
            $date->modify('-60 days');
            $formatted = $date->format('Y-m-d H:i:s');
           // Property::where('created_at', '<=', $formatted)->delete();
            Property::where('created_at', '<=', $formatted)->update(['status'=>false]);
    }
}
